package com.app.tint.component.profile.userprofile.fav;

import com.google.gson.annotations.SerializedName;

public class UserFavoriteItem{

	@SerializedName("videoThumbnail")
	private String videoThumbnail;

	@SerializedName("androidLiveUrl")
	private String androidLiveUrl;

	@SerializedName("iosLiveUrl")
	private String iosLiveUrl;

	@SerializedName("videoName ")
	private String videoName;

	@SerializedName("onlineVideoUrl")
	private String onlineVideoUrl;

	@SerializedName("isLiked")
	private boolean isLiked;

	@SerializedName("videoId ")
	private int videoId;

	@SerializedName("liveStatus")
	private boolean liveStatus;

	@SerializedName("postedDate")
	private String postedDate;

	public void setVideoThumbnail(String videoThumbnail){
		this.videoThumbnail = videoThumbnail;
	}

	public String getVideoThumbnail(){
		return videoThumbnail;
	}

	public void setAndroidLiveUrl(String androidLiveUrl){
		this.androidLiveUrl = androidLiveUrl;
	}

	public String getAndroidLiveUrl(){
		return androidLiveUrl;
	}

	public void setIosLiveUrl(String iosLiveUrl){
		this.iosLiveUrl = iosLiveUrl;
	}

	public String getIosLiveUrl(){
		return iosLiveUrl;
	}

	public void setVideoName(String videoName){
		this.videoName = videoName;
	}

	public String getVideoName(){
		return videoName;
	}

	public void setOnlineVideoUrl(String onlineVideoUrl){
		this.onlineVideoUrl = onlineVideoUrl;
	}

	public String getOnlineVideoUrl(){
		return onlineVideoUrl;
	}

	public void setIsLiked(boolean isLiked){
		this.isLiked = isLiked;
	}

	public boolean isIsLiked(){
		return isLiked;
	}

	public void setVideoId(int videoId){
		this.videoId = videoId;
	}

	public int getVideoId(){
		return videoId;
	}

	public void setLiveStatus(boolean liveStatus){
		this.liveStatus = liveStatus;
	}

	public boolean isLiveStatus(){
		return liveStatus;
	}

	public void setPostedDate(String postedDate){
		this.postedDate = postedDate;
	}

	public String getPostedDate(){
		return postedDate;
	}

	@Override
 	public String toString(){
		return 
			"UserFavoriteItem{" + 
			"videoThumbnail = '" + videoThumbnail + '\'' + 
			",androidLiveUrl = '" + androidLiveUrl + '\'' + 
			",iosLiveUrl = '" + iosLiveUrl + '\'' + 
			",videoName  = '" + videoName + '\'' + 
			",onlineVideoUrl = '" + onlineVideoUrl + '\'' + 
			",isLiked = '" + isLiked + '\'' + 
			",videoId  = '" + videoId + '\'' + 
			",liveStatus = '" + liveStatus + '\'' + 
			",postedDate = '" + postedDate + '\'' + 
			"}";
		}
}