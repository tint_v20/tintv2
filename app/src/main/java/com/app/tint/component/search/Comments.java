package com.app.tint.component.search;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Comments{

	@SerializedName("commentsCount")
	private int commentsCount;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	@SerializedName("end")
	private int end;

	@SerializedName("userComments")
	private List<UserCommentsItem> userComments;

	public void setCommentsCount(int commentsCount){
		this.commentsCount = commentsCount;
	}

	public int getCommentsCount(){
		return commentsCount;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setEnd(int end){
		this.end = end;
	}

	public int getEnd(){
		return end;
	}

	public void setUserComments(List<UserCommentsItem> userComments){
		this.userComments = userComments;
	}

	public List<UserCommentsItem> getUserComments(){
		return userComments;
	}

	@Override
 	public String toString(){
		return 
			"Comments{" + 
			"commentsCount = '" + commentsCount + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			",end = '" + end + '\'' + 
			",userComments = '" + userComments + '\'' + 
			"}";
		}
}