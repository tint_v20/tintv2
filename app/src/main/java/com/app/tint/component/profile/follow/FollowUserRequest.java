package com.app.tint.component.profile.follow;

import com.google.gson.annotations.SerializedName;

public class FollowUserRequest{

	@SerializedName("followedUserDetails")
	private FollowedUserDetails followedUserDetails;

	@SerializedName("followerUserDetails")
	private FollowerUserDetails followerUserDetails;

	public void setFollowedUserDetails(FollowedUserDetails followedUserDetails){
		this.followedUserDetails = followedUserDetails;
	}

	public FollowedUserDetails getFollowedUserDetails(){
		return followedUserDetails;
	}

	public void setFollowerUserDetails(FollowerUserDetails followerUserDetails){
		this.followerUserDetails = followerUserDetails;
	}

	public FollowerUserDetails getFollowerUserDetails(){
		return followerUserDetails;
	}

	@Override
 	public String toString(){
		return 
			"FollowUserRequest{" + 
			"followedUserDetails = '" + followedUserDetails + '\'' + 
			",followerUserDetails = '" + followerUserDetails + '\'' + 
			"}";
		}
}