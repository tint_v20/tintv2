package com.app.tint.component.search;

import com.google.gson.annotations.SerializedName;

public class UserVideosItem{

	@SerializedName("videoThumbnail")
	private String videoThumbnail;

	@SerializedName("comments")
	private Comments comments;

	@SerializedName("isLiked")
	private boolean isLiked;

	@SerializedName("description")
	private String description;

	@SerializedName("videoId")
	private int videoId;

	@SerializedName("userId")
	private int userId;

	@SerializedName("streamName")
	private String streamName;

	@SerializedName("isFav")
	private boolean isFav;

	@SerializedName("isFollowed")
	private boolean isFollowed;

	@SerializedName("postedDate")
	private String postedDate;

	@SerializedName("imgUrl")
	private String imgUrl;

	@SerializedName("androidLiveUrl")
	private String androidLiveUrl;

	@SerializedName("iosLiveUrl")
	private String iosLiveUrl;

	@SerializedName("videoName")
	private String videoName;

	@SerializedName("onlineVideoUrl")
	private String onlineVideoUrl;

	@SerializedName("liveStatus")
	private boolean liveStatus;

	@SerializedName("likesCount")
	private int likesCount;

	public UserVideosItem(String _streamName) {
		this.streamName = _streamName;
	}

	public void setVideoThumbnail(String videoThumbnail){
		this.videoThumbnail = videoThumbnail;
	}

	public String getVideoThumbnail(){
		return videoThumbnail;
	}

	public void setComments(Comments comments){
		this.comments = comments;
	}

	public Comments getComments(){
		return comments;
	}

	public void setIsLiked(boolean isLiked){
		this.isLiked = isLiked;
	}

	public boolean isIsLiked(){
		return isLiked;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setVideoId(int videoId){
		this.videoId = videoId;
	}

	public int getVideoId(){
		return videoId;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setStreamName(String streamName){
		this.streamName = streamName;
	}

	public String getStreamName(){
		return streamName;
	}

	public void setIsFav(boolean isFav){
		this.isFav = isFav;
	}

	public boolean isIsFav(){
		return isFav;
	}

	public void setIsFollowed(boolean isFollowed){
		this.isFollowed = isFollowed;
	}

	public boolean isIsFollowed(){
		return isFollowed;
	}

	public void setPostedDate(String postedDate){
		this.postedDate = postedDate;
	}

	public String getPostedDate(){
		return postedDate;
	}

	public void setImgUrl(String imgUrl){
		this.imgUrl = imgUrl;
	}

	public String getImgUrl(){
		return imgUrl;
	}

	public void setAndroidLiveUrl(String androidLiveUrl){
		this.androidLiveUrl = androidLiveUrl;
	}

	public String getAndroidLiveUrl(){
		return androidLiveUrl;
	}

	public void setIosLiveUrl(String iosLiveUrl){
		this.iosLiveUrl = iosLiveUrl;
	}

	public String getIosLiveUrl(){
		return iosLiveUrl;
	}

	public void setVideoName(String videoName){
		this.videoName = videoName;
	}

	public String getVideoName(){
		return videoName;
	}

	public void setOnlineVideoUrl(String onlineVideoUrl){
		this.onlineVideoUrl = onlineVideoUrl;
	}

	public String getOnlineVideoUrl(){
		return onlineVideoUrl;
	}

	public void setLiveStatus(boolean liveStatus){
		this.liveStatus = liveStatus;
	}

	public boolean isLiveStatus(){
		return liveStatus;
	}

	public void setLikesCount(int likesCount){
		this.likesCount = likesCount;
	}

	public int getLikesCount(){
		return likesCount;
	}

	@Override
 	public String toString(){
		return 
			"UserVideosItem{" + 
			"videoThumbnail = '" + videoThumbnail + '\'' + 
			",comments = '" + comments + '\'' + 
			",isLiked = '" + isLiked + '\'' + 
			",description = '" + description + '\'' + 
			",videoId = '" + videoId + '\'' + 
			",userId = '" + userId + '\'' + 
			",streamName = '" + streamName + '\'' + 
			",isFav = '" + isFav + '\'' + 
			",isFollowed = '" + isFollowed + '\'' + 
			",postedDate = '" + postedDate + '\'' + 
			",imgUrl = '" + imgUrl + '\'' + 
			",androidLiveUrl = '" + androidLiveUrl + '\'' + 
			",iosLiveUrl = '" + iosLiveUrl + '\'' + 
			",videoName = '" + videoName + '\'' + 
			",onlineVideoUrl = '" + onlineVideoUrl + '\'' + 
			",liveStatus = '" + liveStatus + '\'' + 
			",likesCount = '" + likesCount + '\'' + 
			"}";
		}
}