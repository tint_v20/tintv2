package com.app.tint.component.profile.followers;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetFollowersResponse{

	@SerializedName("end")
	private int end;

	@SerializedName("followersCount")
	private int followersCount;

	@SerializedName("userFollowers")
	private List<UserFollowersItem> userFollowers;

	public void setEnd(int end){
		this.end = end;
	}

	public int getEnd(){
		return end;
	}

	public void setFollowersCount(int followersCount){
		this.followersCount = followersCount;
	}

	public int getFollowersCount(){
		return followersCount;
	}

	public void setUserFollowers(List<UserFollowersItem> userFollowers){
		this.userFollowers = userFollowers;
	}

	public List<UserFollowersItem> getUserFollowers(){
		return userFollowers;
	}

	@Override
 	public String toString(){
		return 
			"GetFollowersResponse{" + 
			"end = '" + end + '\'' + 
			",followersCount = '" + followersCount + '\'' + 
			",userFollowers = '" + userFollowers + '\'' + 
			"}";
		}
}