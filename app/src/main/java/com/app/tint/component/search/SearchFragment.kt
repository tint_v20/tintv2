package com.app.tint.component.search

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ItemTouchHelper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.LinearLayout
import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.app.slider.FragmentSlider
import com.app.tint.app.slider.IndikatorSlider
import com.app.tint.app.slider.SliderPagerAdapter
import com.app.tint.app.slider.SliderView
import com.app.tint.services.TintAPICall
import com.app.tint.services.TintServiceResponseCallback
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.jetbrains.anko.toast
import java.util.ArrayList
import android.text.Html
import com.app.tint.component.MainActivity
import com.app.tint.component.home.HomeFragment
import com.app.tint.utility.*


class SearchFragment: Fragment(), TintServiceResponseCallback {


    private var rvTrending: RecyclerView? = null

    private var mTrendingDataList : MutableList<UserVideosItem> = ArrayList()

    private var trendingAdapter: TrendingAdapter? = null

    //slider
    private var indicators: IndikatorSlider? = null

    private var slider: SliderView? = null
    private var linearLayout: LinearLayout? = null
    private var perPageTrendingItem = 0

    private var loadingTrending = false
    private var pastVisibleItemsTrending: Int = 0
    private var visibleItemCountTrending:Int = 0
    private var totalItemCountTrending:Int = 0

    private var gridLayoutManager: GridLayoutManager? = null

    companion object {

        fun newInstance(): SearchFragment {
            return SearchFragment()
        }
    }




    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        slider = view.findViewById(R.id.slider)
        linearLayout = view.findViewById(R.id.pagesContainer)

        //val text = "<font color=#ffffff><b>10:00 AM</b></font> <font color=#cc0029>How to clean brushes od India</font>"
        //tvHeader.text = HtmlCompat.fromHtml(text, HtmlCompat.FROM_HTML_MODE_LEGACY)



        //visiblePopularLayout(true)

        //flEdit.visibility = View.VISIBLE


        rvTrending = view.findViewById(R.id.rvTrending)

        gridLayoutManager = GridLayoutManager(context, 2)
        //layoutManagerPopular  = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        rvTrending!!.layoutManager = gridLayoutManager


        val itemTouchHelper = ItemTouchHelper(simpleCallbackItemTouchHelper)
        itemTouchHelper.attachToRecyclerView(rvTrending)

        //rvPopular = view.findViewById(R.id.rvPopular)

        //rvPopular.layoutManager = layoutManagerPopular

        loader.visibility = View.VISIBLE
        TintAPICall(context, this@SearchFragment).makeTintApiCall(perPageTrendingItem, ServiceType.GET_TRENDING_VIDEO)

        //TintAPICall(context, this@SearchFragment).makeTintApiCall(perPagePopularItem, ServiceType.GET_POPULAR_VIDEO, false)

       // TintAPICall(context, this@SearchFragment).makeTintApiCall(perPagePopularItem, ServiceType.SEARCH_QUERY, true)



    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupSlider()

        rvTrending!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            var isScrollUp = false

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0)
                //check for scroll down
                {

                    isScrollUp = false
                    visibleItemCountTrending = gridLayoutManager!!.childCount
                    totalItemCountTrending = gridLayoutManager!!.itemCount
                    pastVisibleItemsTrending = gridLayoutManager!!.findFirstVisibleItemPosition()

                    if (loadingTrending) {
                        if (visibleItemCountTrending + pastVisibleItemsTrending >= totalItemCountTrending) {
                            loadingTrending = false
                            Log.d("...", "Last Item Wow !")
                            //Do pagination.. i.e. fetch new data
                            rvTrending!!.post(Runnable {
                                loadMoreTrending()
                            })
                        }
                    }
                } else if (dy < 0) {
                    // Recycle view scrolling up...
                    //loading = true
                    isScrollUp = true
                } else {
                    if (!isScrollUp && loadingTrending) {
                        Log.d("elseRun", "else")
                        rvTrending!!.post(Runnable {
                            loadMoreTrending()
                        })
                    }
                }
            }


            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    // Do something
                    Log.d("newState", "SCROLL_STATE_TOUCH_SCROLL $loadingTrending $isScrollUp")
                    if (!isScrollUp && loadingTrending) {
                        loadMoreTrending()
                    }
                }
            }
        })

    }


    private fun setupSlider() {


        val fragment: MutableList<Fragment> = ArrayList()

        TintSingleton.getInstance().sliderVideosList.forEach {

            slider!!.setDurationScroll(800)
            fragment.add(FragmentSlider.newInstance(it.videoThumbnail, it.onlineVideoUrl, it.availableOn, it.description))

            val adapter = SliderPagerAdapter(activity!!.supportFragmentManager, fragment)
            slider!!.adapter = adapter
            indicators = IndikatorSlider(context!!, linearLayout!!, slider!!, R.drawable.indicator_circle)
            indicators!!.setPageCount(fragment.size)
            indicators!!.show()

        }

    }


//    private fun loadMorePopular() {
//
//        mPopularDataList.add(UserVideosItem("load"))
//        popularAdapter.notifyItemInserted(mPopularDataList.size - 1)
//
//        perPagePopularItem+5
//        TintAPICall(context, this@SearchFragment).makeTintApiCall(perPagePopularItem, ServiceType.LOAD_MORE, false)
//
//
//
//    }

    private fun loadMoreTrending() {

        mTrendingDataList.add(UserVideosItem("load"))
        trendingAdapter!!.notifyItemInserted(mTrendingDataList.size - 1)

        perPageTrendingItem+5

        TintAPICall(context, this@SearchFragment).makeTintApiCall(perPageTrendingItem, ServiceType.LOAD_MORE)

    }


    override fun onSuccessTintService(success: Any?, serviceType: Int) {

        if(success != null && isAdded) {

            when(serviceType) {

                ServiceType.GET_TRENDING_VIDEO -> {

                    loader.visibility = View.GONE

                    if(success is TrendingResponse) {

                        if(success.end == -1) {
                            loadingTrending = false
                        }

                        success.userVideos.forEach {
                            val userVideosItem = com.app.tint.component.home.UserVideosItem("")
                            userVideosItem.availableOn = it.postedDate.substring(0, 10)
                            userVideosItem.videoThumbnail = it.videoThumbnail
                            userVideosItem.description = it.description
                            userVideosItem.onlineVideoUrl = it.onlineVideoUrl

                            TintSingleton.getInstance().sliderVideosList.add(userVideosItem)
                        }


                        mTrendingDataList = success.userVideos

                        mTrendingDataList.addAll(mTrendingDataList)

                        //mTrendingDataList.removeAt((mTrendingDataList.size - 1))

                        trendingAdapter = TrendingAdapter(context!!, mTrendingDataList)
                        rvTrending!!.adapter = trendingAdapter

                    }

                    setupSlider()

                }

                ServiceType.GET_POPULAR_VIDEO -> {
//                    if(success is TrendingResponse) {
//
//                        if(success.end == -1) {
//                            loadingPopular = false
//                        }
//
//                        mPopularDataList = success.userVideos
//
//                        popularAdapter = PopularAdapter(context!!, mPopularDataList)
//                        rvPopular.adapter = popularAdapter
//                    }

                }

                ServiceType.SEARCH_QUERY-> {

//                    if(success is SearchQueryResponse && success.userVideos != null) {
//                        mSearchQueryDataList = success.userVideos
//                        val layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
//                        rvVideos.layoutManager = layoutManager
//
//                        searchQueryAdapter = SearchQueryAdapter(context!!, mSearchQueryDataList)
//                        rvVideos.adapter = searchQueryAdapter
//
//                    }

                }
            }
        }


    }

    override fun onErrorTintService(error: Any?, serviceType: Int) {

        if(error!= null && isAdded) {
           activity!!.toast(error.toString())

            loader.visibility = View.GONE
        }

    }


    private var simpleCallbackItemTouchHelper: ItemTouchHelper.SimpleCallback = object : ItemTouchHelper.SimpleCallback(0 , ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {

        override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {


            return false
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

            if(direction == ItemTouchHelper.RIGHT) {

                (activity as MainActivity).loadFragment(HomeFragment.newInstance(), "")


            }

            postDelayed(2000) {
                if(trendingAdapter != null)
                    trendingAdapter!!.notifyDataSetChanged()
            }

        }
    }




    override fun onDestroyView() {
        super.onDestroyView()

        if(indicators != null) {
            indicators!!.cleanUp()
            indicators = null
        }
        gridLayoutManager = null
    }




}