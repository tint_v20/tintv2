package com.app.tint.component.forgotauth

import android.os.Bundle
import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.app.BaseActivity
import com.app.tint.component.login.LoginActivity
import com.app.tint.component.resetpassword.ResetPasswordActivity
import com.app.tint.utility.launchActivity
import com.app.tint.utility.showToast
import kotlinx.android.synthetic.main.activity_forgot_pass.*

class ForgotPasswordActivity : BaseActivity(), BaseActivity.OnOTPVerification {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_pass)



        btnSubmit.setOnClickListener {

            when {
                edtPhone.text.isNullOrBlank() -> showToast(this, getString(R.string.missing_phone_no))
                else -> {
                    //todo call API OTP

                    processOtpVerification(edtPhone.text.toString())

                }
            }

        }
    }


    override fun oppVerified() {

        //todo call API

        launchActivity(this, ResetPasswordActivity::class.java, isFinish = true, isClearBack = true, isAnimLeft =true)
        TintApplication.instance.phoneNo = edtPhone.text.toString()

    }

    override fun onDestroy() {
        super.onDestroy()
        mGoogleApiClient!!.disconnect()
        activityContext = null
        mGoogleApiClient = null
    }


}