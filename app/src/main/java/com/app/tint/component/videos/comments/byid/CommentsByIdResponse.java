package com.app.tint.component.videos.comments.byid;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CommentsByIdResponse{

	@SerializedName("commentsCount")
	private int commentsCount;

	@SerializedName("end")
	private int end;

	@SerializedName("userComments")
	private List<UserCommentsItem> userComments;

	@SerializedName("status")
	private String status;

	public void setCommentsCount(int commentsCount){
		this.commentsCount = commentsCount;
	}

	public int getCommentsCount(){
		return commentsCount;
	}

	public void setEnd(int end){
		this.end = end;
	}

	public int getEnd(){
		return end;
	}

	public void setUserComments(List<UserCommentsItem> userComments){
		this.userComments = userComments;
	}

	public List<UserCommentsItem> getUserComments(){
		return userComments;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"CommentsByIdResponse{" + 
			"commentsCount = '" + commentsCount + '\'' + 
			",end = '" + end + '\'' + 
			",userComments = '" + userComments + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}