package com.app.tint.component.videos.views;

import com.google.gson.annotations.SerializedName;

public class GetVideoCountResponse{

	@SerializedName("viewsCount")
	private int viewsCount;

	public void setViewsCount(int viewsCount){
		this.viewsCount = viewsCount;
	}

	public int getViewsCount(){
		return viewsCount;
	}

	@Override
 	public String toString(){
		return 
			"GetVideoCountResponse{" + 
			"viewsCount = '" + viewsCount + '\'' + 
			"}";
		}
}