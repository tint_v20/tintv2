package com.app.tint.component.privay

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.component.MainActivity
import com.app.tint.component.login.LoginActivity
import com.app.tint.component.profile.EditProfileFragment
import com.app.tint.component.profile.ProfileFragment
import com.app.tint.component.resetpassword.ResetPasswordActivity
import com.app.tint.utility.Constants
import com.app.tint.utility.TintSingleton
import com.app.tint.utility.launchActivity
import kotlinx.android.synthetic.main.fragment_privacy.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import android.content.Intent
import android.net.Uri
import com.app.tint.utility.showToast


class PrivacyPolicyFragment: Fragment() {


    companion object {

        fun newInstance(): PrivacyPolicyFragment {
            return PrivacyPolicyFragment()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        TintSingleton.getInstance().PAGE_FROM = -1
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_privacy, container, false)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivBack.visibility = View.VISIBLE
        tvTitle.visibility = View.VISIBLE

    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tvTitle.text = getString(R.string.privacy_settings)

        tvPrivacySafety.setOnClickListener {  }
        tvEditProfile.setOnClickListener {

            TintSingleton.getInstance().PAGE_FROM = Constants.PAGE_PRIVACY_SAFETY

            (activity as MainActivity).loadFragment(EditProfileFragment.newInstance(), "")
        }
        tvResetPass.setOnClickListener {


            TintSingleton.getInstance().PAGE_FROM = Constants.PAGE_PRIVACY_SAFETY

            launchActivity(context!!, ResetPasswordActivity::class.java, isFinish = false, isClearBack = false, isAnimLeft = true)

        }
        tvPushNotification.setOnClickListener {  }
        tvPrivacyPolicy.setOnClickListener {
            val url = "http://scanta.io/terms-and-conditions/"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i) }
        tvPrivateAccount.setOnClickListener {
            showToast(activity!!, "Will available soon")
        }
        tvLogout.setOnClickListener {

            //TintApplication.instance.sharedPreferences.clearSharedPreference()
            TintApplication.instance.sharedPreferences.removeValue(Constants.PREFS_KEY_USER_NAME)
            TintApplication.instance.sharedPreferences.removeValue(Constants.PREFS_KEY_MOB)
            TintApplication.instance.sharedPreferences.removeValue(Constants.PREFS_KEY_USER_ID)
            TintApplication.instance.sharedPreferences.removeValue(Constants.PREFS_KEY_USER_PROFILE_PIC)
            TintApplication.instance.sharedPreferences.removeValue(Constants.PREFS_KEY_USER_EMAIL)
            TintApplication.instance.sharedPreferences.removeValue(Constants.PREFS_KEY_NAME)
            TintApplication.instance.sharedPreferences.removeValue(Constants.IS_OPEN_FRESH_AFTER_LOGOUT)

            launchActivity(context!!, LoginActivity::class.java, isFinish = true, isClearBack = true, isAnimLeft = false)

        }

        ivBack.setOnClickListener {

            (activity as MainActivity).loadFragment(ProfileFragment.newInstance(), "")
        }
    }


}