package com.app.tint.component.videos.videostream;

import com.google.gson.annotations.SerializedName;

public class VideoRequestById{

	@SerializedName("start")
	private int start;

	@SerializedName("userId")
	private int userId;

	public void setStart(int start){
		this.start = start;
	}

	public int getStart(){
		return start;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	@Override
 	public String toString(){
		return 
			"VideoRequestById{" + 
			"start = '" + start + '\'' + 
			",userId = '" + userId + '\'' + 
			"}";
		}
}