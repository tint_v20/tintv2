package com.app.tint.component.profile.userprofile

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.app.tint.component.profile.userprofile.fav.UserTagFragment
import com.app.tint.component.profile.userprofile.history.UserVideoHistoryFragment

class TabPagerAdapter(fm: FragmentManager, private var tabCount: Int) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {

        return when (position) {
            0 -> UserVideoHistoryFragment.newInstance()
            else -> UserTagFragment.newInstance()
        }
    }

    override fun getCount(): Int {
        return tabCount
    }
}
