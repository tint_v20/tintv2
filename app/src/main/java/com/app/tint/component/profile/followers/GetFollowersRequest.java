package com.app.tint.component.profile.followers;

import com.google.gson.annotations.SerializedName;

public class GetFollowersRequest{

	@SerializedName("start")
	private int start;

	@SerializedName("userId")
	private int userId;

	public void setStart(int start){
		this.start = start;
	}

	public int getStart(){
		return start;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	@Override
 	public String toString(){
		return 
			"GetFollowersRequest{" + 
			"start = '" + start + '\'' + 
			",userId = '" + userId + '\'' + 
			"}";
		}
}