package com.app.tint.component.home

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ItemTouchHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.component.MainActivity
import com.app.tint.component.login.LoginActivity
import com.app.tint.component.permisssions.PermissionsActivity
import com.app.tint.component.search.SearchFragment
import com.app.tint.component.videos.CameraActivity
import com.app.tint.component.videos.comments.byid.CommentsByIdResponse
import com.app.tint.component.videos.comments.byid.UserCommentsItem
import com.app.tint.component.videos.comments.request.AddCommentRequest
import com.app.tint.component.videos.comments.request.UserDetails
import com.app.tint.component.videos.comments.request.UserLiveVideos
import com.app.tint.component.videos.comments.response.CommentsResponse
import com.app.tint.jiaozivideoplayer.Jzvd
import com.app.tint.services.TintAPICall
import com.app.tint.services.TintServiceResponseCallback
import com.app.tint.utility.*
import com.app.tint.utility.Constants.encodeEmoji
import com.vanniktech.emoji.EmojiPopup
import kotlinx.android.synthetic.main.activity_bot_nav.*
import kotlinx.android.synthetic.main.fragment_home.*
import org.jetbrains.anko.toast
import kotlin.math.max
import kotlin.math.min


class HomeFragment: Fragment(), TintServiceResponseCallback {

    private var mRecyclerView: RecyclerView? = null
    private var homeAdapter1: HomeAdapter? = null
    private var perPage: Int = 0
    private var emojiPopup: EmojiPopup? = null
    private var rootView: ViewGroup? = null
    private var videoPos = 0
    private var chatBroadcastAdapter: ChatBroadcastAdapter? = null
    private var rvChatOnVideo: RecyclerView? = null
    private var pastVisibleItems: Int = 0
    private var visibleItemCount:Int = 0
    private var totalItemCount:Int = 0
    private var linearLayoutManager: LinearLayoutManager? = null
    private var isLoadMoreServiceHit: Boolean = false


    companion object {

        const val TAG = "MainDialog"

        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        this.chatBroadcastAdapter = ChatBroadcastAdapter(context)
        TintSingleton.getInstance().sliderVideosList.clear()

    }

    override fun onStop() {

        if (emojiPopup != null) {
            emojiPopup!!.dismiss()
        }
        super.onStop()
    }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            return inflater.inflate(R.layout.fragment_home, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mRecyclerView = view.findViewById(R.id.recycler_view)
        linearLayoutManager = LinearLayoutManager(context)
        mRecyclerView!!.layoutManager = linearLayoutManager
        mRecyclerView!!.addItemDecoration(SimpleDividerItemDecoration(context))
        mRecyclerView!!.setHasFixedSize(true)

        rvChatOnVideo = view.findViewById(R.id.rvChatOnVideo)
        rvChatOnVideo!!.adapter = chatBroadcastAdapter
        val layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        layoutManager.reverseLayout = true
        rvChatOnVideo!!.layoutManager = layoutManager
        rvChatOnVideo!!.isNestedScrollingEnabled = true



        rootView = view.findViewById(R.id.main_dialog_root_view)

        //ivEmojiInsert.setColorFilter(ContextCompat.getColor(context!!, R.color.emoji_icons), PorterDuff.Mode.SRC_IN);
        ivEmojiInsert.setOnClickListener { emojiPopup!!.toggle() }

        ivAtRate.setOnClickListener {
            emojicon_edit_text!!.text!!.insert(emojicon_edit_text!!.selectionStart, "@")
        }


        val itemTouchHelper = ItemTouchHelper(simpleCallbackItemTouchHelper)
        itemTouchHelper.attachToRecyclerView(mRecyclerView)

        callHomeService(isLoadMore = false)

        setUpEmojiPopup()

    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        homeAdapter1 = HomeAdapter(activity!!)


//        if(TintSingleton.instance.videosItemList.size > 0) {
//            btnStart.visibility = View.GONE
//            input_bar.visibility = View.GONE
////            homeAdapter.addData(TintSingleton.instance.videosItemList)
////            mRecyclerView.adapter = homeAdapter
////            snapHelper.attachToRecyclerView(mRecyclerView)
//
//            callHomeService(false)
//        } else {
//            callHomeService(false)
//        }

        btnStart.setOnClickListener {

            //todo All permission granted todo Camera Live
            if(TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID) != null) {
                if (TintApplication.instance.sharedPreferences.getValue(Constants.IS_CAMERA_PERMISSION_GRANTED) == null
                        || TintApplication.instance.sharedPreferences.getValue(Constants.IS_MICROPHONE_PERMISSION_GRANTED) == null) {

                    launchActivity(context!!, PermissionsActivity::class.java, isFinish = false, isClearBack = false, isAnimLeft = false)

                } else {

                    launchActivity(context!!, CameraActivity::class.java, isFinish = false, isClearBack = false, isAnimLeft = false)
                }

            }
            else {
                launchActivity(context!!, LoginActivity::class.java, isFinish = true, isClearBack = true, isAnimLeft = true)
            }
        }


        //todo Pagination

        mRecyclerView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            var isScrollUp = false

//            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
//                if (newState == RecyclerView.SCROLL_STATE_DRAGGING || newState == RecyclerView.SCROLL_STATE_SETTLING || newState == RecyclerView.SCROLL_STATE_IDLE) {
//                    homeAdapter1.onScrolled(recyclerView)
//                }
//
//               if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
//                   // Do something
//                    Log.d("newState", "SCROLL_STATE_TOUCH_SCROLL $TintSingleton.instance.isPaginationAvailable $isScrollUp")
//                    if (!isScrollUp && isPaginationAvailable) {
//                        isPaginationAvailable = false
//
//                        callHomeService(isLoadMore = true)
//                    }
//                }
//            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                    if (dy > 0)
                    //check for scroll down
                    {

                        isScrollUp = false
                        visibleItemCount = linearLayoutManager!!.childCount
                        totalItemCount = linearLayoutManager!!.itemCount
                        pastVisibleItems = linearLayoutManager!!.findFirstVisibleItemPosition()

                        if (TintSingleton.getInstance().isPaginationAvailable) {
                            if (visibleItemCount + pastVisibleItems >= totalItemCount) {
                                TintSingleton.getInstance().isPaginationAvailable = false
                                Log.d("...", "Last Item Wow !")
                                //Do pagination.. i.e. fetch new data
                                mRecyclerView!!.post(Runnable {
                                    callHomeService(isLoadMore = true)
                                })
                            }
                        }
                    } else if (dy < 0) {
                        // Recycle view scrolling up...
                        //loading = true
                        isScrollUp = true
                    }



//                    else {
//                        if (!isScrollUp && isPaginationAvailable) {
//                            Log.d("elseRun", "else")
//                            mRecyclerView.post(Runnable {
//                                callHomeService(isLoadMore = true)
//                            })
//                        }
//                    }
                }

        })

        emojicon_edit_text.setOnClickListener {
            emojiPopup!!.toggle()
        }


        ivSend.setOnClickListener {
            val text = emojicon_edit_text.text

            if (text != null && text.toString().isNotEmpty()) {

                val addCommentRequest = AddCommentRequest()
                addCommentRequest.comment = encodeEmoji(text.toString())
                addCommentRequest.userDetails = UserDetails()
                addCommentRequest.userDetails.userId = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID)
                addCommentRequest.userLiveVideos = UserLiveVideos()
                addCommentRequest.userLiveVideos.id = ""+TintApplication.instance.selectedVideoId

                TintAPICall(context, this).makeTintApiCall(addCommentRequest, ServiceType.ADD_COMMENT)

                emojicon_edit_text.setText("")
                rvChatOnVideo!!.visibility = View.VISIBLE
            }
        }


        mRecyclerView!!.addOnChildAttachStateChangeListener(object : RecyclerView.OnChildAttachStateChangeListener {
            override fun onChildViewAttachedToWindow(view: View) {

            }

            override fun onChildViewDetachedFromWindow(view: View) {
                val jzvd = view.findViewById<Jzvd>(R.id.videoplayer)
                if (jzvd != null && Jzvd.CURRENT_JZVD != null &&
                        jzvd.jzDataSource.containsTheUrl(Jzvd.CURRENT_JZVD.jzDataSource.currentUrl)) {
                    if (Jzvd.CURRENT_JZVD != null && Jzvd.CURRENT_JZVD.screen != Jzvd.SCREEN_FULLSCREEN) {
                        //Jzvd.releaseAllVideos()
                    }
                }
            }
        })

        snapHelper.attachToRecyclerView(mRecyclerView)

    }


    private fun setUpEmojiPopup() {
        emojiPopup = EmojiPopup.Builder.fromRootView(rootView)
                .setOnEmojiBackspaceClickListener { ignore -> Log.d(TAG, "Clicked on Backspace") }
                .setOnEmojiClickListener { _, _ -> Log.d(TAG, "Clicked on emoji") }
                .setOnEmojiPopupShownListener { ivEmojiInsert.setImageResource(R.drawable.ic_keyboard) }
                .setOnSoftKeyboardOpenListener {
                    ignore -> Log.d(TAG, "Opened soft keyboard")
                    if(ivSend != null)
                    ivSend.setImageResource(R.mipmap.ic_send_on)

                    if(activity is MainActivity) {
                        (activity as MainActivity).nav_view.visibility = View.GONE
                    }
                }
                .setOnEmojiPopupDismissListener { ivEmojiInsert.setImageResource(R.mipmap.emoji) }
                .setOnSoftKeyboardCloseListener {
                    Log.d(TAG, "Closed soft keyboard")
                    if(ivSend != null)
                    ivSend.setImageResource(R.mipmap.ic_send_off)

                    if(activity is MainActivity) {
                        (activity as MainActivity).nav_view.visibility = View.VISIBLE
                    }
                }
                .setKeyboardAnimationStyle(R.style.emoji_fade_animation_style)
                .setPageTransformer(PageTransformer())
                .build(emojicon_edit_text)
    }


    private var snapHelper: LinearSnapHelper = object : LinearSnapHelper() {
        override fun findTargetSnapPosition(layoutManager: RecyclerView.LayoutManager?, velocityX: Int, velocityY: Int): Int {
            val centerView = findSnapView(layoutManager!!) ?: return RecyclerView.NO_POSITION

            val position = layoutManager.getPosition(centerView)
            var targetPosition = -1
            if (layoutManager.canScrollHorizontally()) {
                targetPosition = if (velocityX < 0) {
                    position - 1
                } else {
                    position + 1
                }
            }

            if (layoutManager.canScrollVertically()) {
                targetPosition = if (velocityY < 0) {
                    position - 1
                } else {
                    position + 1
                }
            }

            val firstItem = 0
            val lastItem = layoutManager.itemCount - 1
            targetPosition = min(lastItem, max(targetPosition, firstItem))
            Log.d("targetPosition", ""+  targetPosition)
            videoPos = targetPosition
            TintApplication.instance.selectedVideoId = TintSingleton.getInstance().videosItemList[targetPosition].videoId

            homeAdapter1!!.playVideoPos(targetPosition)

            rvChatOnVideo!!.visibility = View.GONE

            postDelayed(3000) {
                if(isAdded)
                TintAPICall(context, this@HomeFragment).makeTintApiCall(TintApplication.instance.perPagePagination, ServiceType.COMMENTS_DETAILS)
            }
            return targetPosition
        }
    }

    override fun onSuccessTintService(success: Any?, serviceType: Int) {

        if (success != null && isAdded) {
            btnStart.visibility = View.GONE

            when (serviceType) {

                ServiceType.GET_ALL_VIDEOS -> {

                    loader.visibility = View.GONE

                        if (success is GetAllVideoResponse) {

                            //remove loading view
                            if(isLoadMoreServiceHit) {
                                if (success.userVideos.size > 0) {

                                    TintSingleton.getInstance().videosItemList.removeAt(TintSingleton.getInstance().videosItemList.size - 1)

                                    TintSingleton.getInstance().videosItemList.addAll(success.userVideos)
                                    homeAdapter1!!.updateData(TintSingleton.getInstance().videosItemList)


                                } else {//result size 0 means there is no more data available at server
                                    homeAdapter1!!.setMoreDataAvailable(false)
                                    //telling adapter to stop calling load more as no more server data available
                                    //Toast.makeText(context,"No More Data Available",Toast.LENGTH_LONG).show();
                                }
                            } else {

                                success.userVideos.forEach {

                                    if(it.isIsUploaded) {

                                        val userVideosItem = UserVideosItem("")
                                        userVideosItem.availableOn = it.availableOn
                                        userVideosItem.videoThumbnail = it.videoThumbnail
                                        userVideosItem.description = it.description
                                        userVideosItem.onlineVideoUrl = it.onlineVideoUrl

                                        TintSingleton.getInstance().sliderVideosList.add(userVideosItem)
                                    }
                                }



                                TintSingleton.getInstance().isPaginationAvailable = success.end != -1

                                TintSingleton.getInstance().videosItemList = success.userVideos

                                TintApplication.instance.selectedVideoId = TintSingleton.getInstance().videosItemList[0].videoId


                                homeAdapter1!!.addData(TintSingleton.getInstance().videosItemList)
                                mRecyclerView!!.adapter = homeAdapter1
                                input_bar.visibility = View.VISIBLE

                            }

                            //TintAPICall(context, this).makeTintApiCall(TintApplication.instance.perPagePagination, ServiceType.COMMENTS_DETAILS, false)
                        }

                }

                ServiceType.ADD_COMMENT -> {

                    if(success is CommentsResponse) {

                        TintAPICall(context, this).makeTintApiCall(TintApplication.instance.perPagePagination, ServiceType.COMMENTS_DETAILS)

                    }
                }

                ServiceType.COMMENTS_DETAILS -> {

                    if(success is CommentsByIdResponse) {

                        if(success.userComments != null && success.commentsCount > 0) {

                            chatBroadcastAdapter!!.addChatData(success.userComments)
                            rvChatOnVideo!!.visibility = View.VISIBLE
                        } else {

                            val userComments : ArrayList<UserCommentsItem> = ArrayList()
                            chatBroadcastAdapter!!.addChatData(userComments)
                            rvChatOnVideo!!.visibility = View.GONE

                        }

                    }
                }

            }
        }
    }

    override fun onErrorTintService(error: Any?, serviceType: Int) {

        if (error != null && isAdded) {
            activity!!.toast(error.toString())
            rvChatOnVideo!!.visibility = View.GONE
            btnStart.visibility = View.VISIBLE
            loader.visibility = View.GONE

        }

    }



    private var simpleCallbackItemTouchHelper: ItemTouchHelper.SimpleCallback = object : ItemTouchHelper.SimpleCallback(0 , ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {

        override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {


            return false
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

            if(direction == ItemTouchHelper.LEFT) {

                (activity as MainActivity).loadFragment(SearchFragment.newInstance(), "")

                //launchActivity(context!!, VideoItemActivity::class.java, isFinish = false, isClearBack = false, isAnimLeft = true)

            } else {

                if (TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID) != null) {


                    if (TintApplication.instance.sharedPreferences.getValue(Constants.IS_CAMERA_PERMISSION_GRANTED) == null
                            || TintApplication.instance.sharedPreferences.getValue(Constants.IS_MICROPHONE_PERMISSION_GRANTED) == null) {

                        launchActivity(context!!, PermissionsActivity::class.java, isFinish = false, isClearBack = false, isAnimLeft = false)

                    } else {

                        //todo All permission granted todo Camera Live
                        launchActivity(context!!, CameraActivity::class.java, isFinish = false, isClearBack = false, isAnimLeft = false)

                    }
                } else {

                    launchActivity(context!!, LoginActivity::class.java, isFinish = true, isClearBack = true, isAnimLeft =true)


                }
            }

            postDelayed(2000) {
                if(homeAdapter1 != null)
                homeAdapter1!!.notifyDataSetChanged()
            }

        }
    }

    private fun callHomeService(isLoadMore: Boolean) {
        if(isLoadMore) {
            isLoadMoreServiceHit = true
            perPage += 5
            Log.d("perPage", ""+ perPage)
            TintSingleton.getInstance().videosItemList.add(UserVideosItem("load"))
            homeAdapter1!!.notifyItemInserted(TintSingleton.getInstance().videosItemList.size - 1)

            TintAPICall(context, this).makeTintApiCall(perPage, ServiceType.GET_ALL_VIDEOS)
        } else {

            loader.visibility = View.VISIBLE

            TintAPICall(context, this).makeTintApiCall(perPage, ServiceType.GET_ALL_VIDEOS)
        }

    }

    override fun onPause() {
        super.onPause()
       Jzvd.releaseAllVideos()

    }


    override fun onDestroyView() {
        super.onDestroyView()

         mRecyclerView= null
         homeAdapter1 = null
         emojiPopup = null
         rootView = null
         chatBroadcastAdapter = null
         rvChatOnVideo= null
         linearLayoutManager = null
         TintSingleton.getInstance().videosItemList.clear()
    }

}