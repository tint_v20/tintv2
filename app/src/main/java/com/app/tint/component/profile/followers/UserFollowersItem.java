package com.app.tint.component.profile.followers;

import com.google.gson.annotations.SerializedName;

public class UserFollowersItem{

	@SerializedName("imgUrl")
	private String imgUrl;

	@SerializedName("name")
	private String name;

	@SerializedName("userType")
	private String userType;

	@SerializedName("userName")
	private String userName;

	@SerializedName("userId")
	private int userId;

	@SerializedName("points")
	private int points;

	public void setImgUrl(String imgUrl){
		this.imgUrl = imgUrl;
	}

	public String getImgUrl(){
		return imgUrl;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setUserType(String userType){
		this.userType = userType;
	}

	public String getUserType(){
		return userType;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setPoints(int points){
		this.points = points;
	}

	public int getPoints(){
		return points;
	}

	@Override
 	public String toString(){
		return 
			"UserFollowersItem{" + 
			"imgUrl = '" + imgUrl + '\'' + 
			",name = '" + name + '\'' + 
			",userType = '" + userType + '\'' + 
			",userName = '" + userName + '\'' + 
			",userId = '" + userId + '\'' + 
			",points = '" + points + '\'' + 
			"}";
		}
}