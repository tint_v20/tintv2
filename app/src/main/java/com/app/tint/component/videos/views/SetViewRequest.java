package com.app.tint.component.videos.views;

import com.google.gson.annotations.SerializedName;

public class SetViewRequest{

	@SerializedName("userLiveVideos")
	private UserLiveVideos userLiveVideos;

	@SerializedName("userDetails")
	private UserDetails userDetails;

	public void setUserLiveVideos(UserLiveVideos userLiveVideos){
		this.userLiveVideos = userLiveVideos;
	}

	public UserLiveVideos getUserLiveVideos(){
		return userLiveVideos;
	}

	public void setUserDetails(UserDetails userDetails){
		this.userDetails = userDetails;
	}

	public UserDetails getUserDetails(){
		return userDetails;
	}

	@Override
 	public String toString(){
		return 
			"SetViewRequest{" + 
			"userLiveVideos = '" + userLiveVideos + '\'' + 
			",userDetails = '" + userDetails + '\'' + 
			"}";
		}
}