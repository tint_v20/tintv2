package com.app.tint.component.videos.views;

import com.google.gson.annotations.SerializedName;


public class UserLiveVideos{

	@SerializedName("id")
	private int id;

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"UserLiveVideos{" + 
			"id = '" + id + '\'' + 
			"}";
		}
}