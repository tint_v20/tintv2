package com.app.tint.component.videos.comments.request;

import com.google.gson.annotations.SerializedName;

public class AddCommentRequest{

	@SerializedName("userLiveVideos")
	private UserLiveVideos userLiveVideos;

	@SerializedName("comment")
	private String comment;

	@SerializedName("userDetails")
	private UserDetails userDetails;

	public void setUserLiveVideos(UserLiveVideos userLiveVideos){
		this.userLiveVideos = userLiveVideos;
	}

	public UserLiveVideos getUserLiveVideos(){
		return userLiveVideos;
	}

	public void setComment(String comment){
		this.comment = comment;
	}

	public String getComment(){
		return comment;
	}

	public void setUserDetails(UserDetails userDetails){
		this.userDetails = userDetails;
	}

	public UserDetails getUserDetails(){
		return userDetails;
	}

	@Override
 	public String toString(){
		return 
			"AddCommentRequest{" + 
			"userLiveVideos = '" + userLiveVideos + '\'' + 
			",comment = '" + comment + '\'' + 
			",userDetails = '" + userDetails + '\'' + 
			"}";
		}
}