/**
 * This is sample code provided by Wowza Media Systems, LLC.  All sample code is intended to be a reference for the
 * purpose of educating developers, and is not intended to be used in any production environment.
 *
 * IN NO EVENT SHALL WOWZA MEDIA SYSTEMS, LLC BE LIABLE TO YOU OR ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 * EVEN IF WOWZA MEDIA SYSTEMS, LLC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * WOWZA MEDIA SYSTEMS, LLC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. ALL CODE PROVIDED HEREUNDER IS PROVIDED "AS IS".
 * WOWZA MEDIA SYSTEMS, LLC HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 * © 2015 – 2019 Wowza Media Systems, LLC. All rights reserved.
 */

package com.app.tint.component.videos

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import androidx.core.view.GestureDetectorCompat
import android.util.Log
import android.view.MotionEvent
import android.view.View

import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.app.CameraActivityBase
import com.app.tint.app.GoCoderSDKActivityBase
import com.app.tint.component.MainActivity
import com.app.tint.component.common.AlertDialogue
import com.app.tint.component.videos.videostream.*
import com.app.tint.services.TintAPICall
import com.app.tint.services.TintServiceResponseCallback
import com.app.tint.utility.Constants
import com.app.tint.utility.Constants.IS_OPEN_FRESH_AFTER_LOGOUT
import com.app.tint.utility.ServiceType
import com.app.tint.utility.launchActivity
import com.wowza.gocoder.sdk.api.devices.WOWZCamera
import kotlinx.android.synthetic.main.activity_camera.*
import kotlinx.android.synthetic.main.before_camera_activity.*
import org.jetbrains.anko.toast
import java.text.SimpleDateFormat
import java.util.*


class CameraActivity : CameraActivityBase(), AlertDialogue.OnDialogItemClick, TintServiceResponseCallback {

    // UI controls
    protected var mBtnSwitchCamera: MultiStateButton? = null
    protected var mBtnTorch: MultiStateButton? = null
    protected var mTimerView: TimerView? = null
    private var steamVideoId: Int? = null
    private var inputVideoName: String? = null
    private var inputVideoDesc: String? = null


    // Gestures are used to toggle the focus modes
    protected var mAutoFocusDetector: GestureDetectorCompat? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)

        if (TintApplication.instance.sharedPreferences.getValue(IS_OPEN_FRESH_AFTER_LOGOUT) == null) {
            onSettings()
            TintApplication.instance.sharedPreferences.save(IS_OPEN_FRESH_AFTER_LOGOUT, "hasOpened")
//
//            Handler().postDelayed({
//                //Do something after 100ms
//                //super.onBackPressed()
//            }, 1000)


        }

        mRequiredPermissions = arrayOf(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO)

        // Initialize the UI controls
        mBtnTorch = findViewById(R.id.ic_torch)
        mBtnSwitchCamera = findViewById(R.id.ic_switch_camera)
        mTimerView = findViewById(R.id.txtTimer)

        rlForm.visibility = View.VISIBLE
        rlCamera.visibility = View.GONE


        findViewById<View>(R.id.ic_broadcast).setOnClickListener {

            val vidName = inputVideoName!!.replace(" ", "_")


            onToggleBroadcast()
            findViewById<View>(R.id.btnFinishBroadcast).visibility = View.VISIBLE
            findViewById<View>(R.id.ic_broadcast).visibility = View.GONE

            findViewById<View>(R.id.rlLiveViews).visibility = View.VISIBLE

            //todo start streaming service

            val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this)
            //val prefsEditor = sharedPrefs.edit()
            //prefsEditor.putString("wz_live_stream_name", TintApplication.instance.streamName)
            //prefsEditor.apply()



            val startStreamVideoRequest = StartStreamVideoRequest()
            startStreamVideoRequest.urlName = sharedPrefs.getString("wz_live_stream_name", null)
            startStreamVideoRequest.videoName = vidName
            startStreamVideoRequest.videoDescription = inputVideoDesc
            startStreamVideoRequest.userDetails = UserDetails()
            startStreamVideoRequest.userDetails.userId = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID)!!.toInt()


            TintAPICall(this, this).makeTintApiCall(startStreamVideoRequest, ServiceType.START_STREAM)

        }

        findViewById<View>(R.id.btnFinishBroadcast).setOnClickListener {
            val alertDialogue = AlertDialogue(this@CameraActivity, getString(R.string.sure), getString(R.string.finish))
            if (!alertDialogue.isShowing) {
                alertDialogue.show()
            }
        }

        fabDone.setOnClickListener {
            if (!isFinishing) {

                     inputVideoName = etVideoName.text.toString()
                     inputVideoDesc = etVideoDescription.text.toString()
                    when {
                        inputVideoName!!.isEmpty() -> toast("Enter Video Name")
                        inputVideoDesc!!.isEmpty() -> toast("Enter Description Name")
                        else -> {

                            //todo open Camera

                            rlForm.visibility = View.GONE
                            rlCamera.visibility = View.VISIBLE

                        }
                    }

                }
        }

        ivCancel.setOnClickListener {

            this.finish()

        }

    }

    /**
     * Android Activity lifecycle methods
     */
    override fun onResume() {
        super.onResume()
        if (this.hasDevicePermissionToAccess() && TintApplication.instance.sGoCoderSDK != null && mWZCameraView != null) {
            if (mAutoFocusDetector == null)
                mAutoFocusDetector = GestureDetectorCompat(this, AutoFocusListener(this, mWZCameraView))

            val activeCamera = mWZCameraView!!.camera
            if (activeCamera != null && activeCamera.hasCapability(WOWZCamera.FOCUS_MODE_CONTINUOUS))
                activeCamera.focusMode = WOWZCamera.FOCUS_MODE_CONTINUOUS
        }
    }

    override fun onPause() {
        super.onPause()
    }

    /**
     * Click handler for the switch camera button
     */
    fun onSwitchCamera(v: View) {
        if (mWZCameraView == null) return

        mBtnTorch!!.setState(false)
        mBtnTorch!!.isEnabled = false

        // Set the new surface extension prior to camera switch such that
        // setting will take place with the new one.  So if it is currently the front
        // camera, then switch to default setting (not mirrored).  Otherwise show mirrored.
        //        if(mWZCameraView.getCamera().getDirection() == WOWZCamera.DIRECTION_FRONT) {
        //            mWZCameraView.setSurfaceExtension(mWZCameraView.EXTENSION_DEFAULT);
        //        }
        //        else{
        //            mWZCameraView.setSurfaceExtension(mWZCameraView.EXTENSION_MIRROR);
        //        }

        val newCamera = mWZCameraView!!.switchCamera()
        if (newCamera != null) {
            if (newCamera.hasCapability(WOWZCamera.FOCUS_MODE_CONTINUOUS))
                newCamera.focusMode = WOWZCamera.FOCUS_MODE_CONTINUOUS

            val hasTorch = newCamera.hasCapability(WOWZCamera.TORCH)
            if (hasTorch) {
                mBtnTorch!!.setState(newCamera.isTorchOn)
                mBtnTorch!!.isEnabled = true
            }
        }
    }

    /**
     * Click handler for the torch/flashlight button
     */
    fun onToggleTorch(v: View) {
        if (mWZCameraView == null) return

        val activeCamera = mWZCameraView!!.camera
        activeCamera.isTorchOn = mBtnTorch!!.toggleState()
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (mAutoFocusDetector != null)
            mAutoFocusDetector!!.onTouchEvent(event)

        return super.onTouchEvent(event)
    }

    /**
     * Update the state of the UI controls
     */
    override fun syncUIControlState(): Boolean {
        val disableControls = super.syncUIControlState()

        if (disableControls) {
            mBtnSwitchCamera!!.isEnabled = false
            mBtnTorch!!.isEnabled = false
        } else {
            val isDisplayingVideo = this.hasDevicePermissionToAccess(Manifest.permission.CAMERA) && broadcastConfig!!.isVideoEnabled && mWZCameraView!!.cameras.size > 0
            val isStreaming = broadcast!!.status.isRunning

            if (isDisplayingVideo) {
                val activeCamera = mWZCameraView!!.camera

                val hasTorch = activeCamera != null && activeCamera.hasCapability(WOWZCamera.TORCH)
                mBtnTorch!!.isEnabled = hasTorch
                if (hasTorch) {
                    mBtnTorch!!.setState(activeCamera!!.isTorchOn)
                }

                mBtnSwitchCamera!!.isEnabled = mWZCameraView!!.cameras.isNotEmpty()
            } else {
                mBtnSwitchCamera!!.isEnabled = false
                mBtnTorch!!.isEnabled = false
            }

            if (isStreaming && !mTimerView!!.isRunning) {
                mTimerView!!.startTimer()
            } else if (broadcast!!.status.isIdle && mTimerView!!.isRunning) {
                mTimerView!!.stopTimer()
            } else if (!isStreaming) {
                mTimerView!!.visibility = View.GONE
            }
        }

        return disableControls
    }

    override fun onButtonClick(code: Int) {

        if (code == Constants.DLG_CONFIRM) {

            val stopLiveStreamRequest = StopLiveStreamRequest()
            stopLiveStreamRequest.id = ""+steamVideoId
            stopLiveStreamRequest.userDetails = UserDetails()
            stopLiveStreamRequest.userDetails.userId = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID)!!.toInt()

            onToggleBroadcast()
            TintAPICall(this, this).makeTintApiCall(stopLiveStreamRequest, ServiceType.STOP_STREAM)
            findViewById<View>(R.id.btnFinishBroadcast).visibility = View.GONE
            findViewById<View>(R.id.ic_broadcast).visibility = View.VISIBLE
            findViewById<View>(R.id.rlLiveViews).visibility = View.GONE


        }

    }

    override fun onBackPressed() {


        if (!isStartStreaming) {
            super.onBackPressed()
            this@CameraActivity.finish()
            overridePendingTransition(R.anim.enter, R.anim.exit)
        }
    }

    companion object {
        private val TAG = CameraActivity::class.java.simpleName
    }

    override fun onSuccessTintService(success: Any?, serviceType: Int) {

        Log.d(TAG, ""+success)

        if(success != null && !isFinishing) {
            when(serviceType) {

                ServiceType.START_STREAM -> {

                    if(success is StartVideoStreamResponse) {

                        Log.d(TAG, ""+success)
                        steamVideoId = success.videoId
                        findViewById<View>(R.id.btnFinishBroadcast).visibility = View.VISIBLE
                    }

                }


                ServiceType.STOP_STREAM -> {

                    if(success is StopLiveStreamResponse) {

                        Log.d(TAG, ""+success)

                        findViewById<View>(R.id.btnFinishBroadcast).visibility = View.GONE

                        launchActivity(
                                this,
                                MainActivity::class.java,
                                isFinish = true,
                                isClearBack = false,
                                isAnimLeft = true
                        )
                    }

                }
            }
        }

    }

    override fun onErrorTintService(error: Any?, serviceType: Int) {

        if(error != null) {
            toast(error.toString())
        }

    }

}
