package com.app.tint.component.signup


import com.google.gson.annotations.SerializedName


class RegistrationResponse {

    @SerializedName("userData")
    var userData: UserData? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("status")
    var status: String? = null

    override fun toString(): String {
        return "RegistrationResponse{" +
                "userData = '" + userData + '\''.toString() +
                ",message = '" + message + '\''.toString() +
                ",status = '" + status + '\''.toString() +
                "}"
    }
}