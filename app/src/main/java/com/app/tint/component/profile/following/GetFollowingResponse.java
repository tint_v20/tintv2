package com.app.tint.component.profile.following;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetFollowingResponse{

	@SerializedName("userFollowing")
	private List<UserFollowingItem> userFollowing;

	@SerializedName("end")
	private int end;

	@SerializedName("followingCount")
	private int followingCount;

	public void setUserFollowing(List<UserFollowingItem> userFollowing){
		this.userFollowing = userFollowing;
	}

	public List<UserFollowingItem> getUserFollowing(){
		return userFollowing;
	}

	public void setEnd(int end){
		this.end = end;
	}

	public int getEnd(){
		return end;
	}

	public void setFollowingCount(int followingCount){
		this.followingCount = followingCount;
	}

	public int getFollowingCount(){
		return followingCount;
	}

	@Override
 	public String toString(){
		return 
			"GetFollowingResponse{" + 
			"userFollowing = '" + userFollowing + '\'' + 
			",end = '" + end + '\'' + 
			",followingCount = '" + followingCount + '\'' + 
			"}";
		}
}