package com.app.tint.component.profile;

import com.google.gson.annotations.SerializedName;

public class GetUserResponse{

	@SerializedName("followersCount")
	private int followersCount;

	@SerializedName("userDetails")
	private UserDetails userDetails;

	@SerializedName("followingCount")
	private int followingCount;

	@SerializedName("isFollowed")
	private boolean isFollowed;

	public void setFollowersCount(int followersCount){
		this.followersCount = followersCount;
	}

	public int getFollowersCount(){
		return followersCount;
	}

	public void setUserDetails(UserDetails userDetails){
		this.userDetails = userDetails;
	}

	public UserDetails getUserDetails(){
		return userDetails;
	}

	public void setFollowingCount(int followingCount){
		this.followingCount = followingCount;
	}

	public int getFollowingCount(){
		return followingCount;
	}

	public void setIsFollowed(boolean isFollowed){
		this.isFollowed = isFollowed;
	}

	public boolean isIsFollowed(){
		return isFollowed;
	}

	@Override
 	public String toString(){
		return 
			"GetUserResponse{" + 
			"followersCount = '" + followersCount + '\'' + 
			",userDetails = '" + userDetails + '\'' + 
			",followingCount = '" + followingCount + '\'' + 
			",isFollowed = '" + isFollowed + '\'' + 
			"}";
		}
}