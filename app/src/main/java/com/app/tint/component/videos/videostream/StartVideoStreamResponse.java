package com.app.tint.component.videos.videostream;

import com.google.gson.annotations.SerializedName;

public class StartVideoStreamResponse{

	@SerializedName("androidLiveUrl")
	private String androidLiveUrl;

	@SerializedName("iosLiveUrl")
	private String iosLiveUrl;

	@SerializedName("userData")
	private UserData userData;

	@SerializedName("videoName")
	private String videoName;

	@SerializedName("onlineVideoUrl")
	private String onlineVideoUrl;

	@SerializedName("videoId")
	private int videoId;

	@SerializedName("message")
	private String message;

	@SerializedName("streamName")
	private String streamName;

	@SerializedName("liveStatus")
	private boolean liveStatus;

	@SerializedName("status")
	private String status;

	public void setAndroidLiveUrl(String androidLiveUrl){
		this.androidLiveUrl = androidLiveUrl;
	}

	public String getAndroidLiveUrl(){
		return androidLiveUrl;
	}

	public void setIosLiveUrl(String iosLiveUrl){
		this.iosLiveUrl = iosLiveUrl;
	}

	public String getIosLiveUrl(){
		return iosLiveUrl;
	}

	public void setUserData(UserData userData){
		this.userData = userData;
	}

	public UserData getUserData(){
		return userData;
	}

	public void setVideoName(String videoName){
		this.videoName = videoName;
	}

	public String getVideoName(){
		return videoName;
	}

	public void setOnlineVideoUrl(String onlineVideoUrl){
		this.onlineVideoUrl = onlineVideoUrl;
	}

	public String getOnlineVideoUrl(){
		return onlineVideoUrl;
	}

	public void setVideoId(int videoId){
		this.videoId = videoId;
	}

	public int getVideoId(){
		return videoId;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStreamName(String streamName){
		this.streamName = streamName;
	}

	public String getStreamName(){
		return streamName;
	}

	public void setLiveStatus(boolean liveStatus){
		this.liveStatus = liveStatus;
	}

	public boolean isLiveStatus(){
		return liveStatus;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"StartVideoStreamResponse{" + 
			"androidLiveUrl = '" + androidLiveUrl + '\'' + 
			",iosLiveUrl = '" + iosLiveUrl + '\'' + 
			",userData = '" + userData + '\'' + 
			",videoName = '" + videoName + '\'' + 
			",onlineVideoUrl = '" + onlineVideoUrl + '\'' + 
			",videoId = '" + videoId + '\'' + 
			",message = '" + message + '\'' + 
			",streamName = '" + streamName + '\'' + 
			",liveStatus = '" + liveStatus + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}