package com.app.tint.component.profile

import android.annotation.SuppressLint
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.app.BottomSheetFragment
import com.app.tint.component.MainActivity
import com.app.tint.component.login.LoginActivity
import com.app.tint.component.openfire.ChatActivity
import com.app.tint.component.privay.PrivacyPolicyFragment
import com.app.tint.component.profile.follow.FollowResponse
import com.app.tint.component.profile.follow.FollowUserRequest
import com.app.tint.component.profile.follow.FollowedUserDetails
import com.app.tint.component.profile.follow.FollowerUserDetails
import com.app.tint.component.profile.userprofile.TabPagerAdapter
import com.app.tint.component.profile.userprofile.history.UserVideoHistoryFragment
import com.app.tint.services.TintAPICall
import com.app.tint.services.TintServiceResponseCallback
import com.app.tint.utility.Constants
import com.app.tint.utility.ServiceType
import com.app.tint.utility.TintSingleton
import com.app.tint.utility.launchActivity
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_top_profile.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class ProfileFragment: Fragment(), TintServiceResponseCallback , BottomSheetFragment.OnBottomSheetInterface, View.OnClickListener, UserVideoHistoryFragment.OnVideoCountUpdateSetListener {


    private var ivProfile: CircleImageView? = null
    private var tvFollowing: TextView? = null
    private var tvFollowers: TextView? = null
    private var btnMessage: TextView? = null
    private var ivAddFriend: ImageView? = null
    private var tvDisplayName: TextView? = null
    private var tvDescription: TextView? = null
    private var tvVideosCount :TextView? = null



    companion object {

        fun newInstance(): ProfileFragment {
            return ProfileFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val profileDetailsRequest = ProfileDetailsRequest()
        profileDetailsRequest.loginUserId = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID)!!.toInt()
        profileDetailsRequest.profileUserId = TintApplication.instance.selectedUserId

        TintAPICall(context, this).makeTintApiCall(profileDetailsRequest, ServiceType.PROFILE_DETAILS)

        ivBack.visibility = View.GONE
        tvTitle.visibility = View.VISIBLE
        ivMore.visibility = View.VISIBLE

        ivProfile = view.findViewById(R.id.ivProfile)
        tvFollowing = view.findViewById(R.id.tvFollowing)
        tvFollowers = view.findViewById(R.id.tvFollowers)
        btnMessage = view.findViewById(R.id.btnMessage)
        ivAddFriend = view.findViewById(R.id.ivAddFrend)
        tvDisplayName = view.findViewById(R.id.tvDisplayName)
        tvDescription = view.findViewById(R.id.tvDescription)
        tvVideosCount = view.findViewById(R.id.tvVideos)

        val tabLayout = view.findViewById(R.id.tab_layout) as TabLayout

        if(TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID)!!.toInt() == TintApplication.instance.selectedUserId) {

            ivAddFriend!!.visibility = View.GONE
            btnMessage!!.text = getString(R.string.edit_profile)

            tabLayout.addTab(tabLayout.newTab().setIcon(R.mipmap.ic_history))
            tabLayout.addTab(tabLayout.newTab().setIcon(R.mipmap.ic_save_inactive))
        } else {

            tabLayout.addTab(tabLayout.newTab().setIcon(R.mipmap.ic_history))
            appBar.visibility = View.GONE
            //tabLayout.addTab(tabLayout.newTab().setIcon(R.mipmap.ic_save_inactive))

        }

        val viewPager = view.findViewById(R.id.pager) as ViewPager
        val adapter = TabPagerAdapter(childFragmentManager, tabLayout.tabCount)
        viewPager.adapter = adapter

        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        @Suppress("DEPRECATION")
        tabLayout.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }

        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        ivMore.setOnClickListener(this)
        ivBack.setOnClickListener(this)

        ivAddFriend!!.setOnClickListener (this)

        textFollowing!!.setOnClickListener(this)

        textFollowers!!.setOnClickListener(this)

        btnMessage!!.setOnClickListener(this)

        tvFollowing!!.setOnClickListener (this)
        tvFollowers!!.setOnClickListener(this)

    }


    @SuppressLint("SetTextI18n")
    override fun onSuccessTintService(success: Any?, serviceType: Int) {

        if(success != null && isAdded) {

            when(serviceType) {

                ServiceType.PROFILE_DETAILS -> {

                    if(success is GetUserResponse) {

                        if(success.userDetails.imgUrl != null) {
                            Glide.with(context!!)
                                    .load(success.userDetails.imgUrl)
                                    //.apply(RequestOptions().placeholder(R.mipmap.ic_launcher))
                                    .into(ivProfile!!)
                        }

                        tvDisplayName!!.text = success.userDetails.name
                        tvTitle.text = success.userDetails.userName
                        tvFollowing!!.text = ""+success.followingCount
                        tvFollowers!!.text = ""+success.followersCount



                        if(success.userDetails!!.userName != null)
                            TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_USER_NAME, success.userDetails!!.userName)
                        TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_USER_ID, "" + success.userDetails!!.userId)

                        if(success.userDetails!!.name != null)
                            TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_NAME, success.userDetails!!.name)
//
//                        if(success.userDetails!!.email != null)
//                            TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_USER_EMAIL, success.userDetails!!.email)

                        if(success.userDetails!!.imgUrl != null)
                            TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_USER_PROFILE_PIC, success.userDetails!!.imgUrl)
//
//                        if(success.userDetails!!.phone != null)
//                            TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_MOB, success.userData!!.phone)



                    }

                }

                ServiceType.FOLLOW_USER -> {
                    if(success is FollowResponse) {

                        Log.d("FOLLOW_USER", ""+ success.followedUserId )
                    } else {

                        ivAddFriend!!.visibility = View.VISIBLE
                    }
                }
            }

        }

    }

    override fun onErrorTintService(error: Any?, serviceType: Int) {



    }


    private fun showBottomSheetDialogFragment() {
        val bottomSheetFragment = BottomSheetFragment(context!!, this)
        bottomSheetFragment.show(childFragmentManager, bottomSheetFragment.tag)
    }


    override fun onSelectedBottomSheet(code: Int) {
        Log.d("code", ""+ code)
        when(code) {

            Constants.BTM_DLG_SHARE_PROFILE -> {

            }

            Constants.BTM_DLG_SETTINGS -> {

                (activity as MainActivity).loadFragment(PrivacyPolicyFragment.newInstance(), "")

            }

        }

    }

    override fun onClick(v: View?) {

        if(v!!.id == R.id.ivMore) {

            showBottomSheetDialogFragment()

        }

        else if(v.id == R.id.textFollowing || v.id == R.id.tvFollowing) {

            val countF = tvFollowing!!.text

            if(countF != null && countF.toString().toInt() > 0) {

                TintApplication.instance.isFollowers = false

                (activity as MainActivity).loadFragment(FollowersFollowingFragment.newInstance(), "")

            }


        } else if(v.id == R.id.textFollowers || v.id == R.id.tvFollowers) {


            val countF = tvFollowers!!.text

            if(countF != null && countF.toString().toInt() > 0) {

                TintApplication.instance.isFollowers = true

                (activity as MainActivity).loadFragment(FollowersFollowingFragment.newInstance(), "")
            }

        } else if(v.id == R.id.ivAddFrend) {

            val followUserRequest = FollowUserRequest()
            followUserRequest.followerUserDetails = FollowerUserDetails()
            followUserRequest.followerUserDetails.userId = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID)

            followUserRequest.followedUserDetails = FollowedUserDetails()
            followUserRequest.followedUserDetails.userId = ""+TintApplication.instance.selectedUserId

            TintAPICall(context, this).makeTintApiCall(followUserRequest, ServiceType.FOLLOW_USER)

        }

        else if(v.id == R.id.btnMessage) {

            val btnText = btnMessage!!.text.toString()

            if(btnText.equals("Message", ignoreCase = true)) {

                launchActivity(context!!, ChatActivity::class.java, isFinish = false, isClearBack = false, isAnimLeft =true)


            } else {

                (activity as MainActivity).loadFragment(EditProfileFragment.newInstance(), "")

            }

        } else {

            (activity as MainActivity).onBackFragmentPress()
        }

    }


    override fun onDestroyView() {
        super.onDestroyView()

         ivProfile = null
         tvFollowing = null
         tvFollowers = null
         btnMessage = null
         ivAddFriend = null
         tvDisplayName = null
         tvDescription = null
    }


    @SuppressLint("SetTextI18n")
    override fun onVideoCountSet(count: Int) {

        tvVideosCount!!.text = ""+count

    }


}