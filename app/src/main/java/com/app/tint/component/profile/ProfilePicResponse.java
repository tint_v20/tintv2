package com.app.tint.component.profile;

import com.google.gson.annotations.SerializedName;

public class ProfilePicResponse {

	@SerializedName("imageUrl")
	private String imageUrl;

	@SerializedName("status")
	private String status;

	public void setImageUrl(String imageUrl){
		this.imageUrl = imageUrl;
	}

	public String getImageUrl(){
		return imageUrl;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ProfilePicResponse{" +
			"imageUrl = '" + imageUrl + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}