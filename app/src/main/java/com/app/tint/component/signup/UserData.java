package com.app.tint.component.signup;


import com.google.gson.annotations.SerializedName;


public class UserData{

	@SerializedName("imgUrl")
	private String imgUrl;

	@SerializedName("pass")
	private String pass;

	@SerializedName("phone")
	private String phone;

	@SerializedName("name")
	private String name;

	@SerializedName("userType")
	private String userType;

	@SerializedName("userName")
	private String userName;

	@SerializedName("userId")
	private int userId;

	@SerializedName("email")
	private String email;

	public void setPass(String pass) {
		this.pass = pass;
	}

	public void setImgUrl(String imgUrl){
		this.imgUrl = imgUrl;
	}

	public String getImgUrl(){
		return imgUrl;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setUserType(String userType){
		this.userType = userType;
	}

	public String getUserType(){
		return userType;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	@Override
 	public String toString(){
		return 
			"UserData{" + 
			"imgUrl = '" + imgUrl + '\'' + 
			",phone = '" + phone + '\'' + 
			",name = '" + name + '\'' + 
			",userType = '" + userType + '\'' + 
			",userName = '" + userName + '\'' +
			",userId = '" + userId + '\'' +
					",pass = '" + pass + '\'' +
					",email = '" + email + '\'' +
			"}";
		}
}