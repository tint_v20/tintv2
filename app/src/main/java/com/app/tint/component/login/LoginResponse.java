package com.app.tint.component.login;


import com.google.gson.annotations.SerializedName;

public class LoginResponse {

	@SerializedName("userData")
	private UserData userData;

	@SerializedName("status")
	private String status;

	public void setUserData(UserData userData){
		this.userData = userData;
	}

	public UserData getUserData(){
		return userData;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"LoginResponse{" +
			"userData = '" + userData + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}