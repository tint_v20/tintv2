package com.app.tint.component.videos.videostream;

import com.google.gson.annotations.SerializedName;

public class StartStreamVideoRequest{

	@SerializedName("urlName")
	private String urlName;

	@SerializedName("videoDescription")
	private String videoDescription;

	@SerializedName("videoName")
	private String videoName;

	@SerializedName("userDetails")
	private UserDetails userDetails;

	public void setUrlName(String urlName){
		this.urlName = urlName;
	}

	public String getUrlName(){
		return urlName;
	}

	public void setVideoDescription(String videoDescription){
		this.videoDescription = videoDescription;
	}

	public String getVideoDescription(){
		return videoDescription;
	}

	public void setVideoName(String videoName){
		this.videoName = videoName;
	}

	public String getVideoName(){
		return videoName;
	}

	public void setUserDetails(UserDetails userDetails){
		this.userDetails = userDetails;
	}

	public UserDetails getUserDetails(){
		return userDetails;
	}

	@Override
 	public String toString(){
		return 
			"StartStreamVideoRequest{" + 
			"urlName = '" + urlName + '\'' + 
			",videoDescription = '" + videoDescription + '\'' + 
			",videoName = '" + videoName + '\'' + 
			",userDetails = '" + userDetails + '\'' + 
			"}";
		}
}