package com.app.tint.component.videos.comments.request;

import com.google.gson.annotations.SerializedName;


public class UserLiveVideos{

	@SerializedName("id")
	private String id;

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"UserLiveVideos{" + 
			"id = '" + id + '\'' + 
			"}";
		}
}