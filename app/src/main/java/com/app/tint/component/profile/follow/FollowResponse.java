package com.app.tint.component.profile.follow;

import com.google.gson.annotations.SerializedName;

public class FollowResponse{

	@SerializedName("followedDetails")
	private FollowedDetails followedDetails;

	@SerializedName("followerUserId")
	private int followerUserId;

	@SerializedName("followerDetails")
	private FollowerDetails followerDetails;

	@SerializedName("followedUserId")
	private int followedUserId;

	@SerializedName("status")
	private String status;

	public void setFollowedDetails(FollowedDetails followedDetails){
		this.followedDetails = followedDetails;
	}

	public FollowedDetails getFollowedDetails(){
		return followedDetails;
	}

	public void setFollowerUserId(int followerUserId){
		this.followerUserId = followerUserId;
	}

	public int getFollowerUserId(){
		return followerUserId;
	}

	public void setFollowerDetails(FollowerDetails followerDetails){
		this.followerDetails = followerDetails;
	}

	public FollowerDetails getFollowerDetails(){
		return followerDetails;
	}

	public void setFollowedUserId(int followedUserId){
		this.followedUserId = followedUserId;
	}

	public int getFollowedUserId(){
		return followedUserId;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"FollowResponse{" + 
			"followedDetails = '" + followedDetails + '\'' + 
			",followerUserId = '" + followerUserId + '\'' + 
			",followerDetails = '" + followerDetails + '\'' + 
			",followedUserId = '" + followedUserId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}