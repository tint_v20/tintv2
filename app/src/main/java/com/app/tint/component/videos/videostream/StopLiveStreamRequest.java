package com.app.tint.component.videos.videostream;

import com.google.gson.annotations.SerializedName;

public class StopLiveStreamRequest{

	@SerializedName("id")
	private String id;

	@SerializedName("userDetails")
	private UserDetails userDetails;

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setUserDetails(UserDetails userDetails){
		this.userDetails = userDetails;
	}

	public UserDetails getUserDetails(){
		return userDetails;
	}

	@Override
 	public String toString(){
		return 
			"StopLiveStreamRequest{" + 
			"id = '" + id + '\'' + 
			",userDetails = '" + userDetails + '\'' + 
			"}";
		}
}