package com.app.tint.component.profile.follow;

import com.google.gson.annotations.SerializedName;

public class FollowerUserDetails{

	@SerializedName("userId")
	private String userId;

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	@Override
 	public String toString(){
		return 
			"FollowerUserDetails{" + 
			"userId = '" + userId + '\'' + 
			"}";
		}
}