package com.app.tint.component.videos.videostream;

import com.google.gson.annotations.SerializedName;

public class StopLiveStreamResponse{

	@SerializedName("videoId")
	private int videoId;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public void setVideoId(int videoId){
		this.videoId = videoId;
	}

	public int getVideoId(){
		return videoId;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"StopLiveStreamResponse{" + 
			"videoId = '" + videoId + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}