package com.app.tint.component.videos

import android.net.Uri
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.animation.AnimationUtils
import android.webkit.URLUtil
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.SeekBar
import android.widget.Toast
import android.widget.VideoView

import com.app.tint.R
import com.app.tint.utility.TintSingleton

import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.URL
import java.util.Timer
import java.util.TimerTask

open class FullViewPlayer : AppCompatActivity(), SeekBar.OnSeekBarChangeListener {

    internal var SrcPath: String? = null//"rtsp://202.146.92.42/ANIMAXVOD/Lunar_Legend_Ep2_4_114.3gp";
    protected var duration: Int = 0
    protected var mVideoView: VideoView? = null
    protected var videoViewLayout: FrameLayout? = null
    protected var mPlay: ImageButton? = null
    // private ImageButton mPause;
    protected var mReset: ImageButton? = null
    protected var mStop: ImageButton? = null
    protected var current: String? = null
    private var controlsLinearLayout: LinearLayout? = null
    protected var mPogressBar: ProgressBar? = null
    private var timer: Timer? = null
    private var timerTask: TimerTask? = null
    protected var seekBar: SeekBar? = null
    protected var _doesNextActivityStarted: Boolean = false

    private var seekbarTimerTask: TimerTask? = null
    private var seekBarTimer: Timer? = null

    private var isPrefetched: Boolean = false

    internal var isTouchedOnceAgain = false
    override fun onCreate(icicle: Bundle?) {
        super.onCreate(icicle)
        setContentView(R.layout.activity_full_player)
        mVideoView = findViewById<View>(R.id.video_view) as VideoView
        duration = -1
        mPogressBar = findViewById<View>(R.id.progressbar_video) as ProgressBar
        //loadingScreenImageView = (ImageView)findViewById(R.id.iv_loading_screen);
        controlsLinearLayout = findViewById<View>(R.id.linearLayout1) as LinearLayout
        videoViewLayout = findViewById<View>(R.id.videoframelayout) as FrameLayout
        //   loadingScreenImageView.setVisibility(View.GONE);
        mPogressBar!!.visibility = View.VISIBLE
        //  }
        controlsLinearLayout!!.animation = AnimationUtils.loadAnimation(this, R.anim.alpha)

        mPlay = findViewById<View>(R.id.play) as ImageButton
        mReset = findViewById<View>(R.id.reset) as ImageButton
        mStop = findViewById<View>(R.id.stop) as ImageButton

        val playDrawable = ContextCompat.getDrawable(this, R.mipmap.ic_upload_tab)//getResources().getDrawable(R.drawable.play);
        val pauseDrawable = ContextCompat.getDrawable(this, R.drawable.ic_play_stream)//getResources().getDrawable(R.drawable.pause);
        mPlay!!.setImageDrawable(pauseDrawable)
        mVideoView!!.setOnPreparedListener { mp ->
            // TODO Auto-generated method stub
            //if(Constants.LOG)
            Log.d("mp.getDuration() : ", "" + mp.duration)
            isPrefetched = true
            //    loadingScreenImageView.setVisibility(View.GONE);
            mPogressBar!!.visibility = View.GONE
        }


        //  mVideoView.setO
        mPlay!!.setOnClickListener {
            if (mVideoView != null && mVideoView!!.isPlaying) {
                mPlay!!.setImageDrawable(playDrawable)
                mVideoView!!.pause()
            } else {
                mPlay!!.setImageDrawable(pauseDrawable)
                playVideo()
            }
        }



        mReset!!.setOnClickListener {
            if (mVideoView != null && mVideoView!!.isPlaying) {
                mVideoView!!.seekTo(0)
            } else {
                current = null
                playVideo()
            }
        }
        mStop!!.setOnClickListener {
            if (mVideoView != null) {
                current = null
                mVideoView!!.stopPlayback()
            }
        }
        seekBar = findViewById<View>(R.id.seekBar1) as SeekBar
        seekBar!!.setOnSeekBarChangeListener(this)




        timer = Timer()
        timerTask = object : TimerTask() {


            override fun run() {
                // TODO Auto-generated method stub
                runOnUiThread {
                    // TODO Auto-generated method stub
                    if (mVideoView != null) {
                        duration = mVideoView!!.duration
                        val percent = mVideoView!!.bufferPercentage
                        //       if(Constants.LOG)Log.d(" mVideoView.getBufferPercentage() : ", ""+mVideoView.getBufferPercentage());

                        if (seekBar!!.secondaryProgress != 100 && percent != 100 && duration != -1)
                            seekBar!!.secondaryProgress = percent
                    }
                    //      if(Constants.LOG)Log.d(" Intimer task before ifisTouchedOnceAgain : ", ""+isTouchedOnceAgain);
                    if (!isTouchedOnceAgain) {

                        setVisibilityOfControls(View.GONE)

                    } else {

                        isTouchedOnceAgain = false
                    }
                }
            }
        }
        timer!!.scheduleAtFixedRate(timerTask, 3000, 3000)

        seekBarTimer = Timer()
        seekbarTimerTask = object : TimerTask() {


            override fun run() {
                // TODO Auto-generated method stub
                runOnUiThread {
                    // TODO Auto-generated method stub
                    if (mVideoView != null) {
                        //       if(Constants.LOG)Log.d(" mVideoView.getDuration() : "+ mVideoView.getDuration(), "  mVideoView.getBufferPercentage() : "+mVideoView.getBufferPercentage()+" mVideoView.getCurrentPosition() : "+mVideoView.getCurrentPosition());
                        duration = mVideoView!!.duration
                        val percent = mVideoView!!.bufferPercentage

                        if (mVideoView!!.currentPosition != -1 && duration != -1) {
                            seekBar!!.secondaryProgress = percent
                            seekBar!!.progress = (mVideoView!!.currentPosition.toFloat() / duration * 100).toInt()
                        }
                    }
                }
            }
        }
        seekBarTimer!!.scheduleAtFixedRate(seekbarTimerTask, 1000, 1000)
    }

    private fun playVideo() {
        try {
            val path = TintSingleton.getInstance().videoURL
            //if(Constants.LOG)
            Log.d(TAG, "path: " + path!!)
            if (path == null || path.length == 0) {
                Toast.makeText(this@FullViewPlayer, "Constants.UrlEmpty",
                        Toast.LENGTH_LONG).show()

            } else {
                // If the path has not changed, just start the media player
                if (path == current && mVideoView != null) {
                    mVideoView!!.start()
                    mVideoView!!.requestFocus()

                    return
                }
                current = path
                mVideoView!!.setVideoURI(Uri.parse(path))

                //    mVideoView.setVideoPath(getDataSource(path));
                mVideoView!!.start()
                mVideoView!!.requestFocus()
                duration = mVideoView!!.duration

                //if(Constants.LOG)
                Log.d("duration", "" + duration)

            }
        } catch (e: Exception) {
            //if(Constants.LOG)
            Log.d(TAG, "error: " + e.message, e)
            if (mVideoView != null) {
                mVideoView!!.stopPlayback()
            }

        }

    }

    private fun getDataSource(path: String): String? {
        try {
            if (!URLUtil.isNetworkUrl(path)) {
                return path
            } else {
                val url = URL(path)
                val cn = url.openConnection()
                cn.connect()
                val stream = cn.getInputStream() ?: throw RuntimeException("stream is null")
                val temp = File.createTempFile("mediaplayertmp", "dat")
                temp.deleteOnExit()
                val tempPath = temp.absolutePath
                val out = FileOutputStream(temp)
                val buf = ByteArray(128)
                do {
                    val numread = stream.read(buf)
                    if (numread <= 0)
                        break
                    out.write(buf, 0, numread)
                } while (true)
                try {
                    stream.close()
                } catch (ex: IOException) {
                    //if(Constants.LOG)
                    Log.e(TAG, "error: " + ex.message, ex)
                }

                return tempPath
            }
        } catch (e: Exception) {
            // TODO: handle exception
            return null
        }

    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        // TODO Auto-generated method stub
        //  if(Constants.LOG)Log.d("In Activity touch listenr"," controlsLinearLayout.getVisibility() : "+controlsLinearLayout.getVisibility() );

        if (controlsLinearLayout!!.visibility == View.GONE) {

            setVisibilityOfControls(View.VISIBLE)

        }
        isTouchedOnceAgain = true


        return if (isPrefetched) {

            super.onTouchEvent(event)
        } else {

            false
        }
    }

    private fun setVisibilityOfControls(visibility: Int) {
        // TODO Auto-generated method stub
        if(seekBar != null && controlsLinearLayout != null && mPlay != null && mStop != null) {
            seekBar!!.visibility = visibility
            controlsLinearLayout!!.visibility = visibility
            mPlay!!.visibility = visibility
            //mReset.visibility = visibility
            mStop!!.visibility = visibility
        }

    }


    override fun onProgressChanged(seekBar: SeekBar, progress: Int,
                                   fromUser: Boolean) {
        // TODO Auto-generated method stub
        /*if(mVideoView.getCurrentPosition()!=-1){
   if(Constants.LOG)Log.d("onProgressChanged : seekbar position changing to ", ""+((float)mVideoView.getCurrentPosition()/duration)*100);
   seekBar.setProgress((int)(((float)mVideoView.getCurrentPosition()/duration)*100));
  }*/
    }


    override fun onStartTrackingTouch(seekBar: SeekBar) {
        // TODO Auto-generated method stub

    }


    override fun onStopTrackingTouch(seekBar: SeekBar) {
        // TODO Auto-generated method stub
        val progress = seekBar.progress
        if (duration != 0 && duration != -1) {
            //if(Constants.LOG)
            Log.d("onStopTrackingTouch : Seeking to :", "" + (duration * progress.toFloat() / 100).toInt())
            mVideoView!!.seekTo((duration * progress.toFloat() / 100).toInt())
        }

    }

    protected fun startPlaying(URL: String) {
        // TODO Auto-generated method stub
        SrcPath = URL

        runOnUiThread { playVideo() }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        // TODO Auto-generated method stub
        super.onRestoreInstanceState(savedInstanceState)
    }

    companion object {


        private val TAG = "VideoViewDemo"
    }

    override fun onDestroy() {
        super.onDestroy()

        SrcPath = null//"rtsp://202.146.92.42/ANIMAXVOD/Lunar_Legend_Ep2_4_114.3gp";
        duration = 0
        mVideoView = null
        videoViewLayout = null
        mPlay = null
        mReset = null
        mStop = null
        current = null
        controlsLinearLayout = null
        mPogressBar = null
        timer = null
        timerTask = null
        seekBar = null

        seekbarTimerTask = null
        seekBarTimer = null
    }
}