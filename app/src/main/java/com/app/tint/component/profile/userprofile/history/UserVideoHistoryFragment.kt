package com.app.tint.component.profile.userprofile.history

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.services.TintAPICall
import com.app.tint.services.TintServiceResponseCallback
import com.app.tint.utility.Constants
import com.app.tint.utility.ServiceType
import kotlinx.android.synthetic.main.fragment_history_video.*
import org.jetbrains.anko.toast


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [UserUploadFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [UserUploadFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class UserVideoHistoryFragment : Fragment(), TintServiceResponseCallback {


    private lateinit var historyListItem: MutableList<UserVideosItem>
    private lateinit var rvHistory: RecyclerView

    private lateinit var historyVideosAdapter: HistoryVideosAdapter
    private var isLoadMoreServiceHit: Boolean = false
    private var pastVisibleItems: Int = 0
    private var visibleItemCount:Int = 0
    private var totalItemCount:Int = 0
    private lateinit var gridLayoutManager: GridLayoutManager
    private var isPaginationAvailable: Boolean = false
    private var perPage: Int = 0
    private var onVideoCountUpdateSetListener: OnVideoCountUpdateSetListener? = null

    companion object {

        fun newInstance(): UserVideoHistoryFragment {
            return UserVideoHistoryFragment()
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        historyListItem = ArrayList()
        gridLayoutManager = GridLayoutManager(context, 3)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history_video, container, false)
    }

    // TODO: Rename method, update argument and hook method into UI event

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvHistory = view.findViewById(R.id.rvHistory)
        rvHistory.setHasFixedSize(true)
        rvHistory.layoutManager = gridLayoutManager
        // rvVideos.addItemDecoration(GridSpacingItemDecoration(dpToPx(10)))
        rvHistory.itemAnimator = DefaultItemAnimator()

        callHistoryService(false)




        //todo Pagination

        rvHistory.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            var isScrollUp = false

//            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
//                if (newState == RecyclerView.SCROLL_STATE_DRAGGING || newState == RecyclerView.SCROLL_STATE_SETTLING || newState == RecyclerView.SCROLL_STATE_IDLE) {
//                    homeAdapter1.onScrolled(recyclerView)
//                }
//
//               if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
//                   // Do something
//                    Log.d("newState", "SCROLL_STATE_TOUCH_SCROLL $TintSingleton.instance.isPaginationAvailable $isScrollUp")
//                    if (!isScrollUp && isPaginationAvailable) {
//                        isPaginationAvailable = false
//
//                        callHomeService(isLoadMore = true)
//                    }
//                }
//            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                if (dy >= 0)
                //check for scroll down
                {

                    isScrollUp = false
                    visibleItemCount = gridLayoutManager.childCount
                    totalItemCount = gridLayoutManager.itemCount
                    pastVisibleItems = gridLayoutManager.findFirstVisibleItemPosition()

                    if (isPaginationAvailable) {
                        if (visibleItemCount + pastVisibleItems >= totalItemCount) {
                            isPaginationAvailable = false
                            Log.d("...", "Last Item Wow !")
                            //Do pagination.. i.e. fetch new data
                            rvHistory.post(Runnable {
                                callHistoryService(isLoadMore = true)
                            })
                        }
                    }
                } else if (dy < 0) {
                    // Recycle view scrolling up...
                    //loading = true
                    isScrollUp = true
                }



//                    else {
//                        if (!isScrollUp && isPaginationAvailable) {
//                            Log.d("elseRun", "else")
//                            mRecyclerView.post(Runnable {
//                                callHomeService(isLoadMore = true)
//                            })
//                        }
//                    }
            }

        })
    }


    private fun callHistoryService(isLoadMore: Boolean) {
        if(isLoadMore) {
            isLoadMoreServiceHit = true
            perPage += 5
            Log.d("perPage", ""+ perPage)
            historyListItem.add(UserVideosItem("load"))
            historyVideosAdapter.notifyItemInserted(historyListItem.size - 1)


            val videoRequest = UserVideoHistoryRequest()
            videoRequest.videoUserId = TintApplication.instance.selectedUserId
            videoRequest.start = perPage
            videoRequest.userId = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID)!!.toInt()


            TintAPICall(context, this).makeTintApiCall(videoRequest, ServiceType.VIDEO_HISTORY)
        } else {

            loader.visibility = View.VISIBLE

            val videoRequest = UserVideoHistoryRequest()
            videoRequest.videoUserId = TintApplication.instance.selectedUserId
            videoRequest.start = 0
            videoRequest.userId = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID)!!.toInt()

            TintAPICall(context, this).makeTintApiCall(videoRequest, ServiceType.VIDEO_HISTORY)
        }

    }



    override fun onSuccessTintService(success: Any?, serviceType: Int) {

        if(isAdded && success != null) {

            when(serviceType) {

                ServiceType.VIDEO_HISTORY -> {

                    loader.visibility = View.GONE

                    if(success is VideoHistoryResponse) {

                        if(isLoadMoreServiceHit) {

                            if (success.userVideos.size > 0) {

                                historyListItem.removeAt(historyListItem.size - 1)

                                historyListItem.addAll(success.userVideos)
                                historyVideosAdapter.updateData(historyListItem)


                            } else {//result size 0 means there is no more data available at server
                                historyVideosAdapter.setMoreDataAvailable(false)
                                //telling adapter to stop calling load more as no more server data available
                                //Toast.makeText(context,"No More Data Available",Toast.LENGTH_LONG).show();
                            }

                        }
                        else {

                            historyListItem = success.userVideos

                            historyVideosAdapter = HistoryVideosAdapter(context!!, historyListItem)
                            rvHistory.adapter = historyVideosAdapter

                          //  TintSingleton.getInstance().historyVideoCount = historyListItem.size
                            isPaginationAvailable = success.end != -1
                        }

                        onVideoCountUpdateSetListener?.onVideoCountSet(historyListItem.size)
                    }

                }
            }
        }

    }

    override fun onErrorTintService(error: Any?, serviceType: Int) {

        if(error!= null && isAdded) {
            activity!!.toast(error.toString())
            loader.visibility = View.GONE
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        onAttachToParentFragment(parentFragment)
    }


    interface OnVideoCountUpdateSetListener {
        fun onVideoCountSet(count: Int)
    }


    // In the child fragment.
    private fun onAttachToParentFragment(fragment: Fragment?) {
        try {
            onVideoCountUpdateSetListener = fragment as OnVideoCountUpdateSetListener

        } catch (e: ClassCastException) {
            throw ClassCastException(
                    "$fragment must implement OnPlayerSelectionSetListener")
        }

    }
}
