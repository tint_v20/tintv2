package com.app.tint.component

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import android.util.Log
import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.app.BaseActivity
import com.app.tint.component.profile.ProfileFragment
import com.app.tint.component.search.SearchFragment
import com.app.tint.component.videos.views.SetViewRequest
import com.app.tint.component.videos.views.UserDetails
import com.app.tint.component.videos.views.UserLiveVideos
import com.app.tint.services.TintAPICall
import com.app.tint.services.TintServiceResponseCallback
import com.app.tint.utility.Constants
import com.app.tint.utility.ServiceType
import org.jetbrains.anko.toast
import android.content.Intent
import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.Context
import android.content.ServiceConnection
import android.os.IBinder
import com.app.tint.component.login.LoginActivity
import com.app.tint.utility.launchActivity
import android.preference.PreferenceManager
import com.app.tint.component.common.AlertDialogue
import com.app.tint.component.home.HomeFragment
import com.app.tint.component.openfire.xmpp.ChatService
import com.app.tint.component.openfire.xmpp.LocalBinder
import com.app.tint.component.profile.follow.FollowResponse
import com.app.tint.component.profile.follow.FollowUserRequest
import com.app.tint.component.profile.follow.FollowedUserDetails
import com.app.tint.component.profile.follow.FollowerUserDetails
import com.app.tint.component.videos.GoCoderSDKPrefs
import com.app.tint.utility.Constants.setVideoStreamName
import com.app.tint.utility.TintSingleton
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : BaseActivity(), TintServiceResponseCallback, AlertDialogue.OnDialogItemClick {

    private var backPressed: Long = 0
    private val TAG = "MainActivity"

    private var mBounded: Boolean = false
    private var mService: ChatService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bot_nav)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        //textMessage = findViewById(R.id.message)
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

        loadFragment(HomeFragment.newInstance(), "")

        GoCoderSDKPrefs.PrefsFragment()

        setVideoStreamName(this)

        if(TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID) != null) {

            val pref = applicationContext.getSharedPreferences("Login", Context.MODE_PRIVATE)
            val editor = pref.edit()
            editor.putString("user", TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID))
            editor.putString("pass", TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID))
            editor.apply()
            doBindService()
        }

    }


    private val mConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName,
                                        service: IBinder) {
            mService = (service as LocalBinder<ChatService>).getService()
            mBounded = true
            Log.d(TAG, "onServiceConnected")
        }

        override fun onServiceDisconnected(name: ComponentName) {
            mService = null
            mBounded = false
            Log.d(TAG, "onServiceDisconnected")
        }
    }


    private fun doBindService() {
        bindService(Intent(this, ChatService::class.java), mConnection,
                Context.BIND_AUTO_CREATE)
    }

    private fun doUnbindService() {
        if (mConnection != null) {
            unbindService(mConnection)
        }
    }

    fun getmService(): ChatService {
        return mService!!
    }


    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                //textMessage.setText(R.string.title_home)
                loadFragment(HomeFragment.newInstance(), "")
                TintApplication.instance.mFragment = HomeFragment.newInstance()
                setVideoStreamName(this)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_search -> {
                loadFragment(SearchFragment.newInstance(), "")
                TintApplication.instance.mFragment = SearchFragment.newInstance()

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {

                if(TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID) != null) {

                    loadFragment(ProfileFragment.newInstance(), "")
                    TintApplication.instance.selectedUserImage = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_PROFILE_PIC)
                    TintApplication.instance.mFragment = ProfileFragment.newInstance()
                    TintApplication.instance.selectedUserId = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID)!!.toInt()

                } else {
                    launchActivity(this, LoginActivity::class.java, isFinish = true, isClearBack = true, isAnimLeft =true)

                }

                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }


    fun onBackFragmentPress() {
        super.onBackPressed()
    }


     fun loadFragment(fragment: Fragment, tag: String) {

        // Pop off everything up to and including the current tab
        val fragmentManager = supportFragmentManager
        fragmentManager.popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE)

        // Add the new tab fragment
        fragmentManager.beginTransaction()
                .add(R.id.frameLayout, fragment)
                .addToBackStack("")
                .commit()

    }


    fun doLikeVideoService(videoId: Int) {

        if(TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID) != null) {
            if (videoId > 0) {

                val likeRequest = SetViewRequest()
                likeRequest.userDetails = UserDetails()
                likeRequest.userDetails.userId = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID)!!.toInt()
                likeRequest.userLiveVideos = UserLiveVideos()
                likeRequest.userLiveVideos.id = videoId

                TintAPICall(this, this).makeTintApiCall(likeRequest, ServiceType.LIKE_UNLIKE)
            }
        } else {
            launchActivity(this, LoginActivity::class.java, isFinish = true, isClearBack = true, isAnimLeft =true)
        }

    }

    fun doSetVideoCount(videoId: Int) {
        if(TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID) != null) {
            if (videoId > 0) {
                val setViewRequest = SetViewRequest()
                setViewRequest.userDetails = UserDetails()
                setViewRequest.userDetails.userId = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID)!!.toInt()

                setViewRequest.userLiveVideos = UserLiveVideos()
                setViewRequest.userLiveVideos.id = videoId
                TintAPICall(this, this).makeTintApiCall(setViewRequest, ServiceType.SET_VIEW_VIDEOS)
            }
        }

    }

    fun doShareVideo(videoLink: String) {

//        val file: File? = null // your code
//        val install = Intent(Intent.ACTION_VIEW)
//        install.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
//    // Old Approach
//    install.setDataAndType(Uri.fromFile(file), "mimetype")
//    // End Old approach
//    // New Approach
//    val apkURI = FileProvider.getUriForFile(
//this,
//            applicationContext
//            .packageName + ".provider", file!!)
//    install.setDataAndType(apkURI, "mimetype")
//    install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
//    // End New Approach
//    startActivity(install)

        try {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Sharing video link")
            //var shareMessage = "\nLet me recommend you this application\n\n"
            //shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n"
            shareIntent.putExtra(Intent.EXTRA_TEXT, videoLink)
            startActivity(Intent.createChooser(shareIntent, "choose one"))
        } catch (e: Exception) {
            //e.toString();
        }

    }

    fun doFavAddRemove(videoId: Int) {

        if(TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID) != null) {

            if (videoId > 0) {

                val setViewRequest = SetViewRequest()
                setViewRequest.userDetails = UserDetails()
                setViewRequest.userDetails.userId = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID)!!.toInt()

                setViewRequest.userLiveVideos = UserLiveVideos()
                setViewRequest.userLiveVideos.id = videoId
                TintAPICall(this, this).makeTintApiCall(setViewRequest, ServiceType.ADD_REMOVE_FAV)
            }
        } else {
            launchActivity(this, LoginActivity::class.java, isFinish = true, isClearBack = true, isAnimLeft =true)
        }

    }


    override fun onSuccessTintService(success: Any?, serviceType: Int) {

        if (success != null && !isFinishing) {

            when (serviceType) {

                ServiceType.LIKE_UNLIKE -> {

                    Log.d("LIKE_UNLIKE", "success")

                }

                ServiceType.ADD_REMOVE_FAV -> {

                    Log.d("ADD_REMOVE_FAV", "success")
                }

                ServiceType.SET_VIEW_VIDEOS -> {

                    Log.d("SET_VIEW_VIDEOS", "success")

                }
                ServiceType.FOLLOW_USER -> {
                    if(success is FollowResponse) {

                        Log.d("FOLLOW_USER", ""+ success.followedUserId )
                    }
                }
            }
        }
    }

    override fun onErrorTintService(error: Any?, serviceType: Int) {

        if (error != null) {
            //toast(error.toString())
        }

    }



    fun doFollowUser(selectedUserId: Int) {

        val followUserRequest = FollowUserRequest()
        followUserRequest.followerUserDetails = FollowerUserDetails()
        followUserRequest.followerUserDetails.userId = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID)

        followUserRequest.followedUserDetails = FollowedUserDetails()
        followUserRequest.followedUserDetails.userId = ""+selectedUserId

        TintAPICall(this, this).makeTintApiCall(followUserRequest, ServiceType.FOLLOW_USER)

    }


//    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
//        if (ev!!.action == MotionEvent.ACTION_UP) {
//            val view = currentFocus
//
//            if (view != null) {
//                val consumed = super.dispatchTouchEvent(ev)
//
//                val viewTmp = currentFocus
//                val viewNew = viewTmp ?: view
//
//                if (viewNew == view) {
//                    val rect = Rect()
//                    val coordinates = IntArray(2)
//
//                    view.getLocationOnScreen(coordinates)
//
//                    rect.set(coordinates[0], coordinates[1], coordinates[0] + view.width, coordinates[1] + view.height)
//
//                    val x = ev.x.toInt()
//                    val y = ev.y.toInt()
//
//                    if (rect.contains(x, y)) {
//                        return consumed
//                    }
//                } else if (viewNew is EditText) {
//                    return consumed
//                }
//
//                val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//
//                inputMethodManager.hideSoftInputFromWindow(viewNew.windowToken, 0)
//
//                viewNew.clearFocus()
//
//                return consumed
//            }
//        }
//
//        return super.dispatchTouchEvent(ev)
//    }

    override fun onDestroy() {
        super.onDestroy()

        TintSingleton.getInstance().sliderVideosList.clear()

        if(TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID) != null)
        doUnbindService()
    }

    override fun onBackPressed() {
       // super.onBackPressed()
        setVideoStreamName(this)
        if (backPressed + 2000 > System.currentTimeMillis()) {
            super.onBackPressed()
            this@MainActivity.finish()
        }
        else toast("Press once again to exit!")
        backPressed = System.currentTimeMillis()
    }

    override fun onButtonClick(code: Int) {

    }
}
