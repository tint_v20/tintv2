package com.app.tint.component.permisssions

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.app.BaseActivity
import com.app.tint.component.videos.CameraActivity
import com.app.tint.utility.Constants
import com.app.tint.utility.launchActivity
import kotlinx.android.synthetic.main.activity_permissions.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import lolodev.permissionswrapper.callback.OnRequestPermissionsCallBack
import lolodev.permissionswrapper.wrapper.PermissionWrapper
import org.jetbrains.anko.toast

class PermissionsActivity: BaseActivity() {

    //private var isCameraPermission = false
    //private var isMicrophonePermission = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_permissions)


        if(TintApplication.instance.sharedPreferences.getValue(Constants.IS_CAMERA_PERMISSION_GRANTED) == null
                && TintApplication.instance.sharedPreferences.getValue(Constants.IS_MICROPHONE_PERMISSION_GRANTED) == null) {
            rlPermission.visibility = View.VISIBLE
            tvAllowCamera.visibility = View.VISIBLE
            tvMicrophone.visibility = View.VISIBLE

        } else if(TintApplication.instance.sharedPreferences.getValue(Constants.IS_MICROPHONE_PERMISSION_GRANTED) == null) {
            rlPermission.visibility = View.VISIBLE
            tvMicrophone.visibility = View.VISIBLE
            //tvMicrophone.setOnClickListener { getMicrophonePermission() }

        } else if(TintApplication.instance.sharedPreferences.getValue(Constants.IS_CAMERA_PERMISSION_GRANTED) == null) {
            rlPermission.visibility = View.VISIBLE
            tvAllowCamera.visibility = View.VISIBLE
           // tvAllowCamera.setOnClickListener { getMicrophonePermission() }

        } else {

            //todo All permission granted todo Camera Live
            rlPermission.visibility = View.GONE
        }


        tvAllowCamera.setOnClickListener {
            getCameraPermission()
        }

        tvMicrophone.setOnClickListener {
            getMicrophonePermission()
        }


    }


    companion object {

        private fun hasPermission(context: Context, permission: String): Boolean {

            val res = context.checkCallingOrSelfPermission(permission)

            Log.v("Permissions", "permission: " + permission + " = \t\t" +
                    if (res == PackageManager.PERMISSION_GRANTED) "GRANTED" else "DENIED")

            return res == PackageManager.PERMISSION_GRANTED

        }

        /**
         * Determines if the context calling has the required permissions
         *
         * @param context     - the IPC context
         * @param permissions - The permissions to check
         * @return true if the IPC has the granted permission
         */
        fun hasPermissions(context: Context, vararg permissions: String): Boolean {

            var hasAllPermissions = true

            for (permission in permissions) {
                //return false instead of assigning, but with this you can log all permission values
                if (!hasPermission(context, permission)) {
                    hasAllPermissions = false
                }
            }

            return hasAllPermissions

        }
    }



    private fun getCameraPermission() {

        val permissions = arrayOf(Manifest.permission.INTERNET, Manifest.permission.CAMERA)
        if (!hasPermissions(this, *permissions)) {
            PermissionWrapper.Builder(this)
                    .addPermissions(permissions)
                    //enable rationale message with a custom message
                    .addPermissionRationale("Please allow Camera permission for the app to function properly")
                    //show settings dialog,in this case with default message base on requested permission/s
                    .addPermissionsGoSettings(true)
                    //enable callback to know what option was choosed
                    .addRequestPermissionsCallBack(object : OnRequestPermissionsCallBack {
                        override fun onGrant() {
                            Log.i(PermissionsActivity::class.java.simpleName, "Permission was granted.")

                            TintApplication.instance.sharedPreferences.save(Constants.IS_CAMERA_PERMISSION_GRANTED, "yes")
                            tvAllowCamera.visibility = View.GONE
                           // isCameraPermission = true
                            if(TintApplication.instance.sharedPreferences.getValue(Constants.IS_MICROPHONE_PERMISSION_GRANTED) != null) {

                                //todo Camera Live
                                launchActivity(this@PermissionsActivity, CameraActivity::class.java, isFinish = true, isClearBack = false, isAnimLeft =true)

                            }

                        }

                        override fun onDenied(permission: String) {
                            Log.i(PermissionsActivity::class.java.simpleName, "Permission was not granted.")
                            toast("Please provide Camera permission granted")
                        }
                    }).build().request()
        } else {

           //already permission granted
            toast("already permission granted")
        }
    }


    private fun getMicrophonePermission() {

        val permissions = arrayOf( Manifest.permission.RECORD_AUDIO)
        if (!hasPermissions(this, *permissions)) {
            PermissionWrapper.Builder(this)
                    .addPermissions(permissions)
                    //enable rationale message with a custom message
                    .addPermissionRationale("Please allow Audio permission for the app to function properly")
                    //show settings dialog,in this case with default message base on requested permission/s
                    .addPermissionsGoSettings(true)
                    //enable callback to know what option was choosed
                    .addRequestPermissionsCallBack(object : OnRequestPermissionsCallBack {
                        override fun onGrant() {
                            Log.i(PermissionsActivity::class.java.simpleName, "Permission was granted.")

                            TintApplication.instance.sharedPreferences.save(Constants.IS_MICROPHONE_PERMISSION_GRANTED, "yes")
                            tvMicrophone.visibility = View.GONE
                            //isMicrophonePermission = true
                            if(TintApplication.instance.sharedPreferences.getValue(Constants.IS_CAMERA_PERMISSION_GRANTED) != null) {
                                //todo Camera Live
                                launchActivity(this@PermissionsActivity, CameraActivity::class.java, isFinish = true, isClearBack = false, isAnimLeft =true)

                            }

                        }

                        override fun onDenied(permission: String) {
                            Log.i(PermissionsActivity::class.java.simpleName, "Permission was not granted.")
                            toast("Please provide Audio permission granted")
                        }
                    }).build().request()
        } else {

            //already permission granted
            toast("already permission granted")
        }
    }
}