/**
 *
 * This is sample code provided by Wowza Media Systems, LLC.  All sample code is intended to be a reference for the
 * purpose of educating developers, and is not intended to be used in any production environment.
 *
 * IN NO EVENT SHALL WOWZA MEDIA SYSTEMS, LLC BE LIABLE TO YOU OR ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 * EVEN IF WOWZA MEDIA SYSTEMS, LLC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * WOWZA MEDIA SYSTEMS, LLC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. ALL CODE PROVIDED HEREUNDER IS PROVIDED "AS IS".
 * WOWZA MEDIA SYSTEMS, LLC HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 * © 2015 – 2019 Wowza Media Systems, LLC. All rights reserved.
 */

package com.app.tint.component.videos

import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.preference.PreferenceManager
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageButton
import android.widget.SeekBar
import android.widget.TextView

import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.app.GoCoderSDKActivityBase
import com.wowza.gocoder.sdk.api.WowzaGoCoder
import com.wowza.gocoder.sdk.api.configuration.WOWZMediaConfig
import com.wowza.gocoder.sdk.api.data.WOWZDataEvent
import com.wowza.gocoder.sdk.api.data.WOWZDataMap
import com.wowza.gocoder.sdk.api.errors.WOWZStreamingError
import com.wowza.gocoder.sdk.api.logging.WOWZLog
import com.wowza.gocoder.sdk.api.player.GlobalPlayerStateManager
import com.wowza.gocoder.sdk.api.player.WOWZPlayerConfig
import com.wowza.gocoder.sdk.api.player.WOWZPlayerView
import com.wowza.gocoder.sdk.api.status.WOWZState
import com.wowza.gocoder.sdk.api.status.WOWZStatus

class PlayerActivity : GoCoderSDKActivityBase() {

    // Stream player view
    private var mStreamPlayerView: WOWZPlayerView? = null
    private var mStreamPlayerConfig: WOWZPlayerConfig? = null

    // UI controls
    private var mBtnPlayStream: MultiStateButton? = null
    private var mBtnSettings: MultiStateButton? = null
    private var mBtnMic: MultiStateButton? = null
    private var mBtnScale: MultiStateButton? = null
    private var mSeekVolume: SeekBar? = null
    private var mBufferingDialog: ProgressDialog? = null
    private var mGoingDownDialog: ProgressDialog? = null
    private var mStatusView: StatusView? = null
    private var mHelp: TextView? = null
    private var mTimerView: TimerView? = null
    private var mStreamMetadata: ImageButton? = null
    private var mVolumeSettingChangeObserver: VolumeChangeObserver? = null
    private val isNetworkAvailable: Boolean
        get() {
            val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                    ?: return false
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stream_player)

        mRequiredPermissions = arrayOf()

        mStreamPlayerView = findViewById(R.id.vwStreamPlayer)

        mBtnPlayStream = findViewById(R.id.ic_play_stream)
        mBtnSettings = findViewById(R.id.ic_settings)
        mBtnMic = findViewById(R.id.ic_mic)
        mBtnScale = findViewById(R.id.ic_scale)

        mTimerView = findViewById(R.id.txtTimer)
        mStatusView = findViewById(R.id.statusView)
        mStreamMetadata = findViewById(R.id.imgBtnStreamInfo)
        mHelp = findViewById(R.id.streamPlayerHelp)

        mSeekVolume = findViewById(R.id.sb_volume)

        mTimerView!!.visibility = View.GONE



        if (TintApplication.instance.sGoCoderSDK != null) {

            /*
            Packet change listener setup
             */
            val activity = this
            val packetChangeListener = object : WOWZPlayerView.PacketThresholdChangeListener {
                override fun packetsBelowMinimumThreshold(packetCount: Int) {
                    WOWZLog.debug("Packets have fallen below threshold $packetCount... ")

                    //                    activity.runOnUiThread(new Runnable() {
                    //                        public void run() {
                    //                            Toast.makeText(activity, "Packets have fallen below threshold ... ", Toast.LENGTH_SHORT).show();
                    //                        }
                    //                    });
                }

                override fun packetsAboveMinimumThreshold(packetCount: Int) {
                    WOWZLog.debug("Packets have risen above threshold $packetCount ... ")

                    //                    activity.runOnUiThread(new Runnable() {
                    //                        public void run() {
                    //                            Toast.makeText(activity, "Packets have risen above threshold ... ", Toast.LENGTH_SHORT).show();
                    //                        }
                    //                    });
                }
            }
            mStreamPlayerView!!.setShowAllNotificationsWhenBelowThreshold(false)
            mStreamPlayerView!!.setMinimumPacketThreshold(20)
            mStreamPlayerView!!.registerPacketThresholdListener(packetChangeListener)
            ///// End packet change notification listener

            mTimerView!!.setTimerProvider(object : TimerView.TimerProvider {
                override fun getTimecode(): Long {
                    return mStreamPlayerView!!.currentTime
                }

                override fun getDuration(): Long {
                    return mStreamPlayerView!!.duration
                }
            })

            mSeekVolume!!.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    if (mStreamPlayerView != null && mStreamPlayerView!!.isPlaying) {
                        mStreamPlayerView!!.volume = progress
                    }
                }

                override fun onStartTrackingTouch(seekBar: SeekBar) {}

                override fun onStopTrackingTouch(seekBar: SeekBar) {}
            })

            // listen for volume changes from device buttons, etc.
            mVolumeSettingChangeObserver = VolumeChangeObserver(this, Handler())
            applicationContext.contentResolver.registerContentObserver(android.provider.Settings.System.CONTENT_URI, true, mVolumeSettingChangeObserver!!)
            mVolumeSettingChangeObserver!!.setVolumeChangeListener { previousLevel, currentLevel ->
                if (mSeekVolume != null)
                    mSeekVolume!!.progress = currentLevel

                if (mStreamPlayerView != null && mStreamPlayerView!!.isPlaying) {
                    mStreamPlayerView!!.volume = currentLevel
                }
            }

            mBtnScale!!.setState(mStreamPlayerView!!.scaleMode == WOWZMediaConfig.FILL_VIEW)

            // The streaming player configuration properties
            mStreamPlayerConfig = WOWZPlayerConfig()

            mBufferingDialog = ProgressDialog(this)
            mBufferingDialog!!.setTitle(R.string.status_buffering)
            mBufferingDialog!!.setMessage(resources.getString(R.string.msg_please_wait))
            mBufferingDialog!!.setButton(DialogInterface.BUTTON_NEGATIVE, resources.getString(R.string.button_cancel)) { dialogInterface, i -> cancelBuffering() }

            mGoingDownDialog = ProgressDialog(this)
            mGoingDownDialog!!.setTitle(R.string.status_buffering)
            mGoingDownDialog!!.setMessage("Please wait while the decoder is shutting down.")

            mStreamPlayerView!!.registerDataEventListener("onClientConnected") { eventName, eventParams ->
                WOWZLog.info(TAG, "onClientConnected data event received:\n" + eventParams.toString(true))

                Handler(Looper.getMainLooper()).post { }

                // this demonstrates how to return a function result back to the original Wowza Streaming Engine
                // function call request
                val functionResult = WOWZDataMap()
                functionResult.put("greeting", "Hello New Client!")

                functionResult
            }
            // testing player data event handler.
            mStreamPlayerView!!.registerDataEventListener("onWowzaData") { eventName, eventParams ->
                var meta = ""
                if (eventParams != null)
                    meta = eventParams.toString()


                WOWZLog.debug("onWZDataEvent -> eventName $eventName = $meta")

                null
            }

            // testing player data event handler.
            mStreamPlayerView!!.registerDataEventListener("onStatus") { eventName, eventParams ->
                if (eventParams != null)
                    WOWZLog.debug("onWZDataEvent -> eventName $eventName = $eventParams")

                null
            }

            // testing player data event handler.
            mStreamPlayerView!!.registerDataEventListener("onTextData") { eventName, eventParams ->
                if (eventParams != null)
                    WOWZLog.debug("onWZDataEvent -> " + eventName + " = " + eventParams.get("text"))

                null
            }
        } else {
            mHelp!!.visibility = View.GONE
            mStatusView!!.setErrorMessage(WowzaGoCoder.getLastError().errorDescription)
        }

    }

    override fun onDestroy() {
        if (mVolumeSettingChangeObserver != null)
            applicationContext.contentResolver.unregisterContentObserver(mVolumeSettingChangeObserver!!)

        super.onDestroy()
    }

    /**
     * Android Activity class methods
     */
    override fun onResume() {
        super.onResume()


        hideBuffering()
        if (!GlobalPlayerStateManager.isReady()) {
            showTearingdownDialog()
            mStreamPlayerView!!.stop()
        } else {
            syncUIControlState()
        }
    }

    override fun onPause() {


        if (mStreamPlayerView != null) {

            AsyncTask.execute {
                WOWZLog.debug("DECODER STATUS Stopping ... ")
                mStreamPlayerView!!.stop()

                // Wait for the streaming player to disconnect and shutdown...
                mStreamPlayerView!!.currentStatus.waitForState(WOWZState.IDLE)
                WOWZLog.debug("DECODER STATUS Stopped!  ")
            }
        }

        super.onPause()
    }

    /*
    Click handler for network pausing
     */
    fun onPauseNetwork(v: View) {
        val btn = findViewById<Button>(R.id.pause_network)
        if (btn.text.toString().trim { it <= ' ' }.equals("pause network", ignoreCase = true)) {
            WOWZLog.info("Pausing network...")
            btn.setText(R.string.wz_unpause_network)
            mStreamPlayerView!!.pauseNetworkStack()
        } else {
            WOWZLog.info("Unpausing network... btn.getText(): " + btn.text)
            btn.setText(R.string.wz_pause_network)
            mStreamPlayerView!!.unpauseNetworkStack()
        }
    }

    fun playStream() {
        if (!this.isNetworkAvailable) {
            displayErrorDialog("No internet connection, please try again later.")
            return
        }

        mHelp!!.visibility = View.GONE
        val configValidationError = mStreamPlayerConfig!!.validateForPlayback()
        if (configValidationError != null) {
            mStatusView!!.setErrorMessage(configValidationError.errorDescription)
        } else {
            // Set the detail level for network logging output
            mStreamPlayerView!!.logLevel = mWZNetworkLogLevel

            // Set the player's pre-buffer duration as stored in the app prefs
            val preBufferDuration = GoCoderSDKPrefs.getPreBufferDuration(PreferenceManager.getDefaultSharedPreferences(this))

            mStreamPlayerConfig!!.preRollBufferDuration = preBufferDuration

            // Start playback of the live stream
            mStreamPlayerView!!.play(mStreamPlayerConfig, this)
        }
    }

    /**
     * Click handler for the playback button
     */
    fun onTogglePlayStream(v: View) {
        if (mStreamPlayerView!!.isPlaying) {
            showTearingdownDialog()
            mStreamPlayerView!!.stop()
            mStreamPlayerView!!.currentStatus.waitForState(WOWZState.IDLE)
        } else if (mStreamPlayerView!!.isReadyToPlay) {
            WOWZLog.debug("onTogglePlayStream() :: DECODER STATUS :: start stream")
            this.playStream()
        }
    }

    /**
     * WOWZStatusCallback interface methods
     */
    @Synchronized
    override fun onWZStatus(status: WOWZStatus) {
        val playerStatus = WOWZStatus(status)

        Handler(Looper.getMainLooper()).post {
            WOWZLog.debug("DECODER STATUS: 000 [player activity] current: $playerStatus")
            when (playerStatus.state) {
                WOWZPlayerView.STATE_PLAYING -> {
                    // Keep the screen on while we are playing back the stream
                    window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

                    if (mStreamPlayerConfig!!.preRollBufferDuration == 0f) {
                        mTimerView!!.startTimer()
                    }

                    mStreamPlayerConfig!!.streamName = TintApplication.instance.streamName

                    // Since we have successfully opened up the server connection, store the connection info for auto complete
                    GoCoderSDKPrefs.storeHostConfig(PreferenceManager.getDefaultSharedPreferences(this@PlayerActivity), mStreamPlayerConfig!!)

                    // Log the stream metadata
                    WOWZLog.debug(TAG, "Stream metadata:\n" + mStreamPlayerView!!.metadata)
                }

                WOWZPlayerView.STATE_READY_TO_PLAY -> {
                    // Clear the "keep screen on" flag
                    WOWZLog.debug(TAG, "STATE_READY_TO_PLAY player activity status!")
                    if (playerStatus.lastError != null)
                        displayErrorDialog(playerStatus.lastError)

                    playerStatus.clearLastError()
                    window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

                    mTimerView!!.stopTimer()
                    if (GlobalPlayerStateManager.isReady()) {
                        hideTearingdownDialog()
                    }
                }

                WOWZPlayerView.STATE_PREBUFFERING_STARTED -> {
                    WOWZLog.debug(TAG, "**** Decoder Dialog for buffering should show...")
                    showBuffering()
                }

                WOWZPlayerView.STATE_PREBUFFERING_ENDED -> {
                    WOWZLog.debug(TAG, "**** Decoder Dialog for buffering should stop...")
                    hideBuffering()
                    // Make sure player wasn't signaled to shutdown
                    if (mStreamPlayerView!!.isPlaying) {
                        mTimerView!!.startTimer()
                    }
                }

                WOWZPlayerView.STATE_PLAYBACK_COMPLETE -> WOWZLog.debug("DECODER STATUS: [player activity2] current: $playerStatus")
                else -> {
                    WOWZLog.debug("DECODER STATUS: [player activity] current: $playerStatus")
                    WOWZLog.debug("DECODER STATUS: [player activity] current: " + GlobalPlayerStateManager.isReady())
                }
            }
            syncUIControlState()
        }
    }

    @Synchronized
    override fun onWZError(playerStatus: WOWZStatus) {
        Handler(Looper.getMainLooper()).post {
            displayErrorDialog(playerStatus.lastError)
            playerStatus.state = WOWZState.IDLE
            syncUIControlState()
        }
    }

    /**
     * Click handler for the mic/mute button
     */
    fun onToggleMute(v: View) {
        mBtnMic!!.toggleState()

        if (mStreamPlayerView != null)
            mStreamPlayerView!!.mute(!mBtnMic!!.isOn)

        mSeekVolume!!.isEnabled = mBtnMic!!.isOn
    }

    fun onToggleScaleMode(v: View) {
        val newScaleMode = if (mStreamPlayerView!!.scaleMode == WOWZMediaConfig.RESIZE_TO_ASPECT) WOWZMediaConfig.FILL_VIEW else WOWZMediaConfig.RESIZE_TO_ASPECT
        mBtnScale!!.setState(newScaleMode == WOWZMediaConfig.FILL_VIEW)
        mStreamPlayerView!!.scaleMode = newScaleMode
    }

    /**
     * Click handler for the metadata button
     */
    fun onStreamMetadata(v: View) {
        val streamMetadata = mStreamPlayerView!!.metadata
        val streamStats = mStreamPlayerView!!.streamStats
        val streamInfo = WOWZDataMap()

        streamInfo.put("- Stream Statistics -", streamStats)
        streamInfo.put("- Stream Metadata -", streamMetadata)
        //streamInfo.put("- Stream Configuration -", streamConfig);

        val dataTableFragment = DataTableFragment.newInstance("Stream Information", streamInfo, false, false)

        // Display/hide the data table fragment
        fragmentManager.beginTransaction()
                .add(android.R.id.content, dataTableFragment)
                .addToBackStack("metadata_fragment")
                .commit()
    }

    /**
     * Click handler for the settings button
     */
    fun onSettings(v: View) {
        // Display the prefs fragment
        val prefsFragment = GoCoderSDKPrefs.PrefsFragment()
        prefsFragment.setFixedSource(true)
        prefsFragment.setForPlayback(true)

        fragmentManager.beginTransaction()
                .replace(android.R.id.content, prefsFragment)
                .addToBackStack(null)
                .commit()
    }

    /**
     * Update the state of the UI controls
     */
    private fun syncUIControlState() {
        val disableControls = !(GlobalPlayerStateManager.isReady() || mStreamPlayerView!!.isPlaying) // (!(GlobalPlayerStateManager.isReady() ||  mStreamPlayerView.isReadyToPlay() || mStreamPlayerView.isPlaying()) || sGoCoderSDK == null);
        if (disableControls) {
            mBtnPlayStream!!.isEnabled = false
            mBtnSettings!!.isEnabled = false
            mSeekVolume!!.isEnabled = false
            mBtnScale!!.isEnabled = false
            mBtnMic!!.isEnabled = false
            mStreamMetadata!!.isEnabled = false
        } else {
            mBtnPlayStream!!.setState(mStreamPlayerView!!.isPlaying)
            mBtnPlayStream!!.isEnabled = true
            if (mStreamPlayerConfig!!.isAudioEnabled) {
                mBtnMic!!.visibility = View.VISIBLE
                mBtnMic!!.isEnabled = true

                mSeekVolume!!.visibility = View.VISIBLE
                mSeekVolume!!.isEnabled = mBtnMic!!.isOn
                mSeekVolume!!.progress = mStreamPlayerView!!.volume
            } else {
                mSeekVolume!!.visibility = View.GONE
                mBtnMic!!.visibility = View.GONE
            }

            mBtnScale!!.visibility = View.VISIBLE
            mBtnScale!!.visibility = if (mStreamPlayerView!!.isPlaying && mStreamPlayerConfig!!.isVideoEnabled) View.VISIBLE else View.GONE
            mBtnScale!!.isEnabled = mStreamPlayerView!!.isPlaying && mStreamPlayerConfig!!.isVideoEnabled

            mBtnSettings!!.isEnabled = !mStreamPlayerView!!.isPlaying
            mBtnSettings!!.visibility = if (mStreamPlayerView!!.isPlaying) View.GONE else View.VISIBLE

            mStreamMetadata!!.isEnabled = mStreamPlayerView!!.isPlaying
            mStreamMetadata!!.visibility = if (mStreamPlayerView!!.isPlaying) View.VISIBLE else View.GONE
        }
    }

    private fun showTearingdownDialog() {

        try {
            if (mGoingDownDialog == null) return
            hideBuffering()
            if (!mGoingDownDialog!!.isShowing) {
                mGoingDownDialog!!.setCancelable(false)
                mGoingDownDialog!!.show()
            }
        } catch (ex: Exception) {
            WOWZLog.warn(TAG, "showTearingdownDialog:$ex")
        }

    }

    private fun hideTearingdownDialog() {

        try {
            if (mGoingDownDialog == null) return
            hideBuffering()
            mGoingDownDialog!!.dismiss()
        } catch (ex: Exception) {
            WOWZLog.warn(TAG, "hideTearingdownDialog exception:$ex")
        }

    }

    private fun showBuffering() {
        try {
            if (mBufferingDialog == null) return

            val mainThreadHandler = Handler(baseContext.mainLooper)
            mBufferingDialog!!.setCancelable(false)
            mBufferingDialog!!.show()
            mBufferingDialog!!.getButton(DialogInterface.BUTTON_NEGATIVE).isEnabled = false
            object : Thread() {
                override fun run() {
                    try {
                        var cnt = 0
                        while (GlobalPlayerStateManager.DECODER_VIDEO_STATE != WOWZState.STARTING && GlobalPlayerStateManager.DECODER_VIDEO_STATE != WOWZState.RUNNING) {
                            if (cnt > 3) {
                                break
                            }
                            Thread.sleep(1000)
                            cnt++
                        }
                    } catch (ex: Exception) {
                        WOWZLog.warn(TAG, "showBuffering:$ex")
                    }

                    mainThreadHandler.post { mBufferingDialog!!.getButton(DialogInterface.BUTTON_NEGATIVE).isEnabled = true }
                }
            }.start()
        } catch (ex: Exception) {
            WOWZLog.warn(TAG, "showBuffering:$ex")
        }

    }

    private fun cancelBuffering() {

        showTearingdownDialog()
        if (mStreamPlayerConfig!!.hlsBackupURL != null || mStreamPlayerConfig!!.isHLSEnabled ||
                mStreamPlayerView != null && mStreamPlayerView!!.isPlaying) {
            mStreamPlayerView!!.stop()
        } else {
            hideTearingdownDialog()
        }
    }

    private fun hideBuffering() {
        if (mBufferingDialog!!.isShowing)
            mBufferingDialog!!.dismiss()
    }

    override fun syncPreferences() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        mWZNetworkLogLevel = Integer.valueOf(prefs.getString("wz_debug_net_log_level", WOWZLog.LOG_LEVEL_DEBUG.toString())!!)

        mStreamPlayerConfig!!.isPlayback = true
        if (mStreamPlayerConfig != null)
            GoCoderSDKPrefs.updateConfigFromPrefsForPlayer(prefs, mStreamPlayerConfig!!)
    }

    companion object {
        private val TAG = PlayerActivity::class.java.simpleName
    }

}
