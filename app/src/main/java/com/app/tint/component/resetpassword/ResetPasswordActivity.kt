package com.app.tint.component.resetpassword

import android.os.Bundle
import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.app.BaseActivity
import com.app.tint.utility.showToast
import kotlinx.android.synthetic.main.activity_reset_password.*

class ResetPasswordActivity: BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)

        btnDone.setOnClickListener {
            when {
                etPass.text.isNullOrBlank() -> showToast(this, getString(R.string.missing_password))
                etPassRetype.text.isNullOrBlank() -> showToast(this, getString(R.string.missing_password))
                etPass.text.toString() != etPassRetype.text.toString() -> showToast(this, getString(R.string.err_match_password))
                else -> {
                    //todo text12call API OTP

                    val requestPassReset = RequestPassReset()
                    requestPassReset.pass = etPass.text.toString()
                    requestPassReset.phone = TintApplication.instance.phoneNo
                    callSetPassword(requestPassReset)

                }
            }
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        mGoogleApiClient!!.disconnect()
        activityContext = null
        mGoogleApiClient = null
    }

}