package com.app.tint.component.profile.userprofile.fav;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class FavVideosResponse{

	@SerializedName("favCount")
	private int favCount;

	@SerializedName("end")
	private int end;

	@SerializedName("userFavorite")
	private List<UserFavoriteItem> userFavorite;

	@SerializedName("status")
	private String status;

	public void setFavCount(int favCount){
		this.favCount = favCount;
	}

	public int getFavCount(){
		return favCount;
	}

	public void setEnd(int end){
		this.end = end;
	}

	public int getEnd(){
		return end;
	}

	public void setUserFavorite(List<UserFavoriteItem> userFavorite){
		this.userFavorite = userFavorite;
	}

	public List<UserFavoriteItem> getUserFavorite(){
		return userFavorite;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"FavVideosResponse{" + 
			"favCount = '" + favCount + '\'' + 
			",end = '" + end + '\'' + 
			",userFavorite = '" + userFavorite + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}