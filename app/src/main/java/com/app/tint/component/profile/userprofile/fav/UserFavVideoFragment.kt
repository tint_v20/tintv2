package com.app.tint.component.profile.userprofile.fav

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.services.TintAPICall
import com.app.tint.services.TintServiceResponseCallback
import com.app.tint.utility.Constants
import com.app.tint.utility.ServiceType
import kotlinx.android.synthetic.main.fragment_saved_video.*
import org.jetbrains.anko.toast

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [UserTagFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [UserTagFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class UserTagFragment : Fragment(), TintServiceResponseCallback {


    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var favoriteItemList: MutableList<UserFavoriteItem>
    private lateinit var rvFav: RecyclerView

    private lateinit var favVideosAdapter: FavVideosAdapter
    private var loading = false
    private var pastVisibleItems: Int = 0
    private var visibleItemCount:Int = 0
    private var totalItemCount:Int = 0
    private lateinit var gridLayoutManager: GridLayoutManager


    companion object {

        fun newInstance(): UserTagFragment {
            return UserTagFragment()
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        favoriteItemList = ArrayList()
        gridLayoutManager = GridLayoutManager(context, 3)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_saved_video, container, false)
    }

    // TODO: Rename method, update argument and hook method into UI event

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvFav = view.findViewById(R.id.rvFav)
        rvFav.setHasFixedSize(true)
        rvFav.layoutManager = gridLayoutManager
        // rvVideos.addItemDecoration(GridSpacingItemDecoration(dpToPx(10)))
        rvFav.itemAnimator = DefaultItemAnimator()

        loader.visibility = View.VISIBLE

        val favVideoRequest = FavVideoRequest()
        favVideoRequest.start = 0
        favVideoRequest.userId = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID)!!.toInt()

        TintAPICall(context, this).makeTintApiCall(favVideoRequest, ServiceType.GET_FAV_VIDEOS)

    }


    override fun onSuccessTintService(success: Any?, serviceType: Int) {

        if(isAdded && success != null) {

            when(serviceType) {

                ServiceType.GET_FAV_VIDEOS -> {

                    loader.visibility = View.GONE

                    if(success is FavVideosResponse) {
                        favoriteItemList = success.userFavorite

                        favVideosAdapter = FavVideosAdapter(context!!, favoriteItemList)
                        rvFav.adapter = favVideosAdapter
                    }

                }
            }
        }

    }

    override fun onErrorTintService(error: Any?, serviceType: Int) {

        if(error!= null && isAdded) {
            loader.visibility = View.GONE
            //activity!!.toast(error.toString())
        }

    }
}
