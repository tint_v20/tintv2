package com.app.tint.component.home;

import com.google.gson.annotations.SerializedName;

public class LikeResponse {

	@SerializedName("userId ")
	private int userId;

	@SerializedName("videoId")
	private int videoId;

	@SerializedName("status")
	private String status;

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setVideoId(int videoId){
		this.videoId = videoId;
	}

	public int getVideoId(){
		return videoId;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"LikeResponse{" + 
			"userId  = '" + userId + '\'' + 
			",videoId = '" + videoId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}