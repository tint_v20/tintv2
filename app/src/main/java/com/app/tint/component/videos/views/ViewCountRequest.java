package com.app.tint.component.videos.views;

import com.google.gson.annotations.SerializedName;

public class ViewCountRequest{

	@SerializedName("videoId")
	private int videoId;

	public void setVideoId(int videoId){
		this.videoId = videoId;
	}

	public int getVideoId(){
		return videoId;
	}

	@Override
 	public String toString(){
		return 
			"ViewCountRequest{" + 
			"videoId = '" + videoId + '\'' + 
			"}";
		}
}