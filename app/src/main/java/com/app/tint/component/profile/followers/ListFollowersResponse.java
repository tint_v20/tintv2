package com.app.tint.component.profile.followers;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ListFollowersResponse{

	@SerializedName("userFollowing")
	private List<UserFollowingItem> userFollowing;

	@SerializedName("followersCount")
	private int followersCount;

	@SerializedName("userFollowers")
	private List<UserFollowersItem> userFollowers;

	@SerializedName("followingCount")
	private int followingCount;

	public void setUserFollowing(List<UserFollowingItem> userFollowing){
		this.userFollowing = userFollowing;
	}

	public List<UserFollowingItem> getUserFollowing(){
		return userFollowing;
	}

	public void setFollowersCount(int followersCount){
		this.followersCount = followersCount;
	}

	public int getFollowersCount(){
		return followersCount;
	}

	public void setUserFollowers(List<UserFollowersItem> userFollowers){
		this.userFollowers = userFollowers;
	}

	public List<UserFollowersItem> getUserFollowers(){
		return userFollowers;
	}

	public void setFollowingCount(int followingCount){
		this.followingCount = followingCount;
	}

	public int getFollowingCount(){
		return followingCount;
	}

	@Override
 	public String toString(){
		return 
			"ListFollowersResponse{" + 
			"userFollowing = '" + userFollowing + '\'' + 
			",followersCount = '" + followersCount + '\'' + 
			",userFollowers = '" + userFollowers + '\'' + 
			",followingCount = '" + followingCount + '\'' + 
			"}";
		}
}