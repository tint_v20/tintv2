package com.app.tint.component.signup

import android.os.Bundle
import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.app.BaseActivity
import com.app.tint.component.common.CommonAuthRequest
import com.app.tint.component.login.LoginActivity
import com.app.tint.services.SocialServiceManager
import com.app.tint.utility.showToast
import kotlinx.android.synthetic.main.activity_register.*
import com.app.tint.utility.Constants
import com.app.tint.utility.launchActivity


class SignupActivity : BaseActivity(), BaseActivity.OnOTPVerification {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        listeners()

    }

    private fun listeners() {
        btnSignUp.setOnClickListener {

            when {
                etUserID.text.isNullOrBlank() -> showToast(this, getString(R.string.missing_user_id))
                etPhone.text.isNullOrBlank() -> showToast(this, getString(R.string.missing_phone_no))
                etPass.text.isNullOrBlank() -> showToast(this, getString(R.string.missing_password))
                else -> {
                    //todo call API OTP

                    processOtpVerification(etPhone.text.toString())

                }
            }

        }

        ivFb.setOnClickListener {

            SocialServiceManager(this, this).callFacebookAuth()
        }

        ivTwitter.setOnClickListener {
            SocialServiceManager(this, this).callTwitterAuth()
        }

        ivGoogle.setOnClickListener {

            SocialServiceManager(this, this).callGoogleAuth(mGoogleApiClient!!)
        }

        ivInstagramSignUp.setOnClickListener {

            TintApplication.instance.instagramHelper.loginFromActivity(this)

        }

        tvNoAccount.setOnClickListener {
            launchActivity(this, LoginActivity::class.java, isFinish = true, isClearBack = false, isAnimLeft =true)
        }
    }

    override fun oppVerified() {

        val userDataInput = CommonAuthRequest()
        userDataInput.username = etUserID.text.toString()
        userDataInput.phone = etPhone.text.toString()
        userDataInput.userType = "tint_user"
        userDataInput.setPass(etPass.text.toString())
        userDataInput.userType = Constants.TINT_USER
        callManualRegistration(userDataInput)

    }

    override fun onDestroy() {
        super.onDestroy()
        mGoogleApiClient!!.disconnect()
        activityContext = null
        mGoogleApiClient = null
    }

}
