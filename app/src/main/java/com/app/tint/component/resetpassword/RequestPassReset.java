package com.app.tint.component.resetpassword;

import com.google.gson.annotations.SerializedName;
public class RequestPassReset{

	@SerializedName("phone")
	private String phone;

	@SerializedName("pass")
	private String pass;

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setPass(String pass){
		this.pass = pass;
	}

	public String getPass(){
		return pass;
	}

	@Override
 	public String toString(){
		return 
			"RequestPassReset{" + 
			"phone = '" + phone + '\'' + 
			",pass = '" + pass + '\'' + 
			"}";
		}
}