package com.app.tint.component.login;


import com.google.gson.annotations.SerializedName;


public class LoginObjectRequest {

	@SerializedName("pass")
	private String pass;

	@SerializedName("username")
	private String username;

	public void setPass(String pass){
		this.pass = pass;
	}

	public String getPass(){
		return pass;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
 	public String toString(){
		return 
			"LoginObjectRequest{" +
			"pass = '" + pass + '\'' + 
			",ic_username = '" + username + '\'' +
			"}";
		}
}