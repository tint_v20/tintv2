package com.app.tint.component.search;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SearchQueryResponse{

	@SerializedName("userVideos")
	private List<UserVideosItem> userVideos;

	@SerializedName("end")
	private int end;

	public void setUserVideos(List<UserVideosItem> userVideos){
		this.userVideos = userVideos;
	}

	public List<UserVideosItem> getUserVideos(){
		return userVideos;
	}

	public void setEnd(int end){
		this.end = end;
	}

	public int getEnd(){
		return end;
	}

	@Override
 	public String toString(){
		return 
			"SearchQueryResponse{" + 
			"userVideos = '" + userVideos + '\'' + 
			",end = '" + end + '\'' + 
			"}";
		}
}