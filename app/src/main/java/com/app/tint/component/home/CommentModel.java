package com.app.tint.component.home;

public class CommentModel {

    private String userName;
    private int videoId;
    private String userPic;
    private String commentText;

    public CommentModel(String userName, int videoId, String userPic, String commentText) {
        this.userName = userName;
        this.videoId = videoId;
        this.userPic = userPic;
        this.commentText = commentText;
    }

    public String getUserName() {
        return userName;
    }

    public int getVideoId() {
        return videoId;
    }

    public String getUserPic() {
        return userPic;
    }

    public String getCommentText() {
        return commentText;
    }
}
