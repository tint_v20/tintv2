package com.app.tint.component.common

import android.app.Activity
import android.app.Dialog
import android.app.PendingIntent.getActivity
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.tint.R
import com.app.tint.utility.Constants
import kotlinx.android.synthetic.main.fragment_alert_dialogue.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [AlertDialogue.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [AlertDialogue.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class AlertDialogue( private val _context: Context,  private val msg: String, private val btnMsg:String) : AlertDialog(_context), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCancelable(false)
        setContentView(R.layout.fragment_alert_dialogue)

        btnCancel.setOnClickListener(this)
        btnConfirm.setOnClickListener(this)

        title.text = msg
        btnConfirm.text = btnMsg
    }



    override fun onClick(v: View?) {

        when(v?.id) {

            R.id.btnCancel -> {

                if (!(_context as Activity).isFinishing) {
                        dismiss()
                    (_context as OnDialogItemClick).onButtonClick(Constants.DLG_CANCEL)
                }

            }

            R.id.btnConfirm -> {
                if (!(_context as Activity).isFinishing) {
                    dismiss()
                    (_context as OnDialogItemClick).onButtonClick(Constants.DLG_CONFIRM)
                }

            }

        }

    }


    interface OnDialogItemClick {

        fun onButtonClick(code: Int)
    }


}




