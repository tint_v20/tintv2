package com.app.tint.component.profile.userprofile;

import com.google.gson.annotations.SerializedName;

public class ProfileUpdateRequest{

	@SerializedName("website")
	private String website;

	@SerializedName("gender")
	private String gender;

	@SerializedName("phone")
	private String phone;

	@SerializedName("name")
	private String name;

	@SerializedName("dateOfBirth")
	private int dateOfBirth;

	@SerializedName("userId")
	private int userId;

	@SerializedName("email")
	private String email;

	public void setWebsite(String website){
		this.website = website;
	}

	public String getWebsite(){
		return website;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setDateOfBirth(int dateOfBirth){
		this.dateOfBirth = dateOfBirth;
	}

	public int getDateOfBirth(){
		return dateOfBirth;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	@Override
 	public String toString(){
		return 
			"ProfileUpdateRequest{" + 
			"website = '" + website + '\'' + 
			",gender = '" + gender + '\'' + 
			",phone = '" + phone + '\'' + 
			",name = '" + name + '\'' + 
			",dateOfBirth = '" + dateOfBirth + '\'' + 
			",userId = '" + userId + '\'' + 
			",email = '" + email + '\'' + 
			"}";
		}
}