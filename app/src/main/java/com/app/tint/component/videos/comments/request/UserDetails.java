package com.app.tint.component.videos.comments.request;

import com.google.gson.annotations.SerializedName;

public class UserDetails{

	@SerializedName("userId")
	private String userId;

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	@Override
 	public String toString(){
		return 
			"UserDetails{" + 
			"userId = '" + userId + '\'' + 
			"}";
		}
}