package com.app.tint.component.profile.userprofile.history;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class VideoHistoryResponse{

	@SerializedName("userVideos")
	private List<UserVideosItem> userVideos;

	@SerializedName("imgUrl")
	private String imgUrl;

	@SerializedName("end")
	private int end;

	public void setUserVideos(List<UserVideosItem> userVideos){
		this.userVideos = userVideos;
	}

	public List<UserVideosItem> getUserVideos(){
		return userVideos;
	}

	public void setImgUrl(String imgUrl){
		this.imgUrl = imgUrl;
	}

	public String getImgUrl(){
		return imgUrl;
	}

	public void setEnd(int end){
		this.end = end;
	}

	public int getEnd(){
		return end;
	}

	@Override
 	public String toString(){
		return 
			"VideoHistoryResponse{" + 
			"userVideos = '" + userVideos + '\'' + 
			",imgUrl = '" + imgUrl + '\'' + 
			",end = '" + end + '\'' + 
			"}";
		}
}