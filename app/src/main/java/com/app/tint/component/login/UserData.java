package com.app.tint.component.login;


import com.google.gson.annotations.SerializedName;
public class UserData{

	@SerializedName("imgUrl")
	private String imgUrl;

	@SerializedName("userType")
	private String userType;

	@SerializedName("userName")
	private String userName;

	@SerializedName("userId")
	private int userId;

	public void setImgUrl(String imgUrl){
		this.imgUrl = imgUrl;
	}

	public String getImgUrl(){
		return imgUrl;
	}

	public void setUserType(String userType){
		this.userType = userType;
	}

	public String getUserType(){
		return userType;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	@Override
 	public String toString(){
		return 
			"UserData{" + 
			"imgUrl = '" + imgUrl + '\'' + 
			",userType = '" + userType + '\'' + 
			",userName = '" + userName + '\'' + 
			",userId = '" + userId + '\'' + 
			"}";
		}
}