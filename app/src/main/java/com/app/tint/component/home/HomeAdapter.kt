package com.app.tint.component.home

import android.annotation.SuppressLint
import android.app.Activity
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.component.MainActivity
import com.app.tint.component.profile.ProfileFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import java.lang.ref.WeakReference
import java.util.*
import kotlin.collections.ArrayList
import android.widget.*
import com.app.tint.component.login.LoginActivity
import com.app.tint.jiaozivideoplayer.Jzvd
import com.app.tint.jiaozivideoplayer.JzvdStd
import com.app.tint.utility.Constants
import com.app.tint.utility.TintSingleton
import com.app.tint.utility.launchActivity




class HomeAdapter(activity: Activity) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var activityWeakReference: WeakReference<Activity>? = null
    private var videosItemList: MutableList<UserVideosItem>? = ArrayList()
    private val likeUnlikeState = HashMap<Int, String>()
    private val favUnFavState = HashMap<Int, String>()
    private val followUnfollowState = HashMap<Int, Int>()
    private var lastPos: Int = -1

    private var loadMoreListener: OnLoadMoreListener? = null
    private var isLoading = false
    private var isMoreDataAvailable = true



    private val activity: Activity
        get() = activityWeakReference!!.get()!!

    init {
        this.activityWeakReference = WeakReference(activity)

    }

    fun addData(_videosItemList: MutableList<UserVideosItem>) {
        this.videosItemList = _videosItemList
    }

    fun updateData(viewModels: List<UserVideosItem>) {

        videosItemList!!.addAll(viewModels)
        isLoading = false
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

            val layoutView = LayoutInflater.from(parent.context).inflate(R.layout.list_item_video, parent, false)
            return VideoViewHolder(layoutView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {


        val itemsItem = videosItemList!![position]

        if (position >= itemCount - 1 && isMoreDataAvailable && !isLoading && loadMoreListener != null) {
            isLoading = true
            loadMoreListener!!.onLoadMore()
        } else
        {

            (holder as VideoViewHolder).bindData(itemsItem, position)
        }

    }


    fun playVideoPos(currentPos: Int) {
        lastPos = currentPos
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return videosItemList!!.size
    }


    fun setMoreDataAvailable(moreDataAvailable: Boolean) {
        isMoreDataAvailable = moreDataAvailable
    }

    interface OnLoadMoreListener {
        fun onLoadMore()
    }
    internal class LoadHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    @Suppress("DEPRECATION")
    inner class VideoViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var videoPlayer: JzvdStd = view.findViewById(R.id.videoplayer)
        var ivProfilePosted: ImageView = view.findViewById(R.id.ivProfilePosted)
        var ivHeart: ImageView = view.findViewById(R.id.ivHeart)
        var ivShare: ImageView = view.findViewById(R.id.ivShare)
        var ivSave: ImageView = view.findViewById(R.id.ivSave)
        var tvLive: TextView = view.findViewById(R.id.tvLive)
        var tvViewCount: TextView = view.findViewById(R.id.tvViewCount)
        var vBg: View = view.findViewById(R.id.vBg)
        var ivPlus : ImageView = view.findViewById(R.id.ivPlus)
        var fLayout: FrameLayout = view.findViewById(R.id.fLayout)
        var tvTitle: TextView = view.findViewById(R.id.tvTitle)
        var tvDescription : TextView = view.findViewById(R.id.tvDescription)


        @SuppressLint("SetTextI18n")
        internal fun bindData(itemList: UserVideosItem, pos: Int) {


            videoPlayer.setUp(
                    itemList.onlineVideoUrl,
                    "", Jzvd.SCREEN_NORMAL)

//            val interval = 5000 * 1000L
//            val options = RequestOptions().frame(interval)
            Glide.with(videoPlayer.context).asBitmap()
                    .load(itemList.videoThumbnail)
//                    .apply(options)
                    .into(videoPlayer.thumbImageView)

            tvTitle.text = ""+itemList.videoName
            tvDescription.text = ""+itemList.description

              //  Glide.with(videoPlayer.context).load(R.mipmap.ic_launcher).into(videoPlayer.thumbImageView)





            if (lastPos != -1 && lastPos == pos) {
                videoPlayer.playFinalVideo()
            } else if(lastPos == -1) {
                videoPlayer.playFinalVideo()
            }



            if (itemList.isLiveStatus) {
                tvLive.visibility = View.VISIBLE
                tvViewCount.visibility = View.VISIBLE
                tvViewCount.text = (itemList.likesCount).toString()

            } else {
                tvLive.visibility = View.GONE
                tvViewCount.visibility = View.GONE

            }


            if (itemList.imgUrl != null) {
                Glide.with(activity)
                        .load(itemList.imgUrl.toString())
                        .apply(RequestOptions().override(140, 140))
                        .into(ivProfilePosted)
            }

            if(TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID) != null) {

                if(itemList.isIsFollowed || (TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID)!!.toInt() == itemList.userId)) {
                    vBg.visibility = View.GONE
                    ivPlus.visibility = View.GONE
                    followUnfollowState[position] = position
                }
                else {
                    vBg.visibility = View.VISIBLE
                    ivPlus.visibility = View.VISIBLE
                }
            }


            if (itemList.isIsLiked) {
                ivHeart.setBackgroundResource(R.mipmap.ic_heart_active)
                likeUnlikeState[position] = videosItemList!![position].onlineVideoUrl
            } else {
                ivHeart.setBackgroundResource(R.mipmap.ic_heart_inactive)
            }

            if (itemList.isIsFav) {
                ivSave.setBackgroundResource(R.mipmap.ic_save_on)
                favUnFavState[position] = videosItemList!![position].onlineVideoUrl
            } else {
                ivSave.setBackgroundResource(R.mipmap.ic_save_off)
            }


            videoPlayer.layoutParams = LinearLayout.LayoutParams(TintSingleton.getInstance().deviceWidth,
                    TintSingleton.getInstance().deviceHeight)


            fLayout.setOnClickListener {

                if(TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID) != null) {

                    TintApplication.instance.selectedUserId = videosItemList!![position].userId
                    TintApplication.instance.selectedUserImage = videosItemList!![position].imgUrl

                    if (itemList.isIsFollowed) {

                        (activity as MainActivity).loadFragment(ProfileFragment.newInstance(), "")
                    } else {
                        //todo service Follow

                        if (followUnfollowState[position] == null) {
                            followUnfollowState[position] = position
                            vBg.visibility = View.GONE
                            ivPlus.visibility = View.GONE

                            (activity as MainActivity).doFollowUser(videosItemList!![position].userId)
                        } else {

                            (activity as MainActivity).loadFragment(ProfileFragment.newInstance(), "")
                        }


                    }
                } else {
                    //todo All permission granted todo Camera Live
                    launchActivity(activity, LoginActivity::class.java, isFinish = true, isClearBack = true, isAnimLeft = false)

                }
            }

            ivHeart.setOnClickListener {

                if (likeUnlikeState[position] == null) {
                    likeUnlikeState[position] = videosItemList!![position].onlineVideoUrl
                    ivHeart.setBackgroundResource(R.mipmap.ic_heart_active)
                } else {
                    ivHeart.setBackgroundResource(R.mipmap.ic_heart_inactive)
                    likeUnlikeState.remove(position)
                }

                (activity as MainActivity).doLikeVideoService(itemList.videoId)
            }
            ivShare.setOnClickListener { (activity as MainActivity).doShareVideo(itemList.onlineVideoUrl) }
            ivSave.setOnClickListener {


                    if (favUnFavState[position] == null) {
                        favUnFavState[position] = videosItemList!![position].onlineVideoUrl
                        ivSave.setBackgroundResource(R.mipmap.ic_save_on)
                    } else {
                        ivSave.setBackgroundResource(R.mipmap.ic_save_off)
                        favUnFavState.remove(position)
                    }

                    (activity as MainActivity).doFavAddRemove(itemList.videoId)

            }

           // if(!isPersonShowCase)
            //showCasePerson(ivProfilePosted)


        }

    }


//    private fun showCasePerson(view: View) {
//
//        isPersonShowCase = true
//        val builder = ShowcaseManager.Builder()
//        builder.context(activity)
//                .key("KEY")
//                .developerMode(true)
//                .view(view)
//                .descriptionImageRes(R.mipmap.ic_launcher)
//                .descriptionTitle("LOREM IPSUM")
//                .descriptionText("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.")
//                .buttonText("Next")
//                .add()
//                .build()
//                .show()
//
//    }
}
