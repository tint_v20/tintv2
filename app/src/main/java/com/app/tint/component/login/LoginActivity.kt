package com.app.tint.component.login

import android.os.Bundle
import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.app.BaseActivity
import com.app.tint.component.MainActivity
import com.app.tint.component.forgotauth.ForgotPasswordActivity
import com.app.tint.component.signup.SignupActivity
import com.app.tint.services.SocialServiceManager
import com.app.tint.services.TintAPICall
import com.app.tint.utility.ServiceType
import com.app.tint.utility.launchActivity
import com.app.tint.utility.showToast
import kotlinx.android.synthetic.main.activity_log_in.*
import org.jetbrains.anko.toast

class LoginActivity: BaseActivity() {

    private var backPressed: Long = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)

        listeners()

    }
    private fun listeners() {
        btnLogin.setOnClickListener {

              val userId=ettUserID.text
              val pass=ettPass.text

                if(userId.isNullOrBlank()) {
                    showToast(this, getString(R.string.missing_user_id))
                }
                else if(pass.isNullOrBlank()) {
                    showToast(this, getString(R.string.missing_password))
                }
                else {
                    //todo call API

                    val loginObjectRequest = LoginObjectRequest()
                    loginObjectRequest.pass = pass.toString()
                    loginObjectRequest.username = userId.toString()
                    callManualLogin(loginObjectRequest)

                }
        }

        tvSkip.setOnClickListener {

            launchActivity(this, MainActivity::class.java, isFinish = true, isClearBack = true, isAnimLeft =true)

        }

        ivFb.setOnClickListener {
            SocialServiceManager(this, this).callFacebookAuth()
        }

        ivTwitter.setOnClickListener {

            SocialServiceManager(this, this).callTwitterAuth()

        }

        ivGoogle.setOnClickListener {

            SocialServiceManager(this, this).callGoogleAuth(mGoogleApiClient!!)

        }

        tvNoAccount.setOnClickListener {

            launchActivity(this, SignupActivity::class.java, isFinish = false, isClearBack = false, isAnimLeft =true)

        }

        tvForgotPassword.setOnClickListener {

            launchActivity(this, ForgotPasswordActivity::class.java, isFinish = false, isClearBack = false, isAnimLeft =true)
        }

        ivInstagramLogin.setOnClickListener {

            TintApplication.instance.instagramHelper.loginFromActivity(this)
        }
    }


    override fun onBackPressed() {
        // super.onBackPressed()
        if (backPressed + 2000 > System.currentTimeMillis()) {
            super.onBackPressed()
            this@LoginActivity.finish()
        }
        else toast("Press once again to exit!")
        backPressed = System.currentTimeMillis()
    }


    override fun onDestroy() {
        super.onDestroy()
        mGoogleApiClient!!.disconnect()
        activityContext = null
        mGoogleApiClient = null
        SocialServiceManager(this, this).removeFbAuth()
    }
}


