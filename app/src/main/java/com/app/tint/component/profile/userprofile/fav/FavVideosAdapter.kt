package com.app.tint.component.profile.userprofile.fav

import android.content.Context
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.app.tint.R
import com.app.tint.component.videos.PlayVideoActivity
import com.app.tint.utility.TintSingleton
import com.app.tint.utility.launchActivity

import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

import java.util.ArrayList
internal class FavVideosAdapter(private val context: Context, itemList: MutableList<UserFavoriteItem>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    val TYPE_DATA = 0
    val TYPE_LOAD = 1

    private var loadMoreListener: OnLoadMoreListener? = null
    private var isLoading = false
    private var isMoreDataAvailable = true
    private var videoList: MutableList<UserFavoriteItem> = ArrayList()

    init {
        this.videoList = itemList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


        if (viewType == TYPE_DATA) {
            val layoutView = LayoutInflater.from(parent.context).inflate(R.layout.item_history, null)
            return ItemViewHolder(layoutView)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_load, parent, false)
            return LoadHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val itemsItem = videoList.get(position)

        if (position >= itemCount - 1 && isMoreDataAvailable && !isLoading && loadMoreListener != null) {
            isLoading = true
            loadMoreListener!!.onLoadMore()
        }

        if (getItemViewType(position) == TYPE_DATA) {
            (holder as ItemViewHolder).bindData(itemsItem)
        }

    }

    override fun getItemCount(): Int {
        return this.videoList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (videoList.get(position).videoId != null) {
            TYPE_DATA
        } else {
            TYPE_LOAD
        }
    }

    fun setMoreDataAvailable(moreDataAvailable: Boolean) {
        isMoreDataAvailable = moreDataAvailable
    }

    fun notifyDataChanged() {
        notifyDataSetChanged()
        isLoading = false
    }

    fun updateData(viewModels: List<UserFavoriteItem>) {
        videoList.clear()
        videoList.addAll(viewModels)
        isLoading = false
        notifyDataSetChanged()
    }

    fun setLoadMoreListener(loadMoreListener: OnLoadMoreListener) {
        this.loadMoreListener = loadMoreListener
    }

    interface OnLoadMoreListener {
        fun onLoadMore()
    }

    internal class LoadHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tvSavedCount: TextView
        var ivThumbnail: ImageView
        var card_view: CardView



        init {

            tvSavedCount = itemView.findViewById<View>(R.id.tvSavedCount) as TextView
            ivThumbnail = itemView.findViewById<View>(R.id.ivThumbnail) as ImageView
            card_view = itemView.findViewById<View>(R.id.card_view) as CardView


        }

        internal fun bindData(itemList: UserFavoriteItem) {

                Glide.with(context)
                        .load(itemList.videoThumbnail)
                        .into(ivThumbnail)

            card_view.setOnClickListener {

                TintSingleton.getInstance().videoURL = itemList.onlineVideoUrl

                launchActivity(context, PlayVideoActivity::class.java, isFinish = false, isClearBack = false, isAnimLeft =true)


            }
        }

    }
}
