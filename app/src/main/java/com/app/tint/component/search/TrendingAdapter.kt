package com.app.tint.component.search

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView

import com.app.tint.R
import com.app.tint.component.videos.PlayVideoActivity
import com.app.tint.utility.TintSingleton
import com.app.tint.utility.launchActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class TrendingAdapter(private val mContext: Context,
                      private val mArrayList: MutableList<UserVideosItem>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TYPE_DATA = 0
    private val TYPE_LOAD = 1

    private var loadMoreListener: TrendingAdapter.OnLoadMoreListener? = null
    private var isLoading = false
    private var isMoreDataAvailable = true


    interface OnLoadMoreListener {
        fun onLoadMore()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


        if (viewType == TYPE_DATA) {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_horizontal, parent, false)
            return HorizontalViewHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_load, parent, false)
            return LoadHolder(view)
        }
    }


    internal class LoadHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        //val current = mArrayList[position]
        //holder.txtTitle.setText(current.getAndroidLiveUrl());

        //holder.itemView.setOnClickListener { Toast.makeText(mContext, current.androidLiveUrl, Toast.LENGTH_SHORT).show() }

        val itemsItem = mArrayList[position]

        if (position >= itemCount - 1 && isMoreDataAvailable && !isLoading && loadMoreListener != null) {
            isLoading = true
            loadMoreListener!!.onLoadMore()
        }

        if (getItemViewType(position) == TYPE_DATA) {
            if(position % 2 == 0) {
                (holder as HorizontalViewHolder).rlPlaceHolder.visibility = View.GONE
                holder.llThumb.visibility = View.VISIBLE
            } else {
                (holder as HorizontalViewHolder).rlPlaceHolder.visibility = View.VISIBLE
                holder.llThumb.visibility = View.GONE
            }

            (holder as HorizontalViewHolder).bindData(itemsItem)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (mArrayList.get(position).videoId != null) {
            TYPE_DATA
        } else {
            TYPE_LOAD
        }
    }

    override fun getItemCount(): Int {
        return mArrayList.size
    }

    inner class HorizontalViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var ivThumb: ImageView = itemView.findViewById(R.id.ivThumb)
        var rlPlaceHolder: RelativeLayout = itemView.findViewById(R.id.rlPlaceHolder)
        var llThumb: LinearLayout = itemView.findViewById(R.id.llThumb)
        var tvTitle: TextView = itemView.findViewById(R.id.tvTitle)
        var tvDesc : TextView = itemView.findViewById(R.id.tvDesc)


        internal fun bindData(itemList: UserVideosItem) {

            Glide.with(mContext)
                    .load(itemList.videoThumbnail)
                    .into(ivThumb)

//            val interval = 5000 * 1000L
//            val options =  RequestOptions().frame(interval);
//            Glide.with(mContext).asBitmap()
//                    .load(itemList.onlineVideoUrl)
//                    .apply(options)
//                    .into(ivThumb)

            tvTitle.text = itemList.videoName.replace("_", " ")
            tvDesc.text = itemList.description.replace("_", " ")


            ivThumb.setOnClickListener {

                TintSingleton.getInstance().videoURL = itemList.onlineVideoUrl

                launchActivity(mContext, PlayVideoActivity::class.java, isFinish = false, isClearBack = false, isAnimLeft =true)

            }

        }
        }
}