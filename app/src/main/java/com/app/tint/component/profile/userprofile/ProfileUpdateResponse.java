package com.app.tint.component.profile.userprofile;

import com.google.gson.annotations.SerializedName;

public class ProfileUpdateResponse{

	@SerializedName("userData")
	private UserData userData;

	@SerializedName("status")
	private String status;

	public void setUserData(UserData userData){
		this.userData = userData;
	}

	public UserData getUserData(){
		return userData;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ProfileUpdateResponse{" + 
			"userData = '" + userData + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}