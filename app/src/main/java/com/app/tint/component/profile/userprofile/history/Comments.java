package com.app.tint.component.profile.userprofile.history;

import com.google.gson.annotations.SerializedName;

public class Comments{

	@SerializedName("commentsCount")
	private int commentsCount;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public void setCommentsCount(int commentsCount){
		this.commentsCount = commentsCount;
	}

	public int getCommentsCount(){
		return commentsCount;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Comments{" + 
			"commentsCount = '" + commentsCount + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}