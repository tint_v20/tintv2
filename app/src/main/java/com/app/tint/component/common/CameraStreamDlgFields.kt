//package com.app.tint.component.common
//
//import android.app.Activity
//import android.content.Context
//import android.graphics.Color
//import android.graphics.drawable.ColorDrawable
//import android.os.Bundle
//import android.support.v7.app.AlertDialog
//import android.view.View
//import com.app.tint.R
//import kotlinx.android.synthetic.main.dlg_start_stream_field.*
//import org.jetbrains.anko.toast
//import android.view.WindowManager
//
//
//
//
//class CameraStreamDlgFields( private val _context: Context) : AlertDialog(_context), View.OnClickListener {
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//
//        setCancelable(true)
//        //this.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        this.window!!.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)
//        setContentView(R.layout.dlg_start_stream_field)
//
//        ivCancel.setOnClickListener(this)
//        fabDone.setOnClickListener(this)
//    }
//
//
//
//    override fun onClick(v: View?) {
//
//        when(v?.id) {
//
//            R.id.ivCancel -> {
//
//                if (!(_context as Activity).isFinishing) {
//                        dismiss()
//                }
//
//            }
//
//            R.id.fabDone -> {
//                if (!(_context as Activity).isFinishing) {
//
//                    val inputVideoName = etVideoName.text.toString()
//                    val inputVideoDesc = etVideoDescription.text.toString()
//                    when {
//                        //inputVideoName.isEmpty() -> _context.toast("Enter Video Name")
//                        inputVideoDesc.isEmpty() -> _context.toast("Enter Description Name")
//                        else -> {
//
//                            dismiss()
//                            (_context as OnStreamDialogItemClick).onStreamButtonConfirm(inputVideoName, inputVideoDesc)
//                        }
//                    }
//
//                }
//
//            }
//        }
//
//    }
//
//
//    interface OnStreamDialogItemClick {
//
//        fun onStreamButtonConfirm(vidName: String, vidDesc: String)
//    }
//
//
//}
//
//
//
//
