package com.app.tint.component.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.tint.R
import com.app.tint.component.videos.comments.byid.UserCommentsItem
import com.app.tint.utility.Constants.decodeEmoji
import com.bumptech.glide.Glide
import com.vanniktech.emoji.EmojiTextView
import com.vanniktech.emoji.EmojiUtils
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

class ChatBroadcastAdapter(private val mContext: Context) : RecyclerView.Adapter<ChatBroadcastAdapter.ChatViewHolder>() {
    private val commentModelArrayList = ArrayList<UserCommentsItem>()
    private val _pos = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ChatViewHolder(layoutInflater.inflate(R.layout.item_adapter_chat, parent, false))
    }

    override fun onBindViewHolder(chatViewHolder: ChatViewHolder, position: Int) {


        val commentModel: UserCommentsItem

        if (_pos != -1)
            commentModel = commentModelArrayList[_pos]
        else
            commentModel = commentModelArrayList[position]

        val emojiInformation = EmojiUtils.emojiInformation(commentModel.comment)
        val res: Int

        if (emojiInformation.isOnlyEmojis && emojiInformation.emojis.size == 1) {
            res = R.dimen.emoji_size_single_emoji
        } else if (emojiInformation.isOnlyEmojis && emojiInformation.emojis.size > 1) {
            res = R.dimen.emoji_size_only_emojis
        } else {
            res = R.dimen.emoji_size_default
        }

        chatViewHolder.textView.setEmojiSizeRes(res, false)
        chatViewHolder.textView.text = decodeEmoji(commentModel.comment)

        chatViewHolder.tvUserName.text = commentModel.userName

        Glide.with(mContext)
                .load(commentModel.userDetails.imgUrl)
                .into(chatViewHolder.ivProfilePosted)
    }

    override fun getItemCount(): Int {
        return commentModelArrayList.size
    }

    fun addChatData(_commentDataList: List<UserCommentsItem>?) {

        if (_commentDataList != null) {
            commentModelArrayList.clear()
            commentModelArrayList.addAll(_commentDataList)
            notifyDataSetChanged()
        }
    }


    //    public void restoreComment(int pos){
    //
    //        _pos = pos;
    //        notifyDataSetChanged();
    //
    //    }

     class ChatViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView: EmojiTextView = view.findViewById(R.id.itemAdapterChatTextView)
        val tvUserName: TextView = view.findViewById(R.id.tvUserName)
        val ivProfilePosted: CircleImageView = view.findViewById(R.id.ivProfilePosted)

    }
}
