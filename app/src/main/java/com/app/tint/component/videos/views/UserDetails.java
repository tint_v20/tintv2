package com.app.tint.component.videos.views;

import com.google.gson.annotations.SerializedName;
public class UserDetails{

	@SerializedName("userId")
	private int userId;

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	@Override
 	public String toString(){
		return 
			"UserDetails{" + 
			"userId = '" + userId + '\'' + 
			"}";
		}
}