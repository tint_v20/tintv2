package com.app.tint.component.videos


import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.util.Log

import com.app.tint.utility.TintSingleton

class PlayVideoActivity : FullViewPlayer() {
    public override fun onCreate(icicle: Bundle?) {
        // TODO Auto-generated method stub
        super.onCreate(icicle)
        mStop!!.setOnClickListener {
            if (mVideoView != null) {
                mVideoView!!.stopPlayback()


                _doesNextActivityStarted = true
                //      Intent homeIntent = new Intent(PlayVideoActivity.this,HomeScreenActivity .class);
                //      startActivity(homeIntent);
                this@PlayVideoActivity.finish()


            }
        }

        mVideoView!!.setOnErrorListener { paramMediaPlayer, paramInt1, paramInt2 ->
            // TODO Auto-generated method stub
            if (isNetworkAvailable(this@PlayVideoActivity)) {
                false
            } else {
                raiseDialog("", "Constants.ERROR_NETWORK_NOT_FOUND")
                true
            }
        }
        mVideoView!!.setOnCompletionListener {
            // TODO Auto-generated method stub
            //if(Constants.LOG)
            Log.d("In OnComplete listerner  ", "")

            current = null
            mVideoView!!.stopPlayback()
            seekBar!!.progress = 0
            seekBar!!.secondaryProgress = 0


            if (!_doesNextActivityStarted) {
                //if(Constants.LOG)
                Log.d(" In setOnCompletionListener ", "Calling Home Screen " + !_doesNextActivityStarted)
                _doesNextActivityStarted = true
                //      Intent RegistrationIntent = new Intent(PlayVideoActivity.this,HomeScreenActivity .class);
                //      startActivity(RegistrationIntent);
                this@PlayVideoActivity.finish()
            }
        }
        //String url = getIntent().getStringExtra(Constants.PLAYVIDEO);
        Log.d("url to play", TintSingleton.getInstance().videoURL)
        startPlaying(TintSingleton.getInstance().videoURL!!)

    }

    private fun raiseDialog(title: String, message: String) {
        // TODO Auto-generated method stub


        val builder = AlertDialog.Builder(this@PlayVideoActivity)
        builder.setMessage(message)
        builder.setTitle(title)
        builder.setPositiveButton("OK") { dialog, which ->
            // TODO Auto-generated method stub
            try {
                current = null
                mVideoView!!.stopPlayback()
                seekBar!!.progress = 0
                seekBar!!.secondaryProgress = 0
            } catch (e: Exception) {
                // TODO: handle exception
            }

            _doesNextActivityStarted = true
            //     Intent RegistrationIntent = new Intent(PlayVideoActivity.this,HomeScreenActivity .class);
            //     startActivity(RegistrationIntent);
            this@PlayVideoActivity.finish()
        }
        builder.setCancelable(true)
        builder.show()

    }

    private fun isNetworkAvailable(context: Context): Boolean {
        //var isNetworkAvailable = false
        val connectivity = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val info = connectivity.allNetworkInfo
        if (info != null) {
            for (i in info.indices) {
                if (info[i].state == NetworkInfo.State.CONNECTED) {
                  //  isNetworkAvailable = true
                    return true
                }
            }
        }
        return false
    }//isNetworkAvailable()

    override fun onSaveInstanceState(outState: Bundle) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        // TODO Auto-generated method stub
        super.onRestoreInstanceState(savedInstanceState)
    }
}
