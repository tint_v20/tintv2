package com.app.tint.component.home;

import java.util.ArrayList;

public class CommentDataItem {

    private int videoId;
    private ArrayList<CommentModel> commentModelArrayList;

    public int getVideoId() {
        return videoId;
    }

    public void setVideoId(int videoId) {
        this.videoId = videoId;
    }

    public ArrayList<CommentModel> getCommentModelArrayList() {
        return commentModelArrayList;
    }

    public void setCommentModelArrayList(ArrayList<CommentModel> commentModelArrayList) {
        this.commentModelArrayList = commentModelArrayList;
    }
}
