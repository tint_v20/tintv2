package com.app.tint.component.profile

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.component.MainActivity
import com.app.tint.component.profile.followers.FollowersAdapter
import com.app.tint.component.profile.followers.GetFollowersRequest
import com.app.tint.component.profile.followers.GetFollowersResponse
import com.app.tint.component.profile.followers.UserFollowersItem
import com.app.tint.component.profile.following.FollowingAdapter
import com.app.tint.component.profile.following.GetFollowingResponse
import com.app.tint.component.profile.following.UserFollowingItem
import com.app.tint.services.TintAPICall
import com.app.tint.services.TintServiceResponseCallback
import com.app.tint.utility.Constants
import com.app.tint.utility.ServiceType
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_ff.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class FollowersFollowingFragment: Fragment() , TintServiceResponseCallback {


    private var mFollowersItemsList : MutableList<UserFollowersItem> = ArrayList()
    private var mFollowingItemsList : MutableList<UserFollowingItem> = ArrayList()
    private lateinit var followersAdapter: FollowersAdapter
    private lateinit var followingAdapter: FollowingAdapter
    private lateinit var layoutManager: LinearLayoutManager
    private var ivUserProfile: ImageView? = null


    companion object {

        fun newInstance(): FollowersFollowingFragment {
            return FollowersFollowingFragment()
        }
    }




    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_ff, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        ivBack.visibility  = View.VISIBLE
        tvTitle.visibility = View.VISIBLE
        ivMore.visibility = View.VISIBLE
        ivMore.setImageResource(R.mipmap.ic_settings)

        ivUserProfile = view.findViewById(R.id.ivUserProfile)


        layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        Glide.with(this)
                .load(TintApplication.instance.selectedUserImage)
                .into(ivUserProfile!!)


    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        if(TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_NAME) != null) {

            tvTitle.text = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_NAME)

        } else {

            tvTitle.text = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_NAME)
        }

        if(TintApplication.instance.isFollowers) {

            val getFollowersRequest = GetFollowersRequest()
            getFollowersRequest.start = TintApplication.instance.perPagePagination
            getFollowersRequest.userId = TintApplication.instance.selectedUserId

            TintAPICall(context, this).makeTintApiCall(getFollowersRequest, ServiceType.GET_FOLLOWERS_LIST)
        } else {

            val getFollowersRequest = GetFollowersRequest()
            getFollowersRequest.start = TintApplication.instance.perPagePagination
            getFollowersRequest.userId = TintApplication.instance.selectedUserId

            TintAPICall(context, this).makeTintApiCall(getFollowersRequest, ServiceType.GET_FOLLOWING_LIST)

        }

        ivBack.setOnClickListener {

            (activity as MainActivity).loadFragment(ProfileFragment.newInstance(), "")
        }

    }


    @SuppressLint("SetTextI18n")
    override fun onSuccessTintService(success: Any?, serviceType: Int) {

        if(success != null && isAdded) {

            when(serviceType) {

                ServiceType.GET_FOLLOWING_LIST -> {

                    if(success is GetFollowingResponse) {

                        tvCount.text = ""+success.followingCount +" Following"

                        mFollowingItemsList = success.userFollowing

                        followingAdapter = FollowingAdapter(context!!, mFollowingItemsList)

                        rvFollow.layoutManager = layoutManager

                        rvFollow.adapter = followingAdapter
                    }

                }

                ServiceType.GET_FOLLOWERS_LIST -> {

                    if(success is GetFollowersResponse) {

                        tvCount.text = ""+success.followersCount+" Followers"

                        mFollowersItemsList = success.userFollowers

                        followersAdapter = FollowersAdapter(context!!, mFollowersItemsList)

                        rvFollow.layoutManager = layoutManager

                        rvFollow.adapter = followersAdapter
                    }
                }

            }
        }

    }

    override fun onErrorTintService(error: Any?, serviceType: Int) {

    }

    override fun onDestroyView() {
        super.onDestroyView()

        ivUserProfile = null
        TintApplication.instance.cancelLoadingDialog()
    }
}