package com.app.tint.component.profile.following

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.app.tint.R
import com.app.tint.component.MainActivity
import com.app.tint.component.profile.FollowersFollowingFragment
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

import java.util.ArrayList
import java.util.HashMap

internal class FollowingAdapter(private val context: Context, itemList: MutableList<UserFollowingItem>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val TYPE_DATA = 0
    private val TYPE_LOAD = 1

    private var loadMoreListener: OnLoadMoreListener? = null
    private var isLoading = false
    private var isMoreDataAvailable = true
    private var videoList: MutableList<UserFollowingItem> = ArrayList()
    private val followUnfollowState = HashMap<Int, String>()

    init {
        this.videoList = itemList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


        if (viewType == TYPE_DATA) {
            val layoutView = LayoutInflater.from(parent.context).inflate(R.layout.item_followers_following, null)
            return ItemViewHolder(layoutView)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_load, parent, false)
            return LoadHolder(view)
        }
    }


    fun filterList(filterNames: ArrayList<UserFollowingItem>) {
        this.videoList = filterNames
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val itemsItem = videoList.get(position)

        if (position >= itemCount - 1 && isMoreDataAvailable && !isLoading && loadMoreListener != null) {
            isLoading = true
            loadMoreListener!!.onLoadMore()
        }

        if (getItemViewType(position) == TYPE_DATA) {
            (holder as ItemViewHolder).bindData(itemsItem)
        }

    }

    override fun getItemCount(): Int {
        return this.videoList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (videoList.get(position).userId != null) {
            TYPE_DATA
        } else {
            TYPE_LOAD
        }
    }

    fun setMoreDataAvailable(moreDataAvailable: Boolean) {
        isMoreDataAvailable = moreDataAvailable
    }

    fun notifyDataChanged() {
        notifyDataSetChanged()
        isLoading = false
    }

    fun updateData(viewModels: List<UserFollowingItem>) {
        videoList.clear()
        videoList.addAll(viewModels)
        isLoading = false
        notifyDataSetChanged()
    }

    fun setLoadMoreListener(loadMoreListener: OnLoadMoreListener) {
        this.loadMoreListener = loadMoreListener
    }

    interface OnLoadMoreListener {
        fun onLoadMore()
    }

    internal class LoadHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tvName: TextView = itemView.findViewById<View>(R.id.tvName) as TextView
        var tvUserName: TextView = itemView.findViewById<View>(R.id.tvUserName) as TextView
        var ivThumbnail: CircleImageView = itemView.findViewById<View>(R.id.ivThumbnail) as CircleImageView
        var btnStatus: TextView = itemView.findViewById<View>(R.id.btnStatus) as TextView


        @Suppress("DEPRECATION")
        internal fun bindData(itemList: UserFollowingItem) {


            Glide.with(context)
                    .load(itemList.imgUrl)
                    .into(ivThumbnail)


            if(itemList.name == null) {


                tvName.text = itemList.userName
            } else {


                tvName.text = itemList.name
                tvUserName.text = itemList.userName

            }


            btnStatus.text = context.getString(R.string.following)

            btnStatus.setOnClickListener {

                //todo make follow/un-follow

                (context as MainActivity).doFollowUser(videoList[position].userId)


                if (followUnfollowState[position] == null) {

                    btnStatus.text = context.getString(R.string.follow)
                    followUnfollowState[position] = ""+videoList[position].userId

                } else {
                    btnStatus.text = context.getString(R.string.following)
                    followUnfollowState.remove(position)

                }

            }


        }

    }
}
