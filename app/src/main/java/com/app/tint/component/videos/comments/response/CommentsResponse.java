package com.app.tint.component.videos.comments.response;

import com.google.gson.annotations.SerializedName;

public class CommentsResponse{

	@SerializedName("userId ")
	private int userId;

	@SerializedName("commentDate")
	private String commentDate;

	@SerializedName("comment")
	private String comment;

	@SerializedName("videoId")
	private int videoId;

	@SerializedName("commentId: ")
	private int commentId;

	@SerializedName("userName")
	private String userName;

	@SerializedName("userDetails")
	private UserDetails userDetails;

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setCommentDate(String commentDate){
		this.commentDate = commentDate;
	}

	public String getCommentDate(){
		return commentDate;
	}

	public void setComment(String comment){
		this.comment = comment;
	}

	public String getComment(){
		return comment;
	}

	public void setVideoId(int videoId){
		this.videoId = videoId;
	}

	public int getVideoId(){
		return videoId;
	}

	public void setCommentId(int commentId){
		this.commentId = commentId;
	}

	public int getCommentId(){
		return commentId;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setUserDetails(UserDetails userDetails){
		this.userDetails = userDetails;
	}

	public UserDetails getUserDetails(){
		return userDetails;
	}

	@Override
 	public String toString(){
		return 
			"CommentsResponse{" + 
			"userId  = '" + userId + '\'' + 
			",commentDate = '" + commentDate + '\'' + 
			",comment = '" + comment + '\'' + 
			",videoId = '" + videoId + '\'' + 
			",commentId:  = '" + commentId + '\'' + 
			",userName = '" + userName + '\'' + 
			",userDetails = '" + userDetails + '\'' + 
			"}";
		}
}