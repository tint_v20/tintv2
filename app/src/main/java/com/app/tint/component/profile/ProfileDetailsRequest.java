package com.app.tint.component.profile;

import com.google.gson.annotations.SerializedName;

public class ProfileDetailsRequest{

	@SerializedName("loginUserId")
	private int loginUserId;

	@SerializedName("profileUserId")
	private int profileUserId;

	public void setLoginUserId(int loginUserId){
		this.loginUserId = loginUserId;
	}

	public int getLoginUserId(){
		return loginUserId;
	}

	public void setProfileUserId(int profileUserId){
		this.profileUserId = profileUserId;
	}

	public int getProfileUserId(){
		return profileUserId;
	}

	@Override
 	public String toString(){
		return 
			"ProfileDetailsRequest{" + 
			"loginUserId = '" + loginUserId + '\'' + 
			",profileUserId = '" + profileUserId + '\'' + 
			"}";
		}
}