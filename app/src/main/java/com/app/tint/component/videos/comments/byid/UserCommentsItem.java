package com.app.tint.component.videos.comments.byid;

import com.google.gson.annotations.SerializedName;

public class UserCommentsItem{

	@SerializedName("commentDate ")
	private String commentDate;

	@SerializedName("userDetails ")
	private UserDetails userDetails;

	@SerializedName("commentId")
	private int commentId;

	@SerializedName("comment")
	private String comment;

	@SerializedName("videoId ")
	private int videoId;

	@SerializedName("userName ")
	private String userName;

	public void setCommentDate(String commentDate){
		this.commentDate = commentDate;
	}

	public String getCommentDate(){
		return commentDate;
	}

	public void setUserDetails(UserDetails userDetails){
		this.userDetails = userDetails;
	}

	public UserDetails getUserDetails(){
		return userDetails;
	}

	public void setCommentId(int commentId){
		this.commentId = commentId;
	}

	public int getCommentId(){
		return commentId;
	}

	public void setComment(String comment){
		this.comment = comment;
	}

	public String getComment(){
		return comment;
	}

	public void setVideoId(int videoId){
		this.videoId = videoId;
	}

	public int getVideoId(){
		return videoId;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	@Override
 	public String toString(){
		return 
			"UserCommentsItem{" + 
			"commentDate  = '" + commentDate + '\'' + 
			",userDetails  = '" + userDetails + '\'' + 
			",commentId = '" + commentId + '\'' + 
			",comment = '" + comment + '\'' + 
			",videoId  = '" + videoId + '\'' + 
			",userName  = '" + userName + '\'' + 
			"}";
		}
}