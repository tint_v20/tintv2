package com.app.tint.component.profile.userprofile.history;

import com.google.gson.annotations.SerializedName;

public class UserVideoHistoryRequest{

	@SerializedName("start")
	private int start;

	@SerializedName("userId")
	private int userId;

	@SerializedName("videoUserId")
	private int videoUserId;

	public void setStart(int start){
		this.start = start;
	}

	public int getStart(){
		return start;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setVideoUserId(int videoUserId){
		this.videoUserId = videoUserId;
	}

	public int getVideoUserId(){
		return videoUserId;
	}

	@Override
 	public String toString(){
		return 
			"UserVideoHistoryRequest{" + 
			"start = '" + start + '\'' + 
			",userId = '" + userId + '\'' + 
			",videoUserId = '" + videoUserId + '\'' + 
			"}";
		}
}