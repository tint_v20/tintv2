package com.app.tint.component.profile

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.component.MainActivity
import com.app.tint.component.privay.PrivacyPolicyFragment
import com.app.tint.component.profile.userprofile.ProfileUpdateRequest
import com.app.tint.services.TintAPICall
import com.app.tint.services.TintServiceResponseCallback
import com.app.tint.utility.Constants
import com.app.tint.utility.ServiceType
import com.app.tint.utility.TintSingleton
import com.app.tint.utility.showToast
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_log_in.*
import kotlinx.android.synthetic.main.fragment_editprofile.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class EditProfileFragment: Fragment(), TintServiceResponseCallback {

    companion object {

        fun newInstance(): EditProfileFragment {
            return EditProfileFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_editprofile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivBack.visibility = View.VISIBLE
        tvTitle.visibility = View.VISIBLE
        tvDone.visibility = View.VISIBLE
        tvTitle.text = getString(R.string.edit_profile)


        tvDone.setOnClickListener {

            val edtName = edtProfileName.text
            val edtWebsite = edtWebsite.text
            val edtEmail = edtProfileEmail.text
            val edtPhone = edtUserProfilePhone.text
            val edtGender = edtGender.text


            if(!edtPhone.isNullOrBlank()) {
                TintSingleton.getInstance().isMobileEditByUser = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_MOB) != edtPhone.toString()
            } else {
                TintSingleton.getInstance().isMobileEditByUser = false
            }

            if (edtEmail.isNullOrBlank() && edtPhone.isNullOrBlank()) {
                showToast(context!!, "Required Email or Phone number.")
            } else {

                TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_NAME, edtName.toString())
                TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_WEBSITE, edtWebsite.toString())
                TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_USER_EMAIL, edtEmail.toString())
                TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_MOB, edtPhone.toString())
                TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_GENDER, edtGender.toString())


                val profileDetailsRequest = ProfileUpdateRequest()
                profileDetailsRequest.userId = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID)?.toInt()!!
                profileDetailsRequest.name = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_NAME)
                profileDetailsRequest.email = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_NAME)
                profileDetailsRequest.dateOfBirth = 0
                profileDetailsRequest.gender = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_GENDER)
                profileDetailsRequest.phone = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_MOB)
                profileDetailsRequest.website = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_WEBSITE)

                TintAPICall(context, this).makeTintApiCall(profileDetailsRequest, ServiceType.UPDATE_PROFILE)


            }

        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        ivBack.setOnClickListener {

            if(TintSingleton.getInstance().PAGE_FROM == Constants.PAGE_PRIVACY_SAFETY)
                (activity as MainActivity).loadFragment(PrivacyPolicyFragment.newInstance(), "")
            else
                (activity as MainActivity).loadFragment(ProfileFragment.newInstance(), "")

        }

        updateProfileUI()

        tvChangeProfilePic.setOnClickListener {

        }


        if(TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_PROFILE_PIC) != null) {
            Glide.with(context!!)
                    .load(TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_PROFILE_PIC))
                    //.apply(RequestOptions().placeholder(R.mipmap.ic_launcher))
                    .into(ivUserProfile)
        }
    }


    override fun onSuccessTintService(success: Any?, serviceType: Int) {

        if (serviceType == ServiceType.UPDATE_PROFILE) {

            updateProfileUI()

            showToast(context!!, "Profile has been updated.")

        }




    }

    override fun onErrorTintService(error: Any?, serviceType: Int) {

        showToast(context!!, "Error in profile update.")

    }


    private fun updateProfileUI() {
        val name = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_NAME)
        val userName = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_NAME)
        val email = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_EMAIL)
        val phone = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_MOB)
        val webSite = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_WEBSITE)



        edtProfileName.setText(name)
        edtProfileUserName.setText(userName)
        edtWebsite.setText(webSite)
        edtProfileEmail.setText(email)
        edtUserProfilePhone.setText(phone)
        edtGender.setText(TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_GENDER))
    }
}