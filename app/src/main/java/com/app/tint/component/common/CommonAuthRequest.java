package com.app.tint.component.common;


import com.google.gson.annotations.SerializedName;

public class CommonAuthRequest{

    @SerializedName("imgUrl")
    private String imgUrl;

    @SerializedName("pass")
    private String pass;

    @SerializedName("phone")
    private String phone;

    @SerializedName("name")
    private String name;

    @SerializedName("userType")
    private String userType;

    @SerializedName("username")
    private String username;

    @SerializedName("email")
    private String email;

    public void setPass(String pass) {
        this.pass = pass;
    }

    public void setImgUrl(String imgUrl){
        this.imgUrl = imgUrl;
    }

    public String getImgUrl(){
        return imgUrl;
    }

    public void setPhone(String phone){
        this.phone = phone;
    }

    public String getPhone(){
        return phone;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setUserType(String userType){
        this.userType = userType;
    }

    public String getUserType(){
        return userType;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public String getUsername(){
        return username;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public String getEmail(){
        return email;
    }

    @Override
    public String toString(){
        return
                "UserData{" +
                        "imgUrl = '" + imgUrl + '\'' +
                        ",phone = '" + phone + '\'' +
                        ",name = '" + name + '\'' +
                        ",userType = '" + userType + '\'' +
                        ",username = '" + username + '\'' +
                        ",pass = '" + pass + '\'' +
                        ",email = '" + email + '\'' +
                        "}";
    }
}