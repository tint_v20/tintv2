package com.app.tint.utility

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Handler
import android.util.Log
import android.util.Patterns
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.app.tint.R
import com.app.tint.TintApplication
import java.io.UnsupportedEncodingException
import java.net.URLDecoder
import java.net.URLEncoder
import java.text.DecimalFormat


fun postDelayed(delayMillis: Long, task: () -> Unit) {
    Handler().postDelayed(task, delayMillis)
}


fun showToast(mContext: Context, msg: String) {

    Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show()
}


fun convertMilliSecondsToDays(milli: String): String {

    val calDateTime = (milli.toDouble() / (60 * 60 * 24 * 1000))

    return DecimalFormat("##.##").format(calDateTime)
}

fun String.isValidEmail(): Boolean = this.isNotEmpty() &&
        Patterns.EMAIL_ADDRESS.matcher(this).matches()


 fun <T> launchActivity(mContext: Context, activityName: Class<T>, isFinish: Boolean, isClearBack: Boolean, isAnimLeft: Boolean) {

    val intent = Intent(mContext, activityName)

    if (isFinish) {
        (mContext as Activity).finish()
    }
    if (isClearBack) {
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    }

     (mContext as Activity).startActivity(intent)

     if(isAnimLeft)
     mContext.overridePendingTransition(R.anim.enter, R.anim.exit)
     else mContext.overridePendingTransition(R.anim.slide_left_to_right, R.anim.slide_right_to_left)
}


fun isNetworkConnected(context: Context): Boolean {
    val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    return cm.getActiveNetworkInfo() != null
}


fun hideSoftKeyboard(activity: Activity, edtView: EditText) {
    val inputMethodManager = activity.getSystemService(
            Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(
            activity.currentFocus!!.windowToken, 0)

    val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
    imm!!.hideSoftInputFromWindow(edtView.windowToken, 0)
}


private var snapHelper: LinearSnapHelper = object : LinearSnapHelper() {
    override fun findTargetSnapPosition(layoutManager: RecyclerView.LayoutManager?, velocityX: Int, velocityY: Int): Int {
        val centerView = findSnapView(layoutManager!!) ?: return RecyclerView.NO_POSITION

        val position = layoutManager.getPosition(centerView)
        var targetPosition = -1
        if (layoutManager.canScrollHorizontally()) {
            targetPosition = if (velocityX < 0) {
                position - 1
            } else {
                position + 1
            }
        }

        if (layoutManager.canScrollVertically()) {
            targetPosition = if (velocityY < 0) {
                position - 1
            } else {
                position + 1
            }
        }

        val firstItem = 0
        val lastItem = layoutManager.itemCount - 1
        targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem))
        Log.d("targetPosition", ""+  targetPosition)
       // videoPos = targetPosition
        TintApplication.instance.selectedVideoId = TintSingleton.getInstance().videosItemList[targetPosition].videoId

        //TintAPICall(context, this@HomeFragment).makeTintApiCall(TintApplication.instance.perPagePagination, ServiceType.COMMENTS_DETAILS, false)

        return targetPosition
    }

}

