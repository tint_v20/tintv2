package com.app.tint.utility;

import com.app.tint.component.home.UserVideosItem;

import java.util.ArrayList;
import java.util.List;

public class TintSingleton {

    public boolean isPaginationAvailable = false;
    public List<UserVideosItem> videosItemList = new ArrayList<>();
    public String videoURL = null;
    public int deviceHeight  = 0;
    public int deviceWidth  = 0;
    //public int historyVideoCount = 0;
    public int PAGE_FROM = -1;
    public boolean isMobileEditByUser = false;

    public List<UserVideosItem> sliderVideosList = new ArrayList<>();


    private static final TintSingleton ourInstance = new TintSingleton();

    public static TintSingleton getInstance() {
        return ourInstance;
    }

    private TintSingleton() {
    }
}
