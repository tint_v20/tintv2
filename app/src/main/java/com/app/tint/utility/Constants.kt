package com.app.tint.utility

import android.annotation.SuppressLint
import android.content.Context
import android.preference.PreferenceManager
import com.app.tint.TintApplication
import java.io.UnsupportedEncodingException
import java.net.URLDecoder
import java.net.URLEncoder
import java.text.SimpleDateFormat
import java.util.*

object Constants {

    val FB_PERMISSION_LIST = arrayListOf("email", "public_profile")
    const val TINT_USER = "tint_user"
    const val PREFS_KEY_USER_NAME = "USER_NAME"
    const val PREFS_KEY_MOB = "MOB_NUM"
    const val PREFS_KEY_USER_ID = "USER_ID"
    const val PREFS_KEY_USER_PROFILE_PIC = "PROFILE_PIC"
    const val PREFS_KEY_USER_EMAIL = "EMAIL"
    const val PREFS_KEY_NAME = "NAME"
    const val PREFS_KEY_WEBSITE = "WEBSITE"
    const val PREFS_KEY_GENDER = "GENDER"
    const val IS_CAMERA_PERMISSION_GRANTED = "isCameraPermission"
    const val IS_MICROPHONE_PERMISSION_GRANTED = "isMicroPhonePermission"
    const val IS_OPEN_FRESH_AFTER_LOGOUT = "freshOpen"

    const val IMAGE_URL_PATH = "http://humanutilitymanufacturer.com/" + "army_images/"


    const val AUTH_USERNAME = "admin@scanta"
    const val AUTH_PASSWORD = "Landmark122003"


    const val DLG_CANCEL = 1
    const val DLG_CONFIRM = 2
    const val BTM_DLG_SHARE_PROFILE = 3
    const val BTM_DLG_SETTINGS = 7

    const val INSTA_CLIENT_ID = "8a925fa1534e4761b12889134498999e"
    const val INSTA_CALLBACK = "https://instagram.com/"
    const val INSTA_BASE = "https://api.instagram.com"

    const val PAGE_PRIVACY_SAFETY = 1
    const val BTM_DLG_REPORT = 2
    const val BTM_DLG_BLOCK = 3
    const val TYPE_DATA = 4



    fun decodeEmoji(message: String): String {
        return try {
            URLDecoder.decode(
                    message, "UTF-8")
        } catch (e: UnsupportedEncodingException) {
            message
        }

    }


    fun encodeEmoji(message: String): String {
        return try {
            URLEncoder.encode(message,
                    "UTF-8")
        } catch (e: UnsupportedEncodingException) {
            message
        }

    }


     fun setVideoStreamName(context: Context) {

        @SuppressLint("SimpleDateFormat")
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        TintApplication.instance.streamName = TintApplication.instance.sharedPreferences
                .getValue(PREFS_KEY_USER_ID) +"/" + "tintStreamVideo"+timeStamp



        @Suppress("DEPRECATION") val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context)
        val prefsEditor = sharedPrefs.edit()
        prefsEditor.putString("wz_live_stream_name", TintApplication.instance.streamName)
        prefsEditor.apply()

    }
}