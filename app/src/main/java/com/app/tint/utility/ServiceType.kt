package com.app.tint.utility

object ServiceType {

    const val REGISTRATION_SERVICE = 1
    const val LOGIN_SERVICE = 2
    const val FACEBOOK_AUTH = 3
    const val GOOGLE_AUTH = 4
    const val TWITTER_AUTH = 5
    const val SET_PASSWORD = 6
    const val GET_ALL_VIDEOS = 7
    const val SET_VIEW_VIDEOS = 8
    const val GET_VIDEOS_COUNT = 9
    const val GET_TRENDING_VIDEO = 10
    const val GET_POPULAR_VIDEO = 11
    const val START_STREAM = 12
    const val STOP_STREAM = 13
    const val ADD_COMMENT = 14
    const val UPDATE_PROFILE = 15
    const val LOAD_MORE = 16
    const val GET_FAV_VIDEOS = 17
    const val LIKE_UNLIKE = 18
    const val ADD_REMOVE_FAV = 19
    const val VIDEO_HISTORY = 20
    const val PROFILE_DETAILS = 21
    const val COMMENTS_DETAILS = 22
    const val SEARCH_QUERY = 23
    const val FOLLOW_USER = 24
    const val GET_FOLLOWERS_LIST = 25
    const val GET_FOLLOWING_LIST = 26
    const val GET_INSTA_AUTH = 27


}