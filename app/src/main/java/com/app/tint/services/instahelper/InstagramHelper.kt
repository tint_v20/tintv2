package com.app.tint.services.instahelper

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import com.app.tint.R
import com.app.tint.services.instahelper.model.InstaUser
import com.app.tint.services.instahelper.utils.CommonUtils
import com.app.tint.services.instahelper.utils.SharedPrefUtils
import java.text.MessageFormat

class InstagramHelper private constructor(private val clientId: String, private val redirectUri: String, private val scope: String) {

    fun loginFromActivity(context: Activity) {
        var authUrl = MessageFormat.format(InstagramHelperConstants.AUTH_URL
                + InstagramHelperConstants.CLIENT_ID_DEF + "{0}"
                + InstagramHelperConstants.REDIRECT_URI_DEF + "{1}"
                + InstagramHelperConstants.RESPONSE_TYPE_DEF, clientId, redirectUri)

        if (!TextUtils.isEmpty(scope)) {
            authUrl += InstagramHelperConstants.SCOPE_TYPE_DEF + scope
        }

        val intent = Intent(context, InstagramLoginActivity::class.java)

        val bundle = Bundle()
        bundle.putString(InstagramHelperConstants.INSTA_AUTH_URL, authUrl)
        bundle.putString(InstagramHelperConstants.INSTA_REDIRECT_URL, redirectUri)
        intent.putExtras(bundle)
        context.startActivityForResult(intent, InstagramHelperConstants.INSTA_LOGIN)
        //com.scanta.util.Utils.enterScreenAnim(context)
        context.overridePendingTransition(R.anim.enter, R.anim.exit)


    }

    fun getInstagramUser(context: Context): InstaUser {
        return SharedPrefUtils.getInstagramUser(context)
    }

    class Builder {
        private lateinit var clientId: String
        private lateinit var redirectUrl: String
        private lateinit var scope: String

        fun withClientId(clientId: String): Builder {
            this.clientId = CommonUtils.checkNotNull(clientId, "clientId == null")
            return this
        }

        fun withRedirectUrl(redirectUrl: String): Builder {
            this.redirectUrl = CommonUtils.checkNotNull(redirectUrl, "redirectUrl == null")
            return this
        }

        fun withScope(scope: String): Builder {
            this.scope = CommonUtils.checkNotNull(scope, "scope == null")
            return this
        }

        fun build(): InstagramHelper {
            return InstagramHelper(clientId, redirectUrl, scope)
        }
    }
}
