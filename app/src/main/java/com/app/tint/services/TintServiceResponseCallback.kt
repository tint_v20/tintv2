package com.app.tint.services

interface TintServiceResponseCallback {

    fun onSuccessTintService(success: Any?, serviceType: Int)

    fun onErrorTintService(error: Any?, serviceType: Int)
}
