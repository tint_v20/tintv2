package com.app.tint.services

import android.app.Activity
import android.content.Context
import androidx.fragment.app.Fragment
import android.util.Log
import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.component.common.CommonAuthRequest
import com.app.tint.component.common.CommonResponse
import com.app.tint.component.home.LikeResponse
import com.app.tint.component.login.LoginObjectRequest
import com.app.tint.component.login.LoginResponse
import com.app.tint.component.profile.userprofile.fav.FavVideoRequest
import com.app.tint.component.profile.userprofile.fav.FavVideosResponse
import com.app.tint.component.profile.GetUserResponse
import com.app.tint.component.profile.ProfileDetailsRequest
import com.app.tint.component.profile.follow.FollowResponse
import com.app.tint.component.profile.follow.FollowUserRequest
import com.app.tint.component.profile.followers.GetFollowersRequest
import com.app.tint.component.profile.followers.GetFollowersResponse
import com.app.tint.component.profile.following.GetFollowingResponse
import com.app.tint.component.profile.userprofile.history.UserVideoHistoryRequest
import com.app.tint.component.profile.userprofile.history.VideoHistoryResponse
import com.app.tint.component.resetpassword.RequestPassReset
import com.app.tint.component.home.GetAllVideoResponse
import com.app.tint.component.profile.userprofile.ProfileUpdateRequest
import com.app.tint.component.profile.userprofile.ProfileUpdateResponse
import com.app.tint.component.search.SearchQueryResponse
import com.app.tint.component.search.TrendingResponse
import com.app.tint.component.signup.RegistrationResponse
import com.app.tint.component.videos.comments.byid.CommentsByIdResponse
import com.app.tint.component.videos.comments.request.AddCommentRequest
import com.app.tint.component.videos.comments.response.CommentsResponse
import com.app.tint.component.videos.videostream.*
import com.app.tint.component.videos.views.GetVideoCountResponse
import com.app.tint.component.videos.views.SetViewRequest
import com.app.tint.component.videos.views.ViewCountRequest
import com.app.tint.component.videos.views.ViewCountResponse
import com.app.tint.utility.Constants
import com.app.tint.utility.isNetworkConnected
import com.app.tint.utility.ServiceType
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TintAPICall {

    private var webServiceCallBack: TintServiceResponseCallback? = null
    private var mContext: Context? = null

    constructor (_mContext: Context?, activity: Activity) {

        this.mContext = _mContext
        this.webServiceCallBack = activity as TintServiceResponseCallback

    }

    constructor (_mContext: Context?, _fragment: Fragment) {

        this.mContext = _mContext
        this.webServiceCallBack = _fragment as TintServiceResponseCallback

    }
    /*
        *This method create instance of api
  */

    fun makeTintApiCall(hmInput: Any ?, serviceType: Int) {

        Log.d("hmInput", "hmInput" + hmInput.toString())

        if (isNetworkConnected(mContext!!)) {

            val tintApiInterface = TintAPIClient.createService((TintStreamApi::class.java))

            when(serviceType) {

                ServiceType.REGISTRATION_SERVICE -> {

                    if (hmInput != null && hmInput is CommonAuthRequest) {


                        doAsync {

                            uiThread {

                                TintApplication.instance.showLoadingDialog(mContext!!)
                            }

                            val call = tintApiInterface.registrationUser(hmInput)
                            call.enqueue(object : Callback<RegistrationResponse> {
                                override fun onResponse(call: Call<RegistrationResponse>?, response: Response<RegistrationResponse>?) {
                                    uiThread {


                                        TintApplication.instance.hideLoadingDialog()

                                        if (response != null && response.body()!!.userData != null) {

                                            webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)

                                        } else {
                                            webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_already), serviceType)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<RegistrationResponse>?, t: Throwable?) {
                                    uiThread {
                                        webServiceCallBack!!.onErrorTintService(t!!, serviceType)

                                        TintApplication.instance.hideLoadingDialog()
                                    }
                                }
                            })
                        }
                    }
                }

                ServiceType.LOGIN_SERVICE -> {
                    if (hmInput != null && hmInput is LoginObjectRequest) {


                        doAsync {

                            uiThread {

                                TintApplication.instance.showLoadingDialog(mContext!!)
                            }

                            val call = tintApiInterface.loginUser(hmInput)
                            call.enqueue(object : Callback<LoginResponse> {
                                override fun onResponse(call: Call<LoginResponse>?, response: Response<LoginResponse>?) {
                                    uiThread {

                                        TintApplication.instance.hideLoadingDialog()

                                        if (response != null && response.body() is LoginResponse) {

                                            if(response.body()!!.userData != null) {

                                                webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)
                                            } else {
                                                webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)

                                            }

                                        } else {
                                            webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
                                    uiThread {
                                        webServiceCallBack!!.onErrorTintService(t!!, serviceType)

                                        TintApplication.instance.hideLoadingDialog()
                                    }
                                }
                            })
                        }
                    }
                }

                ServiceType.FACEBOOK_AUTH, ServiceType.TWITTER_AUTH, ServiceType.GOOGLE_AUTH -> {
                    if (hmInput != null && hmInput is CommonAuthRequest) {


                        doAsync {

                            uiThread {

                                TintApplication.instance.showLoadingDialog(mContext!!)
                            }

                            val call = tintApiInterface.socialAuth(hmInput)
                            call.enqueue(object : Callback<RegistrationResponse> {
                                override fun onResponse(call: Call<RegistrationResponse>?, response: Response<RegistrationResponse>?) {
                                    uiThread {

                                        TintApplication.instance.hideLoadingDialog()

                                        if (response != null && response.isSuccessful) {

                                            webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)

                                        } else {
                                            webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<RegistrationResponse>?, t: Throwable?) {
                                    uiThread {
                                        webServiceCallBack!!.onErrorTintService(t!!, serviceType)
                                        TintApplication.instance.hideLoadingDialog()
                                    }
                                }
                            })
                        }
                    }
                }

                ServiceType.SET_PASSWORD -> {
                    if (hmInput != null && hmInput is RequestPassReset) {


                        doAsync {

                            uiThread {

                                TintApplication.instance.showLoadingDialog(mContext!!)
                            }

                            val call = tintApiInterface.resetPassword(hmInput)
                            call.enqueue(object : Callback<CommonResponse> {
                                override fun onResponse(call: Call<CommonResponse>?, response: Response<CommonResponse>?) {
                                    uiThread {

                                        TintApplication.instance.hideLoadingDialog()

                                        if (response != null && response.isSuccessful) {

                                            webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)

                                        } else {
                                            webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<CommonResponse>?, t: Throwable?) {
                                    uiThread {
                                        webServiceCallBack!!.onErrorTintService(t!!, serviceType)
                                        TintApplication.instance.hideLoadingDialog()
                                    }
                                }
                            })
                        }
                    }
                }

                ServiceType.GET_ALL_VIDEOS -> {
                    if (hmInput != null && hmInput is Int) {


                        doAsync {

                            var userId = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID)
                            if(userId == null) {
                                userId = "0"
                            }

                            val call = tintApiInterface.getAllVideo(hmInput, userId!!.toInt())
                            call.enqueue(object : Callback<GetAllVideoResponse> {
                                override fun onResponse(call: Call<GetAllVideoResponse>?, response: Response<GetAllVideoResponse>?) {
                                    uiThread {

                                        if (response?.body() != null && response.body()!!.userVideos.size > 0) {

                                            webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)

                                        } else {
                                            webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<GetAllVideoResponse>?, t: Throwable?) {
                                    uiThread {
                                        webServiceCallBack!!.onErrorTintService(t!!, serviceType)
                                    }
                                }
                            })
                        }
                    }
                }

                ServiceType.SET_VIEW_VIDEOS -> {

                        if (hmInput != null && hmInput is SetViewRequest) {

                            doAsync {

                                val call = tintApiInterface.setViewCountVideo(hmInput)
                                call.enqueue(object : Callback<ViewCountResponse> {
                                    override fun onResponse(call: Call<ViewCountResponse>?, response: Response<ViewCountResponse>?) {
                                        uiThread {

                                            if (response != null && response.isSuccessful) {

                                                webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)

                                            } else {
                                                webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                            }
                                        }
                                    }

                                    override fun onFailure(call: Call<ViewCountResponse>?, t: Throwable?) {
                                        uiThread {
                                            webServiceCallBack!!.onErrorTintService(t!!, serviceType)
                                        }
                                    }
                                })
                            }
                        }

                }

                ServiceType.GET_VIDEOS_COUNT -> {

                    if (hmInput != null && hmInput is ViewCountRequest) {

                        doAsync {

                            val call = tintApiInterface.getVideoViewCounts(hmInput)
                            call.enqueue(object : Callback<GetVideoCountResponse> {
                                override fun onResponse(call: Call<GetVideoCountResponse>?, response: Response<GetVideoCountResponse>?) {
                                    uiThread {

                                        if (response != null && response.isSuccessful) {

                                            webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)

                                        } else {
                                            webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<GetVideoCountResponse>?, t: Throwable?) {
                                    uiThread {
                                        webServiceCallBack!!.onErrorTintService(t!!, serviceType)
                                    }
                                }
                            })
                        }
                    }

                }

                ServiceType.GET_TRENDING_VIDEO -> {

                    if (hmInput != null && hmInput is Int) {

                        var userId = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID)
                        if(userId == null) {
                            userId = "0"
                        }

                        doAsync {

                            val call = tintApiInterface.getTrendingVideos(hmInput, userId!!.toInt())
                            call.enqueue(object : Callback<TrendingResponse> {
                                override fun onResponse(call: Call<TrendingResponse>?, response: Response<TrendingResponse>?) {
                                    uiThread {

                                        if (response != null && response.body()!!.userVideos != null) {

                                            webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)

                                        } else {
                                            webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<TrendingResponse>?, t: Throwable?) {
                                    uiThread {
                                        webServiceCallBack!!.onErrorTintService(t!!, serviceType)
                                    }
                                }
                            })
                        }
                    }

                }

                ServiceType.GET_POPULAR_VIDEO -> {

                    if (hmInput != null && hmInput is Int) {

                        var userId = TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_ID)
                        if(userId == null) {
                            userId = "0"
                        }

                        doAsync {
                            uiThread {
                                TintApplication.instance.showLoadingDialog(mContext!!)
                            }

                            val call = tintApiInterface.getPopularVideos(hmInput, userId!!.toInt())
                            call.enqueue(object : Callback<TrendingResponse> {
                                override fun onResponse(call: Call<TrendingResponse>?, response: Response<TrendingResponse>?) {
                                    uiThread {
                                        TintApplication.instance.hideLoadingDialog()
                                        if (response != null && response.body()!!.userVideos != null) {

                                            webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)

                                        } else {
                                            webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<TrendingResponse>?, t: Throwable?) {
                                    uiThread {
                                        webServiceCallBack!!.onErrorTintService(t!!, serviceType)
                                        TintApplication.instance.hideLoadingDialog()
                                    }
                                }
                            })
                        }
                    }

                }

                ServiceType.ADD_COMMENT -> {

                    if (hmInput != null && hmInput is AddCommentRequest) {

                        doAsync {

                            val call = tintApiInterface.addComment(hmInput)
                            call.enqueue(object : Callback<CommentsResponse> {
                                override fun onResponse(call: Call<CommentsResponse>?, response: Response<CommentsResponse>?) {
                                    uiThread {

                                        if (response != null && response.body()!!.userDetails != null) {

                                            webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)

                                        } else {
                                            webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<CommentsResponse>?, t: Throwable?) {
                                    uiThread {
                                        webServiceCallBack!!.onErrorTintService(t!!, serviceType)
                                    }
                                }
                            })
                        }
                    }

                }

                ServiceType.COMMENTS_DETAILS -> {

                    if (hmInput != null && hmInput is Int) {

                        doAsync {

                            val call = tintApiInterface.getCommentsById(hmInput, TintApplication.instance.selectedVideoId)
                            call.enqueue(object : Callback<CommentsByIdResponse> {
                                override fun onResponse(call: Call<CommentsByIdResponse>?, response: Response<CommentsByIdResponse>?) {
                                    uiThread {

                                        if (response!!.body() != null) {

                                            webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)

                                        } else {
                                            //webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<CommentsByIdResponse>?, t: Throwable?) {
                                    uiThread {
                                        webServiceCallBack!!.onErrorTintService(t!!, serviceType)
                                    }
                                }
                            })
                        }
                    }

                }

                ServiceType.START_STREAM -> {

                    if (hmInput != null && hmInput is StartStreamVideoRequest) {

                        doAsync {

                            uiThread {

                                TintApplication.instance.showLoadingDialog(mContext!!)
                            }

                            val call = tintApiInterface.startLiveStreaming(hmInput)
                            call.enqueue(object : Callback<StartVideoStreamResponse> {
                                override fun onResponse(call: Call<StartVideoStreamResponse>?, response: Response<StartVideoStreamResponse>?) {
                                    uiThread {
                                        TintApplication.instance.hideLoadingDialog()

                                        if (response!!.body() != null && response.body()!!.userData != null) {

                                            webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)

                                        } else {
                                            webServiceCallBack!!.onErrorTintService(response.body(), serviceType)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<StartVideoStreamResponse>?, t: Throwable?) {
                                    uiThread {
                                        webServiceCallBack!!.onErrorTintService(t!!, serviceType)
                                        TintApplication.instance.hideLoadingDialog()
                                    }
                                }
                            })
                        }
                    }

                }

                ServiceType.STOP_STREAM -> {

                    if (hmInput != null && hmInput is StopLiveStreamRequest) {

                        doAsync {

                            uiThread {

                                TintApplication.instance.showLoadingDialog(mContext!!)
                            }

                            val call = tintApiInterface.stopLiveStreaming(hmInput)
                            call.enqueue(object : Callback<StopLiveStreamResponse> {
                                override fun onResponse(call: Call<StopLiveStreamResponse>?, response: Response<StopLiveStreamResponse>?) {
                                    uiThread {

                                        TintApplication.instance.hideLoadingDialog()

                                        if (response != null && response.body()!!.status!!.toBoolean()) {

                                            webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)

                                        } else {
                                            webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<StopLiveStreamResponse>?, t: Throwable?) {
                                    uiThread {
                                        webServiceCallBack!!.onErrorTintService(t!!, serviceType)
                                        TintApplication.instance.hideLoadingDialog()
                                    }
                                }
                            })
                        }
                    }

                }

                ServiceType.GET_FAV_VIDEOS -> {

                    if (hmInput != null && hmInput is FavVideoRequest) {

                        doAsync {

                            val call = tintApiInterface.getAllFavVideos(hmInput)
                            call.enqueue(object : Callback<FavVideosResponse> {
                                override fun onResponse(call: Call<FavVideosResponse>?, response: Response<FavVideosResponse>?) {
                                    uiThread {
                                        if (response!!.body() != null && response.body()!!.userFavorite != null) {

                                            webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)

                                        } else {
                                            webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<FavVideosResponse>?, t: Throwable?) {
                                    uiThread {
                                        webServiceCallBack!!.onErrorTintService(t!!, serviceType)
                                    }
                                }
                            })
                        }
                    }

                }

                ServiceType.LIKE_UNLIKE, ServiceType.ADD_REMOVE_FAV -> {

                    if (hmInput != null && hmInput is SetViewRequest) {

                        doAsync {

                            var call = tintApiInterface.doLikeVideo(hmInput)

                            if(serviceType == ServiceType.ADD_REMOVE_FAV) {

                                call = tintApiInterface.addToFav(hmInput)
                            }

                            call.enqueue(object : Callback<LikeResponse> {
                                override fun onResponse(call: Call<LikeResponse>?, response: Response<LikeResponse>?) {
                                    uiThread {

                                        if (response?.body() != null) {

                                            if(response.body()!!.status == "true") {

                                                webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)
                                            } else {
                                                webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                            }

                                        } else {
                                            webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<LikeResponse>?, t: Throwable?) {
                                    uiThread {
                                        webServiceCallBack!!.onErrorTintService(t!!, serviceType)

                                    }
                                }
                            })
                        }
                    }

                }

                ServiceType.VIDEO_HISTORY -> {

                    if (hmInput != null && hmInput is UserVideoHistoryRequest) {

                        doAsync {

                            val call = tintApiInterface.getAllVideoById(hmInput)

                            call.enqueue(object : Callback<VideoHistoryResponse> {
                                override fun onResponse(call: Call<VideoHistoryResponse>?, response: Response<VideoHistoryResponse>?) {
                                    uiThread {
                                        if (response?.body() != null) {

                                            if(response.body()!!.userVideos != null ) {

                                                webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)
                                            } else {
                                                webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                            }

                                        } else {
                                            webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<VideoHistoryResponse>?, t: Throwable?) {
                                    uiThread {
                                        webServiceCallBack!!.onErrorTintService(t!!, serviceType)
                                    }
                                }
                            })
                        }
                    }

                }

                ServiceType.PROFILE_DETAILS -> {

                    if (hmInput != null && hmInput is ProfileDetailsRequest) {

                        doAsync {

                            val call = tintApiInterface.getUserDetails(hmInput)

                            call.enqueue(object : Callback<GetUserResponse> {
                                override fun onResponse(call: Call<GetUserResponse>?, response: Response<GetUserResponse>?) {
                                    uiThread {
                                        if (response?.body() != null) {

                                            if(response.body()!!.userDetails != null ) {

                                                webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)
                                            } else {
                                                webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                            }

                                        } else {
                                            webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<GetUserResponse>?, t: Throwable?) {
                                    uiThread {
                                        webServiceCallBack!!.onErrorTintService(t!!, serviceType)
                                    }
                                }
                            })
                        }
                    }

                }

                ServiceType.SEARCH_QUERY -> {

                    if (hmInput != null && hmInput is Int) {

                        doAsync {

                            val call = tintApiInterface.searchVideoQuery(hmInput)
                            call.enqueue(object : Callback<SearchQueryResponse> {
                                override fun onResponse(call: Call<SearchQueryResponse>?, response: Response<SearchQueryResponse>?) {
                                    uiThread {

                                        if (response!!.body() != null) {

                                            webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)

                                        } else {
                                            //webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<SearchQueryResponse>?, t: Throwable?) {
                                    uiThread {
                                        webServiceCallBack!!.onErrorTintService(t!!, serviceType)
                                    }
                                }
                            })
                        }
                    }

                }

                ServiceType.FOLLOW_USER -> {

                    if (hmInput != null && hmInput is FollowUserRequest) {

                        doAsync {

                            val call = tintApiInterface.followUser(hmInput)

                            call.enqueue(object : Callback<FollowResponse> {
                                override fun onResponse(call: Call<FollowResponse>?, response: Response<FollowResponse>?) {
                                    uiThread {

                                        if (response?.body() != null) {

                                            if(response.body()!!.followedDetails != null ) {

                                                webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)
                                            } else {
                                                webServiceCallBack!!.onSuccessTintService("un-followed", serviceType)
                                            }

                                        } else {
                                            webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<FollowResponse>?, t: Throwable?) {
                                    uiThread {
                                        webServiceCallBack!!.onErrorTintService(t!!, serviceType)

                                    }
                                }
                            })
                        }
                    }

                }

                ServiceType.GET_FOLLOWERS_LIST -> {

                    if (hmInput != null && hmInput is GetFollowersRequest) {

                        doAsync {

                            val call = tintApiInterface.getFollowersList(hmInput)

                            call.enqueue(object : Callback<GetFollowersResponse> {
                                override fun onResponse(call: Call<GetFollowersResponse>?, response: Response<GetFollowersResponse>?) {
                                    uiThread {

                                        TintApplication.instance.hideLoadingDialog()
                                        if (response?.body() != null) {

                                            if(response.body()!!.userFollowers != null ) {

                                                webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)
                                            } else {
                                                webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                            }

                                        } else {
                                            webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<GetFollowersResponse>?, t: Throwable?) {
                                    uiThread {
                                        webServiceCallBack!!.onErrorTintService(t!!, serviceType)
                                    }
                                }
                            })
                        }
                    }

                }

                ServiceType.GET_FOLLOWING_LIST -> {

                    if (hmInput != null && hmInput is GetFollowersRequest) {

                        doAsync {

                            val call = tintApiInterface.getFollowingList(hmInput)

                            call.enqueue(object : Callback<GetFollowingResponse> {
                                override fun onResponse(call: Call<GetFollowingResponse>?, response: Response<GetFollowingResponse>?) {
                                    uiThread {

                                        if (response?.body() != null) {

                                            if(response.body()!!.userFollowing != null ) {

                                                webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)
                                            } else {
                                                webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                            }

                                        } else {
                                            webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<GetFollowingResponse>?, t: Throwable?) {
                                    uiThread {
                                        webServiceCallBack!!.onErrorTintService(t!!, serviceType)
                                    }
                                }
                            })
                        }
                    }

                }

                ServiceType.UPDATE_PROFILE -> {

                    if (hmInput != null && hmInput is ProfileUpdateRequest) {

                        doAsync {

                            val call = tintApiInterface.updateUserProfile(hmInput)

                            call.enqueue(object : Callback<ProfileUpdateResponse> {
                                override fun onResponse(call: Call<ProfileUpdateResponse>?, response: Response<ProfileUpdateResponse>?) {
                                    uiThread {

                                        if (response?.body() != null) {

                                            if(response.body()!!.userData != null ) {

                                                webServiceCallBack!!.onSuccessTintService(response.body(), serviceType)
                                            } else {
                                                webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                            }

                                        } else {
                                            webServiceCallBack!!.onErrorTintService(mContext!!.getString(R.string.err_data), serviceType)
                                        }
                                    }
                                }

                                override fun onFailure(call: Call<ProfileUpdateResponse>?, t: Throwable?) {
                                    uiThread {
                                        webServiceCallBack!!.onErrorTintService(t!!, serviceType)
                                    }
                                }
                            })
                        }
                    }

                }
            }


        } else {
            (mContext as Activity).toast(mContext!!.getString(R.string.err_network))
        }
    }

}