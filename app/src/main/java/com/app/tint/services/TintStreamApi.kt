package com.app.tint.services

import com.app.tint.component.common.CommonAuthRequest
import com.app.tint.component.common.CommonResponse
import com.app.tint.component.home.LikeResponse
import com.app.tint.component.login.LoginObjectRequest
import com.app.tint.component.login.LoginResponse
import com.app.tint.component.profile.*
import com.app.tint.component.profile.follow.FollowResponse
import com.app.tint.component.profile.follow.FollowUserRequest
import com.app.tint.component.profile.followers.GetFollowersRequest
import com.app.tint.component.profile.followers.GetFollowersResponse
import com.app.tint.component.profile.followers.ListFollowersResponse
import com.app.tint.component.profile.following.GetFollowingResponse
import com.app.tint.component.profile.userprofile.fav.FavVideoRequest
import com.app.tint.component.profile.userprofile.fav.FavVideosResponse
import com.app.tint.component.profile.userprofile.history.UserVideoHistoryRequest
import com.app.tint.component.profile.userprofile.history.VideoHistoryResponse
import com.app.tint.component.resetpassword.RequestPassReset
import com.app.tint.component.home.GetAllVideoResponse
import com.app.tint.component.profile.userprofile.ProfileUpdateRequest
import com.app.tint.component.profile.userprofile.ProfileUpdateResponse
import com.app.tint.component.search.SearchQueryResponse
import com.app.tint.component.search.TrendingResponse
import com.app.tint.component.signup.RegistrationResponse
import com.app.tint.component.videos.comments.byid.CommentsByIdResponse
import com.app.tint.component.videos.comments.request.AddCommentRequest
import com.app.tint.component.videos.comments.response.CommentsResponse
import com.app.tint.component.videos.videostream.*
import com.app.tint.component.videos.views.GetVideoCountResponse
import com.app.tint.component.videos.views.SetViewRequest
import com.app.tint.component.videos.views.ViewCountRequest
import com.app.tint.component.videos.views.ViewCountResponse

import retrofit2.Call
import okhttp3.RequestBody
import retrofit2.http.*


interface TintStreamApi {
    @POST("user/registerUser")
    fun registrationUser(@Body body: CommonAuthRequest): Call<RegistrationResponse>

    @POST("user/login")
    fun loginUser(@Body body: LoginObjectRequest): Call<LoginResponse>

    @POST("user/socialRegisterByUserId")
    fun socialAuth(@Body body: CommonAuthRequest): Call<RegistrationResponse>

    @POST("user/resetPasswordByPhone")
    fun resetPassword(@Body requestPassReset: RequestPassReset): Call<CommonResponse>

    @POST("item/imageUpload")
    fun profilePicUpdate(@Part("file\"; filename=\"pp.png\" ") file: RequestBody, @Part("userId") fname: RequestBody, @Part("type") id: RequestBody): Call<ProfilePicResponse>

    @POST("user/startLiveStream")
    fun startLiveStreaming(@Body startStreamVideoRequest: StartStreamVideoRequest): Call<StartVideoStreamResponse>

    @PUT("user/stopLiveStream")
    fun stopLiveStreaming(@Body stopLiveStreamRequest: StopLiveStreamRequest) : Call<StopLiveStreamResponse>

    @GET("user/getAllVideos?")
    fun getAllVideo(@Query(value = "start", encoded = true) key: Int, @Query(value = "userId", encoded = true) userIdKey: Int): Call<GetAllVideoResponse>

    @POST("user/getAllVideosByUserId")
    fun getAllVideoById(@Body uvr: UserVideoHistoryRequest): Call<VideoHistoryResponse>

    @GET("user/getTrendingVideos")
    fun getTrendingVideos(@Query(value = "start", encoded = true) key: Int, @Query(value = "userId", encoded = true) userIdKey: Int): Call<TrendingResponse>

    @GET("user/getPopular")
    fun getPopularVideos(@Query(value = "start", encoded = true) key: Int, @Query(value = "userId", encoded = true) userIdKey: Int): Call<TrendingResponse>

    @POST("user/addComment")
    fun addComment(@Body addCommentRequest: AddCommentRequest) : Call<CommentsResponse>

    @GET("user/getCommentsByVideoId")
    fun getCommentsById(@Query(value = "start", encoded = true) key: Int, @Query(value = "videoId", encoded = true) videoIdKey: Int): Call<CommentsByIdResponse>

    @POST("user/viewVideo")
    fun setViewCountVideo(@Body setViewRequest: SetViewRequest): Call<ViewCountResponse>

    @POST("user/getViewsCount")
    fun getVideoViewCounts(@Body viewCountRequest: ViewCountRequest): Call<GetVideoCountResponse>

    @POST("user/likeVideo")
    fun doLikeVideo(@Body setViewRequest: SetViewRequest): Call<LikeResponse>

    @POST("user/addToFavorite")
    fun addToFav(@Body setViewRequest: SetViewRequest): Call<LikeResponse>

    @POST("user/getFavVideosByUserId")
    fun getAllFavVideos(@Body favVideoRequest: FavVideoRequest): Call<FavVideosResponse>

    @POST("user/followUser")
    fun followUser(@Body followUserRequest: FollowUserRequest): Call<FollowResponse>

    @POST("user/getFollowersByUserId")
    fun getFollowersList(@Body userDetails: com.app.tint.component.profile.UserDetails): Call<ListFollowersResponse>

    @POST("user/getUser")
    fun getUserDetails(@Body profileDetailsRequest: ProfileDetailsRequest): Call<GetUserResponse>

    @GET("user/searchVideo")
    fun searchVideoQuery(@Query(value = "start", encoded = true) key: Int): Call<SearchQueryResponse>

    @POST("user/getFollowers")
    fun getFollowersList(@Body getFollowersRequest: GetFollowersRequest): Call<GetFollowersResponse>

    @POST("user/getFollowing")
    fun getFollowingList(@Body getFollowersRequest: GetFollowersRequest): Call<GetFollowingResponse>

    @POST("user/updateProfile")
    fun updateUserProfile(@Body profileUpdateRequest: ProfileUpdateRequest): Call<ProfileUpdateResponse>


}
