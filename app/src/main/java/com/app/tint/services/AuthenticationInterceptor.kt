package com.app.tint.services

import okhttp3.Interceptor
import okhttp3.Response

class AuthenticationInterceptor(private val auth: String): Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val original = chain.request()

        val builder = original.newBuilder()
                .header("Authorization", auth)

       val req = builder.build()

        return chain.proceed(req)
    }
}