package com.app.tint.services

import com.app.tint.component.common.CommonAuthRequest

interface SocialLoginCallback {

    fun successSocialAuth(type: Int, userData: CommonAuthRequest)
    fun errSocialAuth(type: Int, err: String)
}