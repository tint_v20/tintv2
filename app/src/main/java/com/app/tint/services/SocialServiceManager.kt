package com.app.tint.services

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.component.common.CommonAuthRequest
import com.app.tint.utility.Constants
import com.app.tint.utility.ServiceType
import com.facebook.AccessToken
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.common.api.GoogleApiClient
import com.twitter.sdk.android.core.Callback
import com.twitter.sdk.android.core.TwitterCore
import com.twitter.sdk.android.core.TwitterException
import com.twitter.sdk.android.core.TwitterSession
import com.twitter.sdk.android.core.identity.TwitterAuthClient


class SocialServiceManager(_mContext: Context, activity: Activity) {

    private var mContext: Context? = _mContext
    private var socialLoginCallback: SocialLoginCallback? = null

    init {
        this.socialLoginCallback = activity as SocialLoginCallback
    }


    fun removeFbAuth() {
        LoginManager.getInstance().unregisterCallback(TintApplication.instance.fbCallbackManager!!)
    }


    fun callFacebookAuth() {
        TintApplication.instance.twitterClient = null
        LoginManager.getInstance().logInWithReadPermissions(mContext as Activity, Constants.FB_PERMISSION_LIST)

        LoginManager.getInstance().registerCallback(TintApplication.instance.fbCallbackManager!!, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult) {
                val accessToken = AccessToken.getCurrentAccessToken()
                val request = GraphRequest.newMeRequest(accessToken) { `object`, response ->
                    try {
                        var fbName: String? = null
                        var fbEmail: String? = null
                        var fbId: String? = null
                        if (`object`.has("name")) {
                            fbName = (`object`.getString("name"))
                        }

                        if (`object`.has("id")) {
                            fbId = (`object`.getString("id"))
                        }

                        if (`object`.has("email")) {
                            fbEmail = (`object`.getString("email"))
                        }

                        Log.d("fbName", fbName)

                        val authData = CommonAuthRequest()
                        authData.email = fbEmail
                        authData.name = fbName
                        authData.username = fbId
                        authData.userType = "facebook_user"
                        socialLoginCallback!!.successSocialAuth(ServiceType.FACEBOOK_AUTH, authData)

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                val parameters = Bundle()
                parameters.putString("fields", "id,name,email,gender,birthday,picture.type(large)")
                request.parameters = parameters
                request.executeAsync()

            }
            override fun onCancel() {
                //TODO Auto-generated method stub
                //Toast.makeText(this@MainActivity, "Cancel", Toast.LENGTH_LONG).show()
                socialLoginCallback!!.errSocialAuth(ServiceType.FACEBOOK_AUTH, (mContext as Activity).getString(R.string.login_cancel))

            }
            override fun onError(error: FacebookException) {
                //TODO Auto-generated method stub
                //Toast.makeText(this@MainActivity, "onError", Toast.LENGTH_LONG).show()
                socialLoginCallback!!.errSocialAuth(ServiceType.FACEBOOK_AUTH, error.localizedMessage)
            }
        })
    }



    fun callGoogleAuth(googleApiClient: GoogleApiClient) {

        TintApplication.instance.twitterClient = null

        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient)
        (mContext as Activity). startActivityForResult(signInIntent, ServiceType.GOOGLE_AUTH)

    }



    fun callTwitterAuth() {
        TintApplication.instance.twitterClient = TwitterAuthClient()
        val twitterSession = TwitterCore.getInstance().sessionManager.activeSession

        if(twitterSession == null) {

            //if user is not authenticated start authenticating
            TintApplication.instance.twitterClient!!.authorize(mContext as Activity, object : Callback<TwitterSession>() {
                override fun success(result: com.twitter.sdk.android.core.Result<TwitterSession>?) {

                    // Do something with result, which provides a TwitterSession for making API calls
                    val twitterSessionData = result!!.data

                    //call fetch email only when permission is granted
                    fetchTwitterEmail(twitterSessionData)
                }


                override fun failure(e: TwitterException) {
                    // Do something on failure
                    Toast.makeText(mContext, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show()
                    socialLoginCallback!!.errSocialAuth(ServiceType.TWITTER_AUTH, e.localizedMessage)
                }
            })


        } else {
            Toast.makeText(mContext, "User already authenticated", Toast.LENGTH_SHORT).show();
        }


    }


    /**
     * Before using this feature, ensure that “Request email addresses from users” is checked for your Twitter app.
     *
     * @param twitterSession user logged in twitter session
     */
    private fun fetchTwitterEmail(twitterSession: TwitterSession) {
        TintApplication.instance.twitterClient!!.requestEmail(twitterSession, object : Callback<String>() {
            override fun success(result: com.twitter.sdk.android.core.Result<String>?) {

                val authData = CommonAuthRequest()
                authData.email = result!!.data
                authData.name = twitterSession.userName
                authData.username = ""+twitterSession.userId
                authData.userType = "twitter_user"
                socialLoginCallback!!.successSocialAuth(ServiceType.TWITTER_AUTH, authData)

            }

            override fun failure(exception: TwitterException) {
                Toast.makeText(mContext, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show()
                socialLoginCallback!!.errSocialAuth(ServiceType.TWITTER_AUTH, exception.localizedMessage)
            }
        })
    }


    /*

    CookieSyncManager.createInstance(this);
CookieManager cookieManager = CookieManager.getInstance();
cookieManager.removeSessionCookie();
TwitterCore.getInstance().getSessionManager().clearActiveSession()
     */
}