package com.app.tint.services

import android.text.TextUtils
import android.util.Log
import com.app.tint.BuildConfig
import com.app.tint.utility.Constants
import okhttp3.Credentials
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object TintAPIClient {

    private const val BASE_URL = "http://34.211.172.51/sass/webapi/"

    private val okHttpClient = OkHttpClient.Builder().addInterceptor(HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message -> Log.d("Tint_Res", message) }).apply {
        level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
    })
    private val builder = Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create())
    private var retrofit = builder.build()

   fun <S> createService(serviceClass: Class<S>): S = createService(serviceClass, Constants.AUTH_USERNAME, Constants.AUTH_PASSWORD)

    private fun <S> createService(
            serviceClass: Class<S>, username: String?, password: String?): S {
        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
            val authToken = Credentials.basic(username!!, password!!)
            return createService(serviceClass, authToken)
        }

        return createService(serviceClass, null, null)
    }

    private fun <S> createService(
            serviceClass: Class<S>, authToken: String): S {
        if (!TextUtils.isEmpty(authToken)) {
            val interceptor = AuthenticationInterceptor(authToken)


            if (!okHttpClient.interceptors().contains(interceptor)) {
                okHttpClient.addInterceptor(interceptor)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .connectTimeout(60, TimeUnit.SECONDS)

                builder.client(okHttpClient.build())

                retrofit = builder.build()
            }
        }

        return retrofit.create(serviceClass)
    }

}