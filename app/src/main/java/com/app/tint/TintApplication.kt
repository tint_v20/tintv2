package com.app.tint

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.TrafficStats
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.app.AppCompatDelegate.MODE_NIGHT_AUTO
import android.util.Base64
import android.util.Log
import com.app.tint.app.LoadingDialog
import com.app.tint.services.instahelper.InstagramHelper
import com.app.tint.storage.SharedPrefrences
import com.app.tint.utility.Constants
import com.danikula.videocache.HttpProxyCacheServer
import com.facebook.CallbackManager
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.GoogleApiClient
import com.twitter.sdk.android.core.DefaultLogger
import com.twitter.sdk.android.core.Twitter
import com.twitter.sdk.android.core.TwitterAuthConfig
import com.twitter.sdk.android.core.TwitterConfig
import com.twitter.sdk.android.core.identity.TwitterAuthClient
import com.vanniktech.emoji.EmojiManager
import com.vanniktech.emoji.ios.IosEmojiProvider
import com.wowza.gocoder.sdk.api.WowzaGoCoder
import com.wowza.gocoder.sdk.api.logging.WOWZLog
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class TintApplication: Application() {


    private var loadingDialog: LoadingDialog? = null
    lateinit var sharedPreferences: SharedPrefrences
    var fbCallbackManager: CallbackManager? = null
    var twitterClient: TwitterAuthClient?  = null
    var phoneNo : String? = null
    var selectedUserId: Int = 0
    var perPagePagination: Int = 0
    var selectedVideoId: Int = 0
    private val THREAD_ID = 10000

    private var mGoogleApiClient: GoogleApiClient? = null
    private var gso: GoogleSignInOptions? = null
    var isFollowers = false
    var selectedUserImage: String? = null
    lateinit var streamName: String
    var sGoCoderSDK: WowzaGoCoder? = null
    var mFragment: Fragment? = null

    lateinit var instagramHelper: InstagramHelper
    private var proxy: HttpProxyCacheServer? = null


    companion object {
        lateinit var instance: TintApplication

        private const val SDK_SAMPLE_APP_LICENSE_KEY = "GOSK-6946-010C-66F7-DA9B-AFDB"//"GOSK-A144-010C-9E08-5FE6-6AA7"

        private val TAG = TintApplication::class.java.simpleName
    }


    override fun onCreate() {
        super.onCreate()

        //registerActivityLifecycleCallbacks( );
//        if (LeakCanary.isInAnalyzerProcess(this)) {
//            return
//        }
        TrafficStats.setThreadStatsTag(THREAD_ID);
        AppCompatDelegate.setDefaultNightMode(MODE_NIGHT_AUTO)

        EmojiManager.install( IosEmojiProvider())

//        if (BuildConfig.DEBUG) {
//          StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder().detectAll().build())
//          StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder().detectAll().build())
//        }

        //LeakCanary.install(this)

        instance = this


        sharedPreferences = SharedPrefrences(this)

        fbCallbackManager  = CallbackManager.Factory.create()

        getHashKey()

        if (sGoCoderSDK == null) {
            // Enable detailed logging from the GoCoder SDK
            WOWZLog.LOGGING_ENABLED = true

            // Initialize the GoCoder SDK
            sGoCoderSDK = WowzaGoCoder.init(this, SDK_SAMPLE_APP_LICENSE_KEY)

            if (sGoCoderSDK == null) {
                WOWZLog.error(TAG, "Test "+ WowzaGoCoder.getLastError())
            } else {
                Log.e(TAG, "init SDK true")
            }
        }

        gso  = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                //.requestIdToken(WEB_CLIENT_ID)
                .requestEmail()
                .build()


        val config = TwitterConfig.Builder(this)
                .logger(DefaultLogger(Log.DEBUG))//enable logging when app is in debug mode
                .twitterAuthConfig(TwitterAuthConfig(resources.getString(R.string.com_twitter_sdk_android_CONSUMER_KEY), resources.getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET)))//pass the created app Consumer KEY and Secret also called API Key and Secret
                .debug(true)//enable debug mode
                .build()

        //finally initialize twitter with created configs
        Twitter.initialize(config)


        instagramHelper = InstagramHelper.Builder()
                .withClientId(Constants.INSTA_CLIENT_ID)
                .withRedirectUrl(Constants.INSTA_CALLBACK)
                .withScope("likes+comments")
                .build()
    }


//    fun getInstagramHelper(): InstagramHelper {
//        return instagramHelper
//    }

    fun getProxy(context: Context): HttpProxyCacheServer {
        val app = context.applicationContext as TintApplication
        if (app.proxy == null) {
            app.proxy = app.newProxy()
        } else app.proxy!!

        return app.proxy!!
    }

    private fun newProxy(): HttpProxyCacheServer {
        return HttpProxyCacheServer(this)
    }



    fun showLoadingDialog(context: Context) {
        if (loadingDialog != null && loadingDialog!!.isShowing) {
            return
        } else {
            loadingDialog = LoadingDialog(context)
            loadingDialog?.setCanceledOnTouchOutside(false)
            loadingDialog?.show()
        }
    }


    fun hideLoadingDialog() {
        if (loadingDialog != null && loadingDialog!!.isShowing) {
            loadingDialog?.dismiss()
        }
    }

    fun cancelLoadingDialog() {
        if (loadingDialog != null && loadingDialog!!.isShowing) {
            loadingDialog?.cancel()
            loadingDialog = null
        }
    }

    override fun onLowMemory() {
        super.onLowMemory()

        cancelLoadingDialog()
    }



    @SuppressLint("PackageManagerGetSignatures")
    private fun getHashKey() {
        val info: PackageInfo
        try {
            @Suppress("DEPRECATION")
            info = this.packageManager.getPackageInfo("com.app.tint", PackageManager.GET_SIGNATURES)
            @Suppress("DEPRECATION")
            for (signature in info.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val something = String(Base64.encode(md.digest(), 0))
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash_key", something)
            }
        } catch (e: NoSuchAlgorithmException) {
            Log.e("no such an algorithm", e.toString())
        } catch (e: Exception) {
            Log.e("exception", e.toString())
        }

    }


    fun getGoogleApiClient(activity: AppCompatActivity, listener: GoogleApiClient.OnConnectionFailedListener): GoogleApiClient {

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(activity, listener)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso!!)
                .build()
        return mGoogleApiClient!!
    }







}