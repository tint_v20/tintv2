package com.app.tint.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class TyntTextViewBd extends androidx.appcompat.widget.AppCompatTextView {

    public TyntTextViewBd(Context context) {
        super( context );
        setFont();

    }

    public TyntTextViewBd(Context context, AttributeSet attrs) {
        super( context, attrs );
        setFont();
    }

    public TyntTextViewBd(Context context, AttributeSet attrs, int defStyle) {
        super( context, attrs, defStyle );
        setFont();
    }

    private void setFont() {

//        Typeface bold = Typeface.createFromAsset( getContext().getAssets(), "fonts/gothicb.ttf" );
//        setTypeface( bold, Typeface.BOLD );

        Typeface normal = Typeface.createFromAsset(getContext().getAssets(),"fonts/HelveticaNeueLTPro-Bd.otf");
        setTypeface( normal, Typeface.NORMAL );

    }




}
