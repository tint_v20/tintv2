package com.app.tint.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class TyntTextViewMd extends androidx.appcompat.widget.AppCompatTextView {

    public TyntTextViewMd(Context context) {
        super( context );
        setFont();

    }

    public TyntTextViewMd(Context context, AttributeSet attrs) {
        super( context, attrs );
        setFont();
    }

    public TyntTextViewMd(Context context, AttributeSet attrs, int defStyle) {
        super( context, attrs, defStyle );
        setFont();
    }

    private void setFont() {

//        Typeface bold = Typeface.createFromAsset( getContext().getAssets(), "fonts/gothicb.ttf" );
//        setTypeface( bold, Typeface.BOLD );

        Typeface normal = Typeface.createFromAsset(getContext().getAssets(),"fonts/HelveticaNeueLTPro-Md.otf");
        setTypeface( normal, Typeface.NORMAL );

    }




}
