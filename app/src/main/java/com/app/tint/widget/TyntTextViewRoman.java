package com.app.tint.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class TyntTextViewRoman extends androidx.appcompat.widget.AppCompatTextView {

    public TyntTextViewRoman(Context context) {
        super( context );
        setFont();

    }

    public TyntTextViewRoman(Context context, AttributeSet attrs) {
        super( context, attrs );
        setFont();
    }

    public TyntTextViewRoman(Context context, AttributeSet attrs, int defStyle) {
        super( context, attrs, defStyle );
        setFont();
    }

    private void setFont() {

//        Typeface bold = Typeface.createFromAsset( getContext().getAssets(), "fonts/gothicb.ttf" );
//        setTypeface( bold, Typeface.BOLD );

        Typeface normal = Typeface.createFromAsset(getContext().getAssets(),"fonts/HelveticaNeueLTPro-Roman.otf");
        setTypeface( normal, Typeface.NORMAL );

    }




}
