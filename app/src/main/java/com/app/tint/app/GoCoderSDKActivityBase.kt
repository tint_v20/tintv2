/**
 * This is sample code provided by Wowza Media Systems, LLC.  All sample code is intended to be a reference for the
 * purpose of educating developers, and is not intended to be used in any production environment.
 *
 * IN NO EVENT SHALL WOWZA MEDIA SYSTEMS, LLC BE LIABLE TO YOU OR ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 * EVEN IF WOWZA MEDIA SYSTEMS, LLC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * WOWZA MEDIA SYSTEMS, LLC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. ALL CODE PROVIDED HEREUNDER IS PROVIDED "AS IS".
 * WOWZA MEDIA SYSTEMS, LLC HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 * © 2015 – 2019 Wowza Media Systems, LLC. All rights reserved.
 */

package com.app.tint.app

import android.app.Activity
import android.app.FragmentManager
import android.content.DialogInterface
import android.content.SharedPreferences
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.preference.PreferenceManager
import androidx.core.app.ActivityCompat
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.view.ContextThemeWrapper
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity

import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.component.videos.GoCoderSDKPrefs
import com.wowza.gocoder.sdk.api.WowzaGoCoder
import com.wowza.gocoder.sdk.api.broadcast.WOWZBroadcast
import com.wowza.gocoder.sdk.api.broadcast.WOWZBroadcastAPI
import com.wowza.gocoder.sdk.api.broadcast.WOWZBroadcastConfig
import com.wowza.gocoder.sdk.api.configuration.WOWZMediaConfig
import com.wowza.gocoder.sdk.api.data.WOWZDataMap
import com.wowza.gocoder.sdk.api.errors.WOWZError
import com.wowza.gocoder.sdk.api.errors.WOWZStreamingError
import com.wowza.gocoder.sdk.api.logging.WOWZLog
import com.wowza.gocoder.sdk.api.monitor.WOWZStreamingStat
import com.wowza.gocoder.sdk.api.status.WOWZStatus
import com.wowza.gocoder.sdk.api.status.WOWZStatusCallback
import java.util.*

abstract class GoCoderSDKActivityBase : AppCompatActivity(), WOWZStatusCallback {

    protected var mRequiredPermissions = arrayOf<String>()

    // GoCoder SDK top level interface
    // protected static WowzaGoCoder sGoCoderSDK = null;

    protected var mPermissionsGranted = false
    private var hasRequestedPermissions = false

    var broadcast: WOWZBroadcast? = null
        protected set

    protected var mWZNetworkLogLevel = WOWZLog.LOG_LEVEL_DEBUG

    protected lateinit var mGoCoderSDKPrefs: GoCoderSDKPrefs

    private var callbackFunction: CameraActivityBase.PermissionCallbackInterface? = null

    var broadcastConfig: WOWZBroadcastConfig? = null
        protected set


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //        if (sGoCoderSDK == null) {
        //            // Enable detailed logging from the GoCoder SDK
        //            WOWZLog.LOGGING_ENABLED = true;
        //
        //            // Initialize the GoCoder SDK
        //            sGoCoderSDK = WowzaGoCoder.init(this, SDK_SAMPLE_APP_LICENSE_KEY);
        //
        //            if (sGoCoderSDK == null) {
        //                WOWZLog.error(TAG, WowzaGoCoder.getLastError());
        //            }
        //        }

        if (TintApplication.instance.sGoCoderSDK != null) {
            // Create a new instance of the preferences mgr
            mGoCoderSDKPrefs = GoCoderSDKPrefs()

            // Create an instance for the broadcast configuration
            broadcastConfig = WOWZBroadcastConfig(WOWZMediaConfig.FRAME_SIZE_1280x720)

            // Create a broadcaster instance
            broadcast = WOWZBroadcast()
            broadcast!!.logLevel = WOWZLog.LOG_LEVEL_DEBUG
        }
    }

    protected fun hasDevicePermissionToAccess(callback: CameraActivityBase.PermissionCallbackInterface) {
        this.callbackFunction = callback
        if (broadcast != null) {
            var result = true
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                result = if (mRequiredPermissions.size > 0) WowzaGoCoder.hasPermissions(this, mRequiredPermissions) else true
                if (!result && !hasRequestedPermissions) {
                    ActivityCompat.requestPermissions(this, mRequiredPermissions, PERMISSIONS_REQUEST_CODE)
                    hasRequestedPermissions = true
                } else {
                    this.callbackFunction!!.onPermissionResult(result)
                }
            } else {
                this.callbackFunction!!.onPermissionResult(result)
            }
        }
    }

    protected fun hasDevicePermissionToAccess(source: String): Boolean {

        val permissionRequestArr = arrayOf(source)
        var result = false
        if (broadcast != null) {
            result = true
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                result = if (mRequiredPermissions.size > 0) WowzaGoCoder.hasPermissions(this, permissionRequestArr) else true
            }
        }
        return result
    }

    protected fun hasDevicePermissionToAccess(): Boolean {
        var result = false
        if (broadcast != null) {
            result = true
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                result = if (mRequiredPermissions.size > 0) WowzaGoCoder.hasPermissions(this, mRequiredPermissions) else true
                if (!result && !hasRequestedPermissions) {
                    ActivityCompat.requestPermissions(this, mRequiredPermissions, PERMISSIONS_REQUEST_CODE)
                    hasRequestedPermissions = true
                }
            }
        }
        return result
    }

    /**
     * Android Activity lifecycle methods
     */
    override fun onResume() {
        super.onResume()

        mPermissionsGranted = this.hasDevicePermissionToAccess()
        if (mPermissionsGranted) {
            syncPreferences()
        }
    }

    override fun onPause() {
        WOWZLog.debug("GoCoderSDKActivityBase - onResume")
        // Stop any active live stream
        if (broadcast != null && broadcast!!.status.isRunning) {
            endBroadcast(true)
        }

        super.onPause()
    }

    /**
     * Click handler for the in button
     */
    //    public void onAbout(View v) {
    //        // Display the About fragment
    //        AboutFragment aboutFragment = AboutFragment.newInstance();
    //        getFragmentManager().beginTransaction()
    //                .replace(android.R.id.content, aboutFragment)
    //                .addToBackStack(null)
    //                .commit();
    //    }

    // Return correctly from any fragments launched and placed on the back stack
    override fun onBackPressed() {
        val fm = fragmentManager
        if (fm.backStackEntryCount > 0) {
            fm.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        mPermissionsGranted = true
        when (requestCode) {
            PERMISSIONS_REQUEST_CODE -> {
                for (grantResult in grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        mPermissionsGranted = false
                    }
                }
            }
        }
        if (this.callbackFunction != null)
            this.callbackFunction!!.onPermissionResult(mPermissionsGranted)
    }

    /**
     * Enable Android's sticky immersive full-screen mode
     * See http://developer.android.com/training/system-ui/immersive.html#sticky
     */
    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)

        if (sFullScreenActivity && hasFocus)
            hideSystemUI()
    }

    fun hideSystemUI() {
        val rootView = window.decorView.findViewById<View>(android.R.id.content)
        if (rootView != null)
            rootView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
    }

    fun showSystemUI() {
        val rootView = window.decorView.findViewById<View>(android.R.id.content)
        if (rootView != null)
            rootView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)

    }

    /**
     * WOWZStatusCallback interface methods
     */
    override fun onWZStatus(goCoderStatus: WOWZStatus) {
        Handler(Looper.getMainLooper()).post {
            if (goCoderStatus.isReady) {
                // Keep the screen on while the broadcast is active
                window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

                // Since we have successfully opened up the server connection, store the connection info for auto complete
                broadcastConfig!!.streamName = TintApplication.instance.streamName

                GoCoderSDKPrefs.storeHostConfig(PreferenceManager.getDefaultSharedPreferences(this@GoCoderSDKActivityBase), broadcastConfig!!)
            } else if (goCoderStatus.isIdle) {
                // Clear the "keep screen on" flag
                window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            }
        }
    }

    override fun onWZError(goCoderStatus: WOWZStatus) {
        Handler(Looper.getMainLooper()).post { WOWZLog.error(TAG, goCoderStatus.lastError) }
    }

    @Synchronized
    protected fun startBroadcast(): WOWZStreamingError? {
        var configValidationError: WOWZStreamingError? = null

        if (broadcast!!.status.isIdle) {

            // Set the detail level for network logging output
            broadcast!!.logLevel = mWZNetworkLogLevel

            //
            // An example of adding metadata values to the stream for use with the onMetadata()
            // method of the IMediaStreamActionNotify2 interface of the Wowza Streaming Engine Java
            // API for server modules.
            //
            // See http://www.wowza.com/resources/serverapi/com/wowza/wms/stream/IMediaStreamActionNotify2.html
            // for additional usage information on IMediaStreamActionNotify2.
            //

            // Add stream metadata describing the current device and platform
            val streamMetadata = WOWZDataMap()
            streamMetadata.put("androidRelease", Build.VERSION.RELEASE)
            streamMetadata.put("androidSDK", Build.VERSION.SDK_INT)
            streamMetadata.put("deviceProductName", Build.PRODUCT)
            streamMetadata.put("deviceManufacturer", Build.MANUFACTURER)
            streamMetadata.put("deviceModel", Build.MODEL)

            broadcastConfig!!.streamMetadata = streamMetadata

            //
            // An example of adding query strings for use with the getQueryStr() method of
            // the IClient interface of the Wowza Streaming Engine Java API for server modules.
            //
            // See http://www.wowza.com/resources/serverapi/com/wowza/wms/client/IClient.html#getQueryStr()
            // for additional usage information on getQueryStr().
            //
            try {
                val pInfo = packageManager.getPackageInfo(packageName, 0)

                // Add query string parameters describing the current app
                val connectionParameters = WOWZDataMap()
                connectionParameters.put("appPackageName", pInfo.packageName)
                connectionParameters.put("appVersionName", pInfo.versionName)
                connectionParameters.put("appVersionCode", pInfo.versionCode)

                broadcastConfig!!.connectionParameters = connectionParameters

            } catch (e: PackageManager.NameNotFoundException) {
                WOWZLog.error(TAG, e)
            }

            val mediaConfig = broadcastConfig!!.videoSourceConfig
            mediaConfig.videoFramerate = broadcastConfig!!.videoFramerate
            mediaConfig.videoFrameHeight = broadcastConfig!!.videoFrameHeight
            mediaConfig.videoFrameWidth = broadcastConfig!!.videoFrameWidth
            broadcastConfig!!.videoSourceConfig = mediaConfig
            WOWZLog.info(TAG, "=============== Broadcast Configuration ===============\n"
                    + broadcastConfig!!.toString()
                    + "\n=======================================================")


            configValidationError = broadcastConfig!!.validateForBroadcast()

            if (configValidationError == null) {


                /// Setup abr bitrate and framerate listeners. EXAMPLE
                //                mWZBroadcastConfig.setABREnabled(false);
                //                ListenToABRChanges abrHandler = new ListenToABRChanges();
                //                mWZBroadcast.registerAdaptiveBitRateListener(abrHandler);
                //                mWZBroadcast.registerAdaptiveFrameRateListener(abrHandler);
                //                mWZBroadcastConfig.setFrameRateLowBandwidthSkipCount(1);

                WOWZLog.debug("***** [FPS]GoCoderSDKActivity " + broadcastConfig!!.videoFramerate)
                broadcast!!.startBroadcast(broadcastConfig, this)
            }
        } else {
            WOWZLog.error(TAG, "startBroadcast() called while another broadcast is active")
        }
        return configValidationError
    }

    internal inner class ListenToABRChanges : WOWZBroadcastAPI.AdaptiveChangeListener {
        override fun adaptiveBitRateChange(broadcastStat: WOWZStreamingStat, newBitRate: Int): Int {
            WOWZLog.debug(TAG, "adaptiveBitRateChange[$newBitRate]")

            return 500
        }

        override fun adaptiveFrameRateChange(broadcastStat: WOWZStreamingStat, newFrameRate: Int): Int {
            WOWZLog.debug(TAG, "adaptiveFrameRateChange[$newFrameRate]")
            return 20
        }
    }

    @Synchronized
    protected fun endBroadcast(appPausing: Boolean) {
        WOWZLog.debug("MP4", "endBroadcast")
        if (!broadcast!!.status.isIdle) {
            WOWZLog.debug("MP4", "endBroadcast-notidle")
            if (appPausing) {
                // Stop any active live stream
                sBroadcastEnded = false
                broadcast!!.endBroadcast(object : WOWZStatusCallback {
                    override fun onWZStatus(wzStatus: WOWZStatus) {
                        WOWZLog.debug("MP4", "onWZStatus::$wzStatus")
                        synchronized(sBroadcastLock) {
                            sBroadcastEnded = true
                            sBroadcastLock.notifyAll()
                        }
                    }

                    override fun onWZError(wzStatus: WOWZStatus) {
                        WOWZLog.debug("MP4", "onWZStatus::" + wzStatus.lastError)
                        WOWZLog.error(TAG, wzStatus.lastError)
                        synchronized(sBroadcastLock) {
                            sBroadcastEnded = true
                            sBroadcastLock.notifyAll()
                        }
                    }
                })

                while (!sBroadcastEnded) {
                    try {
                        sBroadcastLock.wait()
                    } catch (e: InterruptedException) {
                    }

                }
            } else {
                broadcast!!.endBroadcast(this)
            }
        } else {
            WOWZLog.error(TAG, "endBroadcast() called without an active broadcast")
        }
    }

    @Synchronized
    protected fun endBroadcast() {
        endBroadcast(false)
    }

    open fun syncPreferences() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        mWZNetworkLogLevel = Integer.valueOf(prefs.getString("wz_debug_net_log_level", WOWZLog.LOG_LEVEL_DEBUG.toString())!!)

        val bandwidthMonitorLogLevel = Integer.valueOf(prefs.getString("wz_debug_bandwidth_monitor_log_level", 0.toString())!!)
        WOWZBroadcast.LOG_STAT_SUMMARY = bandwidthMonitorLogLevel > 0
        WOWZBroadcast.LOG_STAT_SAMPLES = bandwidthMonitorLogLevel > 1

        if (broadcastConfig != null)
            GoCoderSDKPrefs.updateConfigFromPrefs(prefs, broadcastConfig!!)
    }

    /**
     * Display an alert dialog containing an error message.
     *
     * @param errorMessage The error message text
     */
    protected fun displayErrorDialog(errorMessage: String) {
        // Log the error message
        try {
            WOWZLog.error(TAG, "ERROR: $errorMessage")

            // Display an alert dialog containing the error message
            val builder = AlertDialog.Builder(ContextThemeWrapper(this, R.style.myDialog))
            //AlertDialog.Builder builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
            builder.setMessage(errorMessage)
                    .setTitle(R.string.dialog_title_error)
            builder.setPositiveButton(R.string.dialog_button_close) { dialog, id -> dialog.dismiss() }

            builder.create().show()
        } catch (ex: Exception) {
        }

    }

    /**
     * Display an alert dialog containing the error message for
     * an error returned from the GoCoder SDK.
     *
     * @param goCoderSDKError An error returned from the GoCoder SDK.
     */
    protected fun displayErrorDialog(goCoderSDKError: WOWZError) {
        displayErrorDialog(goCoderSDKError.errorDescription)
    }

    companion object {

        private val TAG = GoCoderSDKActivityBase::class.java.simpleName

        //private static final String SDK_SAMPLE_APP_LICENSE_KEY = "GOSK-5442-0101-750D-4A14-FB5C";
        private val SDK_SAMPLE_APP_LICENSE_KEY = "GOSK-6946-010C-66F7-DA9B-AFDB"

        private val PERMISSIONS_REQUEST_CODE = 0x1

        private val sBroadcastLock = Object()
        private var sBroadcastEnded = true

        // indicates whether this is a full screen activity or note
        protected var sFullScreenActivity = true
    }
}
