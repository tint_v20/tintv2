package com.app.tint.app.slider

import android.annotation.SuppressLint
import android.content.Context
import androidx.viewpager.widget.ViewPager
import android.util.AttributeSet
import android.util.Log
import android.view.animation.DecelerateInterpolator
import android.widget.Scroller
import java.lang.reflect.Field

class SliderView : ViewPager {

    private val DEFAULT_SCROLL_DURATION = 200

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        init()
    }

    @SuppressLint("ClickableViewAccessibility")
    fun init() {
        setDurationScroll(DEFAULT_SCROLL_DURATION)
        this.setOnTouchListener { _, _ -> true }
    }

    fun setDurationScroll(millis: Int) {

        try {
            val viewPager: Class<*> = ViewPager::class.java
            val scroller: Field = viewPager.getDeclaredField("mScroller")
            scroller.isAccessible = true
            scroller.set(this, OwnScroller(context, millis))
        } catch (e: Exception) {
            Log.i("info", e.message)
        }
    }

    inner class OwnScroller(context: Context, durationScroll: Int) : Scroller(context, DecelerateInterpolator()) {
        private var durationScrollMillis = 1

        init {
            this.durationScrollMillis = durationScroll
        }

        override fun startScroll(startX: Int, startY: Int, dx: Int, dy: Int, duration: Int) {
            super.startScroll(startX, startY, dx, dy, durationScrollMillis)
        }
    }
}