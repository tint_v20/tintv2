package com.app.tint.app.slider

import android.content.Context
import android.content.res.Resources
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import androidx.viewpager.widget.ViewPager
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import org.jetbrains.annotations.NotNull

class IndikatorSlider(
        @NotNull var context: Context,
        var container: LinearLayout,
        private var viewPager: ViewPager,
        private var drawableRes: Int
) : ViewPager.OnPageChangeListener {

    private var mHal: Int = 0
    private var mSize: Int = 0
    private var mSpasi: Int = 0
    private var mHalInit: Int = 0

    private val DEFAULTSIZE = 12
    private val DEFAULTSPACING = 12

    fun setPageCount(jmlhPage: Int) {
        mHal = jmlhPage
    }

    @Suppress("unused")
    fun setInitialHalaman(hal: Int) {
        mHalInit = hal
    }

    @Suppress("unused")
    fun setDrawable(@DrawableRes drawable: Int) {
        drawableRes = drawable
    }

    @Suppress("unused")
    fun setSpasi(@DimenRes spacingRes: Int) {
        mSpasi = spacingRes
    }

    @Suppress("unused")
    fun setSize(@DimenRes dimenRes: Int) {
        mSize = dimenRes
    }

    fun show() {
        initIndicator()
        setIndicatorSeek(mHalInit)
        android.os.Handler().postDelayed({ viewPager.currentItem = 1 }, 2500)
    }

    private fun initIndicator() {
        if (mHal <= 0) {
            return
        }
        viewPager.addOnPageChangeListener(this)
        val res: Resources = context.resources
        container.removeAllViews()
        for (i in 0 until mHal) {
            val view = View(context)
            val dimens: Int =
                if (mSize != 0) res.getDimensionPixelSize(mSize) else ((res.displayMetrics.density * DEFAULTSIZE).toInt())
            val margin: Int =
                if (mSpasi != 0) res.getDimensionPixelSize(mSpasi) else ((res.displayMetrics.density * DEFAULTSPACING).toInt())
            val lp = LinearLayout.LayoutParams(dimens, dimens)
            lp.setMargins(if (i == 0) 0 else margin, 0, 0, 0)
            view.layoutParams = lp
            view.setBackgroundResource(drawableRes)
            view.isSelected = i == 0
            container.addView(view)
        }
    }

    private fun setIndicatorSeek(index: Int) {
        container.let {
            for (i in 0 until container.childCount) {
                val view: View = container.getChildAt(i)
                view.isSelected = (i == index)
            }
        }
    }

    @Suppress("unused")
    fun cleanUp() {
        viewPager.clearOnPageChangeListeners()
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {


    }

    override fun onPageSelected(position: Int) {
        val index: Int = position % mHal
        setIndicatorSeek(index)
        val moveTo: Int = position + 1
        try {
            android.os.Handler().postDelayed({ viewPager.currentItem = moveTo }, 2500)
        } catch (e: Exception) {
            Log.i("Info", e.message)
        }
    }
}