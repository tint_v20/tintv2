package com.app.tint.app

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.tint.R
import com.app.tint.utility.Constants
import kotlinx.android.synthetic.main.bottom_sheet_layout.*

@SuppressLint("ValidFragment")
class BottomSheetFragment @SuppressLint("ValidFragment") constructor(_mContext: Context, _fragment: Fragment) : BottomSheetDialogFragment() {

    private var onBottomSheetInterface: OnBottomSheetInterface
    private var mContext: Context

    private var fragmentView: View? = null

    init {
        this.onBottomSheetInterface = _fragment as OnBottomSheetInterface
        this.mContext = _mContext
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentView = inflater.inflate(R.layout.bottom_sheet_layout, container, false)
        return fragmentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        tvShareProfile.setOnClickListener {

            onBottomSheetInterface.onSelectedBottomSheet(Constants.BTM_DLG_SHARE_PROFILE)
            dismiss()
        }


        tvSettings.setOnClickListener {

            onBottomSheetInterface.onSelectedBottomSheet(Constants.BTM_DLG_SETTINGS)
            dismiss()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        fragmentView = null
    }

    interface OnBottomSheetInterface {
        fun onSelectedBottomSheet(code: Int)
    }
}