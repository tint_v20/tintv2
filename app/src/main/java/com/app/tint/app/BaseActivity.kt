package com.app.tint.app

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Point
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.component.MainActivity
import com.app.tint.component.common.CommonAuthRequest
import com.app.tint.component.common.CommonResponse
import com.app.tint.component.forgotauth.ForgotPasswordActivity
import com.app.tint.component.login.LoginActivity
import com.app.tint.component.login.LoginObjectRequest
import com.app.tint.component.login.LoginResponse
import com.app.tint.component.resetpassword.RequestPassReset
import com.app.tint.component.signup.RegistrationResponse
import com.app.tint.component.signup.SignupActivity
import com.app.tint.component.home.GetAllVideoResponse
import com.app.tint.services.SocialLoginCallback
import com.app.tint.services.TintAPICall
import com.app.tint.services.TintServiceResponseCallback
import com.app.tint.services.instahelper.InstagramHelperConstants
import com.app.tint.utility.*
import com.facebook.accountkit.AccountKitLoginResult
import com.facebook.accountkit.PhoneNumber
import com.facebook.accountkit.ui.AccountKitActivity
import com.facebook.accountkit.ui.AccountKitConfiguration
import com.facebook.accountkit.ui.LoginType
import com.facebook.accountkit.ui.SkinManager
import com.facebook.accountkit.ui.UIManager
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import org.jetbrains.anko.toast


abstract class BaseActivity: GoCoderSDKActivityBase(), SocialLoginCallback, TintServiceResponseCallback, GoogleApiClient.OnConnectionFailedListener {

    protected var mGoogleApiClient: GoogleApiClient? = null
    protected var activityContext: Context? = null

    companion object {
        var APP_REQUEST_CODE = 99
    }


    interface OnOTPVerification {
        fun oppVerified()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activityContext = this

        mGoogleApiClient = (application as TintApplication).getGoogleApiClient(this, this)

         val display = windowManager.defaultDisplay
            val size = Point()
            display.getSize(size)
            TintSingleton.getInstance().deviceHeight = size.y
            TintSingleton.getInstance().deviceWidth = size.x

    }


    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.d("onConnectionFailed", "onConnectionFailed:$connectionResult")
    }

    override fun onStart() {
        mGoogleApiClient!!.connect()
        super.onStart()
//        val opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient)
//        if (opr.isDone) {
//            Log.d("onStart", "Got cached sign-in")
//            val result = opr.get()
//            handleSignInResult(result)
//        } else {
//            opr.setResultCallback { googleSignInResult ->
//                //hideProgressDialog();
//                handleSignInResult(googleSignInResult)
//            }
//        }


    }


    protected fun callManualRegistration(commonAuthRequest: CommonAuthRequest) {

        TintAPICall(this, this).makeTintApiCall(commonAuthRequest, ServiceType.REGISTRATION_SERVICE)

    }

    protected fun callManualLogin(loginObjectRequest: LoginObjectRequest) {

        TintAPICall(this, this).makeTintApiCall(loginObjectRequest, ServiceType.LOGIN_SERVICE)
    }

    protected fun callSetPassword(requestPassReset: RequestPassReset) {

        TintAPICall(this, this).makeTintApiCall(requestPassReset, ServiceType.SET_PASSWORD)

    }

    protected fun processOtpVerification(phoneNo: String) {
        val intent = Intent(this, AccountKitActivity::class.java)

        val uiManager: UIManager

        uiManager = SkinManager(SkinManager.Skin.CONTEMPORARY, Color.parseColor("#AF61CE"))

        val configurationBuilder = AccountKitConfiguration.AccountKitConfigurationBuilder(
                LoginType.PHONE,
                AccountKitActivity.ResponseType.CODE
        ) // or .ResponseType.TOKEN
        // ... perform additional configuration ...
        configurationBuilder.setUIManager(uiManager)
        val phoneNumber = PhoneNumber("+91", phoneNo, "IN") // country code, phone number, country code

        configurationBuilder.setInitialPhoneNumber(phoneNumber)
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build()
        )
        startActivityForResult(intent, APP_REQUEST_CODE)
    }


    override fun onStop() {
        mGoogleApiClient!!.disconnect()
        super.onStop()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == InstagramHelperConstants.INSTA_LOGIN && resultCode == Activity.RESULT_OK) {
            val user = TintApplication.instance.instagramHelper.getInstagramUser(this)
            //App.preferences.put(Constants.KEY_INSTA_TOKEN, user.data.id)
            //loginBtn.setVisibility(View.GONE)
            //userInfoPanel.setVisibility(View.VISIBLE)
            //Picasso.with(this).load(user.data.profilePicture).into(userPhoto)
            //userTextInfo.setText(user.data.username + "\n"
            //      + user.data.fullName + "\n"
            //    + user.data.website
            Log.d("InstaUser: ", user.data.username)

            val commonAuthRequest = CommonAuthRequest()
            commonAuthRequest.userType = "insta_user"
            commonAuthRequest.email = ""
            commonAuthRequest.name = user.data.fullName
            commonAuthRequest.username = user.data.username

            //toast("Internal Server error")
            TintAPICall(this, this).makeTintApiCall(commonAuthRequest, ServiceType.GET_INSTA_AUTH)


        } else if(requestCode == InstagramHelperConstants.INSTA_LOGIN) {
            Toast.makeText(this, "Login failed", Toast.LENGTH_LONG).show()
        }
        else if (requestCode == ServiceType.GOOGLE_AUTH) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            handleSignInResult(result)
        }

        else if (requestCode == APP_REQUEST_CODE) { // confirm that this response matches your request
            val loginResult = data!!.getParcelableExtra<AccountKitLoginResult>(AccountKitLoginResult.RESULT_KEY)
            val toastMessage: String
            if (loginResult.error != null) {
                toastMessage = loginResult.error!!.errorType.message

            } else if (loginResult.wasCancelled()) {
                toastMessage = "Login Cancelled"
            } else {
                toastMessage = if (loginResult.accessToken != null) {
                    "Success: " + loginResult.accessToken!!.accountId

                } else {
                    String.format(
                            "Success: ",
                            loginResult.authorizationCode!!.substring(0, 10)
                    )
                }

                when (activityContext) {
                    is SignupActivity -> (activityContext as OnOTPVerification).oppVerified()
                    is ForgotPasswordActivity -> (activityContext as OnOTPVerification).oppVerified()
                }
            }

            toast(toastMessage)
        }

        else if(TintApplication.instance.twitterClient != null) {

            TintApplication.instance.twitterClient!!.onActivityResult(requestCode, resultCode, data)
        }

        else {

            try {
                TintApplication.instance.fbCallbackManager!!.onActivityResult(requestCode, resultCode, data)
            } catch (e: Exception) {
                e.printStackTrace()

            }
        }
    }


    private fun handleSignInResult(result: GoogleSignInResult) {
        Log.d("Google", "handleSignInResult:" + result.isSuccess)
        if (result.isSuccess) {
            // Signed in successfully, show authenticated UI.
            val userAccount = result.signInAccount
            val userId = userAccount!!.id
            val displayedUsername = userAccount.displayName
            val userEmail = userAccount.email
            //val userProfilePhoto = userAccount.photoUrl!!.toString()

            //todo call login via Google using our API

            val commonAuthRequest = CommonAuthRequest()
            commonAuthRequest.userType = "google_user"
            commonAuthRequest.email = userEmail
            commonAuthRequest.name = displayedUsername
            commonAuthRequest.username = userId

            //toast("Internal Server error")
            TintAPICall(this, this).makeTintApiCall(commonAuthRequest, ServiceType.GOOGLE_AUTH)

        }
    }


    @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
    override fun successSocialAuth(serviceType: Int, userData: CommonAuthRequest) {

        when(serviceType) {
            ServiceType.FACEBOOK_AUTH -> {

                //toast("Internal Server error")
                //todo call OWN LOGIN API via fb

                TintAPICall(this, this).makeTintApiCall(userData, ServiceType.FACEBOOK_AUTH)
            }

            ServiceType.TWITTER_AUTH -> {

                //toast("Internal Server error")
                //todo call OWN LOGIN API via twitter

                TintAPICall(this, this).makeTintApiCall(userData, ServiceType.TWITTER_AUTH)
            }
        }



    }

    @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
    override fun errSocialAuth(serviceType: Int, error: String) {
        when(serviceType) {
            ServiceType.FACEBOOK_AUTH -> {

                toast("Fb error")
            }

            ServiceType.TWITTER_AUTH -> {

                toast("Twitter error")
            }
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()

        if (activityContext is MainActivity)
        overridePendingTransition(R.anim.slide_left_to_right, R.anim.slide_right_to_left)
        else overridePendingTransition(R.anim.enter, R.anim.exit)
        this.finish()
    }


    override fun onSuccessTintService(success: Any?, serviceType: Int) {

        if(success != null && !isFinishing) {

            when (serviceType) {

                ServiceType.FACEBOOK_AUTH, ServiceType.TWITTER_AUTH, ServiceType.GOOGLE_AUTH, ServiceType.REGISTRATION_SERVICE, ServiceType.GET_INSTA_AUTH -> {

                    if (success is RegistrationResponse) {

                        if(success.userData!= null ) {
                            toast(getString(R.string.success_registration))

                            if(success.userData!!.userName != null)
                            TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_USER_NAME, success.userData!!.userName)
                            TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_USER_ID, "" + success.userData!!.userId)

                            if(success.userData!!.name != null)
                            TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_NAME, success.userData!!.name)

                            if(success.userData!!.email != null)
                            TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_USER_EMAIL, success.userData!!.email)

                            if(success.userData!!.imgUrl != null)
                            TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_USER_PROFILE_PIC, success.userData!!.imgUrl)

                            if(success.userData!!.phone != null)
                                TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_MOB, success.userData!!.phone)

//                        TintAPICall(this, this)
//                                .makeTintApiCall(TintApplication.instance.perPagePagination,
//                                        ServiceType.GET_ALL_VIDEOS, false)

                            launchActivity(this, MainActivity::class.java, isFinish = true, isClearBack = true, isAnimLeft = true)
                        }

                    } else {
                            toast(success.toString())
                    }

                }

                ServiceType.LOGIN_SERVICE -> {

                    if(success is LoginResponse) {

                        TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_USER_NAME, success.userData!!.userName)
                        TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_USER_ID, "" + success.userData!!.userId)
                        //TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_NAME, success.userData!!.name)
                        //TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_USER_EMAIL, success.userData!!.email)
                        TintApplication.instance.sharedPreferences.save(Constants.PREFS_KEY_USER_PROFILE_PIC, success.userData!!.imgUrl)

//                        TintAPICall(this, this)
//                                .makeTintApiCall(TintApplication.instance.perPagePagination,
//                                        ServiceType.GET_ALL_VIDEOS, false)
                        launchActivity(this, MainActivity::class.java, isFinish = true, isClearBack = true, isAnimLeft =true)


                    } else{
                        toast(success.toString())
                    }


                }

                ServiceType.GET_ALL_VIDEOS -> {

                    if (success is GetAllVideoResponse) {

                        if(success.userVideos.size > 0) {

                            TintSingleton.getInstance().isPaginationAvailable = success.end != -1

                            TintSingleton.getInstance().videosItemList = success.userVideos

                            TintApplication.instance.selectedVideoId = TintSingleton.getInstance().videosItemList[0].videoId
                        }

                        launchActivity(this, MainActivity::class.java, isFinish = true, isClearBack = true, isAnimLeft =true)

                    }
                }

                ServiceType.SET_PASSWORD -> {

                    if(success is CommonResponse) {
                        toast(success.message)

                        postDelayed(500) {

                            if(TintSingleton.getInstance().PAGE_FROM == Constants.PAGE_PRIVACY_SAFETY)
                                this.finish()
                            else
                                launchActivity(this, LoginActivity::class.java, isFinish = true, isClearBack = true, isAnimLeft =true)
                        }

                    } else{
                        toast(success.toString())
                    }


                }
            }
        }
    }

    override fun onErrorTintService(error: Any?, serviceType: Int) {

        if(error!= null) {
            toast(error.toString())
        }

    }


    override fun onDestroy() {

        TintApplication.instance.cancelLoadingDialog()
        TintApplication.instance.phoneNo = null
        super.onDestroy()
    }

}