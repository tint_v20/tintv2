package com.app.tint.app

import android.animation.ObjectAnimator
import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.Window
import android.view.animation.OvershootInterpolator
import com.app.tint.R
import kotlinx.android.synthetic.main.dialog_loading.*


class LoadingDialog(internal var context: Context) : AlertDialog(context, R.style.Theme_TranslucentDialog) {

    public override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_loading)
        rlLoadingDialog.bringToFront()

        val innerRingAnimator = ObjectAnimator.ofFloat(ivInnerRing, "rotation", 0f, 360f)
        val outerRingAnimator = ObjectAnimator.ofFloat(ivOuterRing, "rotation", 360f, 0f)
        innerRingAnimator.interpolator = OvershootInterpolator()
        outerRingAnimator.interpolator = OvershootInterpolator()
        innerRingAnimator.duration = 1700
        outerRingAnimator.duration = 1700
        innerRingAnimator.repeatCount = ObjectAnimator.INFINITE
        outerRingAnimator.repeatCount = ObjectAnimator.INFINITE

        innerRingAnimator.start()
        outerRingAnimator.start()
    }


    override fun onBackPressed() {

    }
}
