/**
 * This is sample code provided by Wowza Media Systems, LLC.  All sample code is intended to be a reference for the
 * purpose of educating developers, and is not intended to be used in any production environment.
 *
 * IN NO EVENT SHALL WOWZA MEDIA SYSTEMS, LLC BE LIABLE TO YOU OR ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 * EVEN IF WOWZA MEDIA SYSTEMS, LLC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * WOWZA MEDIA SYSTEMS, LLC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. ALL CODE PROVIDED HEREUNDER IS PROVIDED "AS IS".
 * WOWZA MEDIA SYSTEMS, LLC HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 *
 * © 2015 – 2019 Wowza Media Systems, LLC. All rights reserved.
 */

package com.app.tint.app

import android.Manifest
import android.app.FragmentManager
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.preference.PreferenceManager
import android.view.View
import android.view.WindowManager
import android.widget.Toast

import com.app.tint.R
import com.app.tint.TintApplication
import com.app.tint.component.videos.GoCoderSDKPrefs
import com.app.tint.component.videos.MultiStateButton
import com.app.tint.component.videos.StatusView
import com.wowza.gocoder.sdk.api.WowzaGoCoder
import com.wowza.gocoder.sdk.api.configuration.WOWZMediaConfig
import com.wowza.gocoder.sdk.api.devices.WOWZAudioDevice
import com.wowza.gocoder.sdk.api.devices.WOWZCamera
import com.wowza.gocoder.sdk.api.devices.WOWZCameraView
import com.wowza.gocoder.sdk.api.errors.WOWZError
import com.wowza.gocoder.sdk.api.errors.WOWZStreamingError
import com.wowza.gocoder.sdk.api.geometry.WOWZSize
import com.wowza.gocoder.sdk.api.graphics.WOWZColor
import com.wowza.gocoder.sdk.api.logging.WOWZLog
import com.wowza.gocoder.sdk.api.status.WOWZStatus

import java.util.Arrays

abstract class CameraActivityBase : GoCoderSDKActivityBase(), WOWZCameraView.PreviewStatusListener {

    // UI controls
    protected var mBtnBroadcast: MultiStateButton? = null
    protected var mBtnSettings: MultiStateButton? = null
    protected var mStatusView: StatusView? = null
    protected var isStartStreaming = false

    // The GoCoder SDK camera preview display view
    protected var mWZCameraView: WOWZCameraView? = null
    protected var mWZAudioDevice: WOWZAudioDevice? = null

    private var mDevicesInitialized = false
    private var mUIInitialized = false

    private var mPrefsChangeListener: SharedPreferences.OnSharedPreferenceChangeListener? = null

    private val backStackListener = FragmentManager.OnBackStackChangedListener {
        val mainHandler = Handler(Looper.getMainLooper())

        val myRunnable = Runnable {
            if (broadcastConfig!!.isVideoEnabled) {
                // Start the camera preview display
                mWZCameraView!!.stopPreview()
                mWZCameraView!!.startPreview()
            } else {
                mWZCameraView!!.stopPreview()
            }
        }
        mainHandler.post(myRunnable)
    }

    private var prefsFragment: GoCoderSDKPrefs.PrefsFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        prefsFragment = GoCoderSDKPrefs.PrefsFragment()
    }

    //define callback interface
     interface PermissionCallbackInterface {

        fun onPermissionResult(result: Boolean)
    }

    /**
     * Android Activity lifecycle methods
     */
    override fun onResume() {
        super.onResume()

        if (!mUIInitialized) {
            initUIControls()
        }
        if (!mDevicesInitialized) {
            initGoCoderDevices()
        }

        this.hasDevicePermissionToAccess(object : PermissionCallbackInterface {

            override fun onPermissionResult(result: Boolean) {
                if (!mDevicesInitialized || result) {
                    initGoCoderDevices()
                }
            }
        })

        if (TintApplication.instance.sGoCoderSDK != null && this.hasDevicePermissionToAccess(Manifest.permission.CAMERA)) {
            val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this)

            /// Set mirror capability.
            //mWZCameraView.setSurfaceExtension(mWZCameraView.EXTENSION_MIRROR);

            // Update the camera preview display config based on the stored shared preferences

            mWZCameraView!!.setCameraConfig(broadcastConfig)
            mWZCameraView!!.scaleMode = GoCoderSDKPrefs.getScaleMode(sharedPrefs)
            mWZCameraView!!.videoBackgroundColor = WOWZColor.DARKGREY

            val base = this


            // Setup up a shared preferences change listener to update the camera preview
            // as the related preference values change
            mPrefsChangeListener = SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, prefsKey ->
                if (mWZCameraView != null && Arrays.binarySearch(CAMERA_CONFIG_PREFS_SORTED, prefsKey) != -1) {

                    if (prefsKey.equals("wz_video_framerate", ignoreCase = true)) {
                        val currentFrameRate = mWZCameraView!!.framerate.toString()
                        val frameRate = sharedPreferences.getString("wz_video_framerate", currentFrameRate)
                        broadcastConfig!!.videoFramerate = Integer.parseInt(frameRate!!)
                        broadcastConfig!!.videoFramerate = Integer.parseInt(frameRate)
                        broadcastConfig!!.videoSourceConfig.videoFramerate = Integer.parseInt(frameRate)
                    }

                    if (prefsKey.equals("wz_video_bitrate", ignoreCase = true)) {
                        val currentBitrate = mWZCameraView!!.framerate.toString()
                        val bitrate = sharedPreferences.getString("wz_video_bitrate", currentBitrate)
                        broadcastConfig!!.videoBitRate = Integer.parseInt(bitrate!!)
                        broadcastConfig!!.videoFramerate = Integer.parseInt(bitrate)
                        broadcastConfig!!.videoSourceConfig.videoFramerate = Integer.parseInt(bitrate)
                    }

                    // Update the camera preview display frame size

                    if (prefsKey.equals("wz_video_frame_width", ignoreCase = true) || prefsKey.equals("wz_video_frame_height", ignoreCase = true)) {
                        val currentFrameSize = mWZCameraView!!.frameSize
                        val prefsFrameWidth = sharedPreferences.getInt("wz_video_frame_width", currentFrameSize.getWidth())
                        val prefsFrameHeight = sharedPreferences.getInt("wz_video_frame_height", currentFrameSize.getHeight())
                        val prefsFrameSize = WOWZSize(prefsFrameWidth, prefsFrameHeight)
                        if (prefsFrameSize != currentFrameSize)
                            mWZCameraView!!.frameSize = prefsFrameSize
                    }
                    if (prefsKey.equals("wz_video_resize_to_aspect", ignoreCase = true)) {
                        val scaleMode = sharedPreferences.getBoolean("wz_video_resize_to_aspect", false) // ? WOWZMediaConfig.RESIZE_TO_ASPECT : WOWZMediaConfig.FILL_VIEW;
                        if (scaleMode) {
                            mWZCameraView!!.scaleMode = WOWZMediaConfig.RESIZE_TO_ASPECT  //WOWZMediaConfig.RESIZE_TO_ASPECT :
                        } else {
                            mWZCameraView!!.scaleMode = WOWZMediaConfig.FILL_VIEW  //WOWZMediaConfig.RESIZE_TO_ASPECT :
                        }
                    }

                    mWZCameraView!!.setCameraConfig(broadcastConfig)

                    // Toggle the camera preview on or off
                    val videoEnabled = sharedPreferences.getBoolean("wz_video_enabled", broadcastConfig!!.isVideoEnabled)
                    if (videoEnabled && !mWZCameraView!!.isPreviewing) {
                        mWZCameraView!!.startPreview()
                    } else if (!videoEnabled && mWZCameraView!!.isPreviewing) {
                        mWZCameraView!!.videoBackgroundColor = WOWZColor.BLACK
                    }
                    mWZCameraView!!.clearView()
                    mWZCameraView!!.stopPreview()

                }
            }

            sharedPrefs.registerOnSharedPreferenceChangeListener(mPrefsChangeListener)

            val ref = this
            val handler = Handler()
            handler.postDelayed({
                if (broadcastConfig!!.isVideoEnabled && !mWZCameraView!!.isPreviewing) {
                    mWZCameraView!!.startPreview(broadcastConfig, ref)

                } else {
                    mWZCameraView!!.stopPreview()
                    Toast.makeText(ref, "The video stream is currently turned off", Toast.LENGTH_LONG).show()
                }
            }, 300)
        }
        syncUIControlState()
    }

    override fun onWZCameraPreviewStarted(wzCamera: WOWZCamera, wzSize: WOWZSize, i: Int) {
        // Briefly display the video configuration
        Toast.makeText(this, broadcastConfig!!.getLabel(true, true, false, true), Toast.LENGTH_LONG).show()
    }

    override fun onWZCameraPreviewStopped(cameraId: Int) {}

    override fun onWZCameraPreviewError(wzCamera: WOWZCamera, wzError: WOWZError) {
        displayErrorDialog(wzError)
    }

    override fun onPause() {
        super.onPause()

        if (mPrefsChangeListener != null) {
            val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this)
            sharedPrefs.unregisterOnSharedPreferenceChangeListener(mPrefsChangeListener)
        }

        if (mWZCameraView != null && mWZCameraView!!.isPreviewing) {
            mWZCameraView!!.stopPreview()
        }
    }

    /**
     * WOWZStatusCallback interface methods
     */
    override fun onWZStatus(goCoderStatus: WOWZStatus) {

        Handler(Looper.getMainLooper()).post {
            if (goCoderStatus.isRunning) {
                // Keep the screen on while we are broadcasting
                window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

                // Since we have successfully opened up the server connection, store the connection info for auto complete
                broadcastConfig!!.streamName = TintApplication.instance.streamName
                GoCoderSDKPrefs.storeHostConfig(PreferenceManager.getDefaultSharedPreferences(this@CameraActivityBase), broadcastConfig!!)
            } else if (goCoderStatus.isIdle) {
                // Clear the "keep screen on" flag
                window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            }

            if (mStatusView != null) mStatusView!!.setStatus(goCoderStatus)
            syncUIControlState()
        }
    }

    override fun onWZError(goCoderStatus: WOWZStatus) {
        Handler(Looper.getMainLooper()).post {
            if (mStatusView != null) mStatusView!!.setStatus(goCoderStatus)
            syncUIControlState()
        }
    }

    /**
     * Click handler for the broadcast button
     */
    protected fun onToggleBroadcast() {
        if (broadcast == null) return

        if (broadcast!!.status.isIdle) {
            if (!broadcastConfig!!.isVideoEnabled && !broadcastConfig!!.isAudioEnabled) {
                Toast.makeText(this, "Unable to publish if both audio and video are disabled", Toast.LENGTH_LONG).show()
            } else {

                WOWZLog.debug("Scale Mode: -> " + mWZCameraView!!.scaleMode)

                if (!broadcastConfig!!.isAudioEnabled) {
                    Toast.makeText(this, "The audio stream is currently turned off", Toast.LENGTH_LONG).show()
                }

                if (!broadcastConfig!!.isVideoEnabled) {
                    Toast.makeText(this, "The video stream is currently turned off", Toast.LENGTH_LONG).show()
                }
                val configError = startBroadcast()
                if (configError != null) {
                    if (mStatusView != null)
                        mStatusView!!.setErrorMessage(configError.errorDescription)
                }
            }
            isStartStreaming = true
        } else {
            endBroadcast()
            isStartStreaming = false
        }
    }



    @Suppress("DEPRECATION")
    private fun showSettings() {
        // Display the prefs fragment

        WOWZLog.debug("*** getOriginalFrameSizes showSettings1")
        prefsFragment!!.setActiveCamera(if (mWZCameraView != null) mWZCameraView!!.camera else null)

        WOWZLog.debug("*** getOriginalFrameSizes showSettings2")
        fragmentManager.addOnBackStackChangedListener(backStackListener)
        fragmentManager.beginTransaction()
                .replace(android.R.id.content, prefsFragment)
                .addToBackStack(null)
                .show(prefsFragment)
                .commit()
        WOWZLog.debug("*** getOriginalFrameSizes showSettings3")
    }


    /**
     * Click handler for the settings button
     */
    fun onSettings() {
        WOWZLog.debug("*** getOriginalFrameSizes Do we have permission?")
        if (this.hasDevicePermissionToAccess(Manifest.permission.CAMERA) && this.hasDevicePermissionToAccess(Manifest.permission.RECORD_AUDIO)) {
            WOWZLog.debug("*** getOriginalFrameSizes Do we have permission = yes")
            this.showSettings()
        } else {
            Toast.makeText(this, "You must enable audio / video access to update the settings", Toast.LENGTH_LONG).show()
        }
    }

    protected fun initGoCoderDevices() {
        if (TintApplication.instance.sGoCoderSDK != null) {
            var videoIsInitialized = false
            var audioIsInitialized = false

            // Initialize the camera preview
            if (this.hasDevicePermissionToAccess(Manifest.permission.CAMERA)) {
                if (mWZCameraView != null) {
                    val availableCameras = mWZCameraView!!.cameras
                    // Ensure we can access to at least one camera
                    if (availableCameras.size > 0) {
                        // Set the video broadcaster in the broadcast config
                        broadcastConfig!!.videoBroadcaster = mWZCameraView
                        videoIsInitialized = true


                        object : Thread() {
                            override fun run() {
                                WOWZLog.debug("*** getOriginalFrameSizes - Get original frame size : ")
                                prefsFragment!!.setActiveCamera(if (mWZCameraView != null) mWZCameraView!!.camera else null)
                                fragmentManager.beginTransaction().replace(android.R.id.content, prefsFragment).hide(prefsFragment).commit()
                            }
                        }.start()

                    } else {
                        mStatusView!!.setErrorMessage("Could not detect or gain access to any cameras on this device")
                        broadcastConfig!!.isVideoEnabled = false
                    }
                }
            }

            if (this.hasDevicePermissionToAccess(Manifest.permission.RECORD_AUDIO)) {
                // Initialize the audio input device interface
                mWZAudioDevice = WOWZAudioDevice()

                // Set the audio broadcaster in the broadcast config
                broadcastConfig!!.audioBroadcaster = mWZAudioDevice
                audioIsInitialized = true
            }

            if (videoIsInitialized && audioIsInitialized)
                mDevicesInitialized = true
        }
    }

    protected fun initUIControls() {
        // Initialize the UI controls
        mBtnBroadcast = findViewById<View>(R.id.ic_broadcast) as MultiStateButton
        mBtnSettings = findViewById<View>(R.id.ic_settings) as MultiStateButton
        mStatusView = findViewById<View>(R.id.statusView) as StatusView

        // The GoCoder SDK camera view
        mWZCameraView = findViewById<View>(R.id.cameraPreview) as WOWZCameraView

        mUIInitialized = true

        if (TintApplication.instance.sGoCoderSDK == null && mStatusView != null)
            mStatusView!!.setErrorMessage(WowzaGoCoder.getLastError().errorDescription)
    }

    protected open fun syncUIControlState(): Boolean {
        val disableControls = broadcast == null || !(broadcast!!.status.isIdle || broadcast!!.status.isRunning)
        val isStreaming = broadcast != null && broadcast!!.status.isRunning

        if (disableControls) {
            if (mBtnBroadcast != null) mBtnBroadcast!!.isEnabled = false
            if (mBtnSettings != null) mBtnSettings!!.isEnabled = false
        } else {
            if (mBtnBroadcast != null) {
                mBtnBroadcast!!.setState(isStreaming)
                mBtnBroadcast!!.isEnabled = true
            }
            if (mBtnSettings != null)
                mBtnSettings!!.isEnabled = !isStreaming
        }

        return disableControls
    }

    companion object {

        private val BASE_TAG = CameraActivityBase::class.java.simpleName

        private val CAMERA_CONFIG_PREFS_SORTED = arrayOf("wz_video_enabled", "wz_video_frame_size", "wz_video_preset")
    }
}
