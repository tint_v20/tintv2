package com.app.tint.app.slider

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.app.tint.R
import com.app.tint.component.common.AlertDialogue
import com.bumptech.glide.Glide

class FragmentSlider : Fragment() {

    companion object {
        private const val ARG_PARAM1 = "paramThumb"
        private const val ARG_PARAM2 = "paramVideo"
        private const val ARG_PARAM3 = "paramAvailableOn"
        private const val ARG_PARAM4 = "paramVideoDes"
        @JvmStatic
        fun newInstance(paramThumb: String, paramVideo: String, paramAvailableOn: String, paramVideoDes: String): FragmentSlider {
            val fragment = FragmentSlider()
            val arg = Bundle()
            arg.putString(ARG_PARAM1, paramThumb)
            arg.putString(ARG_PARAM2, paramVideo)
            arg.putString(ARG_PARAM3, paramAvailableOn)
            arg.putString(ARG_PARAM4, paramVideoDes)
            fragment.arguments = arg
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
        val image = arguments!!.getString(ARG_PARAM1)
        val desc = arguments!!.getString(ARG_PARAM4)
        val availableOn = arguments!!.getString(ARG_PARAM3)
        //val availableOn = arguments!!.getString(ARG_PARAM4)
        val view: View = inflater.inflate(R.layout.fragment_fragment_slider, container, false)
        val img: ImageView = view.findViewById(R.id.img)
        val tvAvailable: TextView = view.findViewById(R.id.tvAvailable)
        val tvDescription: TextView = view.findViewById(R.id.tvDescription)
//        Log.i("Image",image)
        //Picasso.get().load(image).into(img)
        Glide.with(context!!).load(image).into(img)

        tvAvailable.text = availableOn
        tvDescription.text = desc


        img.setOnClickListener {
            val alertDialogue = AlertDialogue(context!!, "Show will start at \n $availableOn", getString(R.string.okay))
            if (!alertDialogue.isShowing) {
                alertDialogue.show()
            }
        }

        return view
    }

}// Required empty public constructor
