package com.app.tint.app.slider

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.app.tint.app.slider.FragmentSlider

class SliderPagerAdapter(fm: FragmentManager, fragments: List<Fragment>) : FragmentStatePagerAdapter(fm) {

    private var mFragments: List<Fragment> = ArrayList()

    init {
        mFragments = fragments
    }


    override fun getItem(position: Int): Fragment {
        //var fragment : Fragment
        val index: Int = position % mFragments.size
        //fragment = FragmentSlider()
        //return fragment
        @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
        return FragmentSlider.newInstance(mFragments[index].arguments!!.getString("paramThumb")!!,
                mFragments[index].arguments!!.getString("paramVideo")!!,
                mFragments[index].arguments!!.getString("paramAvailableOn")!!,
                mFragments[index].arguments!!.getString("paramVideoDes")!!)
    }

    override fun getCount(): Int {
        return Int.MAX_VALUE
    }
}