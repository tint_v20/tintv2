package com.app.tint

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.app.tint.app.BaseActivity
import com.app.tint.component.MainActivity
import com.app.tint.component.login.LoginActivity
import com.app.tint.component.home.GetAllVideoResponse
import com.app.tint.services.TintServiceResponseCallback
import com.app.tint.utility.*
import org.jetbrains.anko.toast


class SplashActivity : AppCompatActivity(), TintServiceResponseCallback {


    companion object {
        const val SCREEN_TIME = 3000L
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        TintApplication.instance.perPagePagination = 0

        if(TintApplication.instance.sharedPreferences.getValue(Constants.PREFS_KEY_USER_NAME) == null) {

            postDelayed(SCREEN_TIME) {

                launchActivity(this, LoginActivity::class.java, isFinish = true, isClearBack = false, isAnimLeft =true)
            }
        } else {
            //todo call Home Activity

            postDelayed(SCREEN_TIME) {

//                TintAPICall(this, this)
//                        .makeTintApiCall(TintApplication.instance.perPagePagination,
//                            ServiceType.GET_ALL_VIDEOS, false)
                launchActivity(this, MainActivity::class.java, isFinish = true, isClearBack = false, isAnimLeft =true)


            }
        }


    }


    override fun onSuccessTintService(success: Any?, serviceType: Int) {

        if (success != null && !isFinishing) {

            when (serviceType) {

                ServiceType.GET_ALL_VIDEOS -> {

                        if (success is GetAllVideoResponse) {

                            if(success.userVideos.size > 0) {

                                TintSingleton.getInstance().isPaginationAvailable = success.end != -1

                                TintSingleton.getInstance().videosItemList = success.userVideos

                                TintApplication.instance.selectedVideoId = TintSingleton.getInstance().videosItemList[0].videoId

                            }

                            launchActivity(this, MainActivity::class.java, isFinish = true, isClearBack = false, isAnimLeft =true)

                        }
                    }

            }
        }
    }

    override fun onErrorTintService(error: Any?, serviceType: Int) {

        if (error != null && !isFinishing) {
            toast(error.toString())
        }

    }
}
