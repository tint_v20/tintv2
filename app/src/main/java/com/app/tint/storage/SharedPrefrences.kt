package com.app.tint.storage

import android.content.Context
import android.content.SharedPreferences

class SharedPrefrences(private val mContext: Context) {

    fun save(key: String, value: String) {
        val settings: SharedPreferences = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        //1
        editor = settings.edit()

        editor.putString(key, value) //3

        editor.apply() //4
    }

    fun getValue(key: String): String? {
        val settings: SharedPreferences = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        val text: String?

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        text = settings.getString(key, null)
        return text
    }

    fun clearSharedPreference() {
        val settings: SharedPreferences = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        editor = settings.edit()

        editor.clear()
        editor.apply()
    }

    fun removeValue(key: String) {
        val settings: SharedPreferences = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor

        editor = settings.edit()

        editor.remove(key)
        editor.apply()
    }

    companion object {
        val PREFS_NAME = "Tint_Preferences"
    }
}


